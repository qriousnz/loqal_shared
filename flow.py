#! /usr/bin/env python3

from collections import defaultdict
from concurrent import futures
import datetime
from pprint import pprint as pp
from subprocess import call

import folium

## pip install selenium
## Put latest gecko driver from https://github.com/mozilla/geckodriver/releases
## in /usr/local/bin so selenium can find it

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, db, folium_map, spatial, utils


class FlowMap():

    @staticmethod
    def _collect_data(imsi, min_date_str, max_date_str):
        unused, cur_gp = db.Pg.gp()
        sql = """\
        SELECT
          start_date_str,
          tower_lat,
          tower_lon
        FROM {events}
         AS events
        INNER JOIN
        {cells} AS cells
        USING(cell_id_rti)
        WHERE imsi_rti = %s
        AND events.date BETWEEN %s AND %s
        AND events.date >= cells.start_date
        AND (cells.end_date IS NULL OR events.date <= cells.end_date)
        ORDER BY events.start_date
        """.format(events=conf.CONNECTION_DATA, cells=conf.CELL_LOCATION_SEED)
        cur_gp.execute(sql, (imsi, min_date_str, max_date_str))
        data = cur_gp.fetchall()
        return data

    @staticmethod
    def _get_time_chunk(date_str):
        """
        2017-01-01 12:46:01 -->
        2017-01-01 12:45:00
    
        2017-01-01 12:59:59 -->
        2017-01-01 12:55:00
    
        Strip off up to hours and tens of mins, extract second part of mins
        and map 0-4 -> 0 and 5-9 -> 5.
    
        String slicing chosen because nice and fast and certain of correctness.
        """
        assert len(date_str) == 19
        up_to_mins = date_str[:15]
        mins = int(date_str[15])
        min2use = '0' if mins < 5 else '5'
        chunk_date_str = up_to_mins + min2use + ':00'
        return chunk_date_str

    @staticmethod
    def map_imsi_by_5min_chunks(data):
        """
        Every 5 minutes grab a chunk of data and calculate a centroid. Treat
        that as possible location. Handles tower bouncing but assumes no-one can
        travel far enough in that time to make it dangerous to use centroid.
        Because we strip out crazy leaps we don't have to worry about people
        travelling large distances in short time.
        """
        debug = True
        time_chunk_coord_dets = defaultdict(list)
        for event_date_str, lat, lon in data:
            time_chunk = FlowMap._get_time_chunk(event_date_str)
            coord = (lat, lon)
            dt = datetime.datetime.strptime(event_date_str, '%Y-%m-%d %H:%M:%S')
            coord_dets = {'coord': coord, 'dt': dt}
            time_chunk_coord_dets[time_chunk].append(coord_dets)
        time_chunk_centroids = []
        for time_chunk in sorted(time_chunk_coord_dets.keys()):
            coords_dets = time_chunk_coord_dets[time_chunk]
            centroid = spatial.Spatial.get_centroid_ex_crazy_leaps(coords_dets)
            time_chunk_centroids.append((time_chunk, centroid))
        if debug: pp(time_chunk_centroids)
        return time_chunk_centroids

    @staticmethod
    def _data_to_jump_points(data):
        """
        For given IMSI iterate through sorted events and break each time a
        certain distance from last jump has been made. Store 'travel' datetime,
        last datetime before 'travel', and coordinates of destination.
    
        Problem - bouncing can be between cell_id_rti's further apart than any
        sensible threshold.
        """
        debug = False
        if not data:
            raise ValueError("No data")
        unused, cur_local = db.Pg.local()
        jumps = []
        last_date_str = None
        for event_date_str, coord in data:
            if not jumps:
                first_jump = {'start': event_date_str, 'coord': coord}
                jumps.append(first_jump)
                last_date_str = event_date_str
                continue
            prev_jump = jumps[-1]
            new_coord = coord
            old_coord = prev_jump['coord']
            dist_km = spatial.Pg.gap_km(cur_local, new_coord, old_coord)
            moved = (dist_km > 5)
            if moved:
                prev_jump['end'] = last_date_str
                new_jump = {'start': event_date_str, 'coord': coord}
                jumps.append(new_jump)
            last_date_str = event_date_str
        jumps[-1]['end'] = '...'
        if debug:
            for jump in jumps:
                print("{} from {} to {}".format(jump['coord'],
                    jump['start'], jump['end']))
        return jumps

    @staticmethod
    def _jump_points_to_jump_pairs(jump_points):
        """
        a, b, c, d --> a-b, b-c, c-d
        """
        jump_pairs = []
        for jump in jump_points:
            start_coord_dets = {'coord': jump['coord'], 'dt': jump['end']}
            end_coord_dets = {'coord': jump['coord'], 'dt': jump['start']}
            if not jump_pairs:
                init_pair = [end_coord_dets, ]  ## complete in next round
                jump_pairs.append(init_pair)
            else:
                prev_pair = jump_pairs[-1]
                prev_pair.append(end_coord_dets)  ## complete previous
                if len(jump_pairs) == len(jump_points) - 1:
                    break
                new_pair = [start_coord_dets, ]  ## start new pair
                jump_pairs.append(new_pair)    
        return jump_pairs

    @staticmethod
    def _get_bb(breakpoints):
        """
        Get a single bounding box we can safely use for all images. Makes it
        easy to see movement and does wonders for video compression ;-).
        """
        all_coords = [breakpoint['coord'] for breakpoint in breakpoints]
        lats = [coord[0] for coord in all_coords]
        lons = [coord[1] for coord in all_coords]
        min_lat, max_lat = min(lats), max(lats)
        min_lon, max_lon = min(lons), max(lons)
        bb = [[max_lat, min_lon], [min_lat, max_lon]]  ## Bounding box specified as two points [southwest, northeast]
        return bb

    @staticmethod
    def rename_imgs(img_folder):
        fnames = os.listdir(img_folder)
        fnames.sort()
        for num, fname in enumerate(fnames, 1):
            fpath = os.path.join(img_folder, fname)
            src = fpath
            dest = os.path.join(img_folder, "map_{:>03}.png".format(num))
            os.rename(src, dest)
        print("Renaming complete")

    @staticmethod
    def make_video(froot=None, output_fpath=None, fps=2, title='custom_video'):
        """
        title only used if output_fpath not supplied. Is used to make an
        output_fpath.
        """
        ## make video for easy sharing and display
        ## https://askubuntu.com/questions/573712/convert-thousands-of-pngs-to-animated-gif-convert-uses-too-much-memory
        ## https://superuser.com/questions/820134/why-cant-quicktime-play-a-movie-file-encoded-by-ffmpeg
        ## https://stackoverflow.com/questions/20847674/ffmpeg-libx264-height-not-divisible-by-2
        debug = True
        if froot is None:
            froot = ('/home/gps/Documents/tourism/storage/flows/reports/images/'
                'charts/custom_video_hopper')
            input("Only have the files you want in '{}' before proceeding"
                .format(froot))
            FlowMap.rename_imgs(froot)
        if output_fpath is None:
            output_fpath = os.path.join(froot, '{}.mp4'.format(title))
        cmd = [
            'ffmpeg',
            '-framerate', str(fps),
            '-i', '{}/map_%03d.png'.format(froot),
            '-c:v', 'libx264',
            '-pix_fmt', 'yuv420p', '-vf', 'scale=-2:720',  ## must be yuv420p for Quicktime and scaled to be even pixels so yuv420p will work
            output_fpath,
        ]
        if debug: print(' '.join(cmd))
        call(cmd)
        print("Finished! Made ")

    @staticmethod
    def make_imsi_travel_video(imsi, min_date_str, max_date_str, fps=4,
            display=False):
        """
        Make lots and lots of little maps, one per journey - each bounded so
        they focus on the line of movement. Give them a consistent bounding box
        so small movements are visible. Within each 5 minute chunk exclude crazy
        leaps. Leave other crazy leaps in e.g. a 5 minute chunk which is
        suddenly centred somewhere far away.
        """
        MAX_SANE_WORKERS = 3 if display else 7
        ## get time-chunked data (excluding any crazy leaps)
        data = FlowMap._collect_data(imsi, min_date_str, max_date_str)
        time_chunk_centroids = FlowMap.map_imsi_by_5min_chunks(data)
        jump_points = FlowMap._data_to_jump_points(time_chunk_centroids)  ## significant jumps of distance
        bb = FlowMap._get_bb(jump_points)
        jump_pairs = FlowMap._jump_points_to_jump_pairs(jump_points)  ## each transition a, b, c, d --> a-b, b-c, c-d
        ## point pairs into sequential charts
        now_str = datetime.datetime.now().isoformat()
        froot = os.path.join(conf.VOYAGER_ROOT, 'storage', 'flows', 'reports',
            'images', 'charts', "{}_{}".format(imsi, now_str))
        try:
            os.mkdir(froot)
        except OSError:
            pass
        confs = []
        for n, jump_pair in enumerate(jump_pairs, 1):
            map_osm = folium.Map()
            map_osm.fit_bounds(bb)
            n_point_pairs = len(jump_pair)
            if n_point_pairs < 1 or n_point_pairs > 2:
                raise Exception("Got {:,} coord_lbls in pair - expected 2 or 1"
                    .format(n_point_pairs))
            start_coord_dets, end_coord_dets = jump_pair
            ## line
            points = [start_coord_dets['coord'], end_coord_dets['coord']]
            folium.PolyLine(points, weight=10).add_to(map_osm)
            ## pop-up
            folium_map.Map.add_map_icon_popup(map_osm, text=str(n),
                colour='red', coord=end_coord_dets['coord'])
            ## text
            start_dt = datetime.datetime.strptime(start_coord_dets['dt'],
                '%Y-%m-%d %H:%M:%S')
            end_dt = datetime.datetime.strptime(end_coord_dets['dt'],
                '%Y-%m-%d %H:%M:%S')
            start_date = start_dt.strftime('%a %d %b')
            end_date = end_dt.strftime('%a %d %b')
            start_time = dates.Dates.friendly_date_str(start_dt)
            end_time = dates.Dates.friendly_date_str(end_dt)
            same_date = (start_date == end_date)
            if same_date:
                lbl = "{} {}-{}".format(start_date, start_time, end_time)
            else:
                lbl = "{} {} - {} {}".format(start_date, start_time, end_date,
                    end_time)
            folium_map.Map.add_map_text_annotation(map_osm, text=lbl, colour='b',
                coord=end_coord_dets['coord'], fontsize=10, fontweight='bold',
                xshift=-20)
            fname = 'map_{:>03}.png'.format(n)  ## need simple format so ffmpeg can process with 
            output_fpath = os.path.join(froot, fname)
            url = folium_map.Map.mapobj2url(map_osm)
            confs.append(
                {'url': url, 'output': output_fpath, 'display': display})
        with futures.ProcessPoolExecutor(MAX_SANE_WORKERS) as executor:
            executor.map(utils.url2png, confs)
        output_fname = '{}/{}_{}_to_{}.mp4'.format(froot, imsi,
            min_date_str, max_date_str)
        output_fpath = os.path.join(froot, output_fname)
        FlowMap.make_video(froot, output_fpath, fps)
        return output_fpath

def main():
    FlowMap.make_video(fps=5); return
    
#     FlowMap.make_imsi_travel_video(imsi=-9218118683450546468,
#         min_date_str='2017-01-05', max_date_str='2017-01-15'); return

if __name__ == '__main__':
    main()
