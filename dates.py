
import calendar
from datetime import date, datetime, timedelta
from pytz import timezone
import pytz
import time


class Dates():
    """
    TIMEZONES

    * POSTGRESQL ****

    HOUR takes hour from timezone-aware date as at the timezone you are set for
    (and as the data displays)
    e.g. 2016-12-13 06... --> 6
    so if timezone is set to 'NZ' then all is well (for postgresql - matplotlib
    needs its own setting to use Pacific/Auckland timezone)
    if you set different timezone you'll see the time relative to that new
    timezone e.g. Asia/Shanghai
    sudo vim /etc/postgresql/9.5/main/postgresql.conf
    sudo /etc/init.d/postgresql start

    * MATPLOTLIB ****
    Because the data is time-zoned it is important to set timezone setting in
    matplotlibrc to e.g. Pacific/Auckland (if that is what the data refers to).

    * PYTHON
    Don't use naive timestamps e.g. datetime.datetime(2016,1,1,12,10,0)
    Instead use:

    from pytz import timezone
    nz = timezone('Pacific/Auckland')
    nz.localize(datetime(2016, 12, 30, 12, 48, 10))
    so that dates coming from postgresql (timezone-aware) and python are
    compatible.
    Hint - if times are off by 13 hours ... ;-)
    """
    nz = timezone('Pacific/Auckland')

    @staticmethod
    def friendly_date_str(date):
        friendly_date_str = (date.strftime('%I').lstrip('0')
            + date.strftime(':%M') + date.strftime('%p').lower())
        return friendly_date_str

    @staticmethod
    def get_tues_date_strs(min_date, max_date):
        day = min_date
        tues_date_strs = []
        while True:
            if day > max_date:
                break
            if day.isoweekday() == 2:  ## Monday = 1
                tues_date_strs.append(day.strftime('%Y-%m-%d'))
            day += timedelta(days=1)
        return tues_date_strs

    @staticmethod
    def end_tues_date_to_range(date_str, compact=False):
        """
        According to wiki on home location this covers prev Tue to Monday
        (https://tdigital.atlassian.net/wiki/spaces/BIGDATA/pages/162831115/...
          ...Home+Location).
        """
        if compact and '-' in date_str:
            raise Exception("Dumbkopf! Date compacts don't have hyphens. "
                "'20171010' is OK but '2017-10-10' is not.")
        dt_format = '%Y%m%d' if compact else '%Y-%m-%d'
        min_date = (datetime.strptime(date_str, dt_format) - timedelta(days=7))
        assert min_date.isoweekday() == 2  ## Monday is 1
        min_date_str = min_date.strftime(dt_format)
        max_date = (datetime.strptime(date_str, dt_format) - timedelta(days=1))
        assert max_date.isoweekday() == 1  ## Monday is 1
        max_date_str = max_date.strftime(dt_format)
        return min_date_str, max_date_str

    @staticmethod
    def gap(date_str_a, date_str_b):
        date_a = datetime.strptime(date_str_a, '%Y-%m-%d').date()
        date_b = datetime.strptime(date_str_b, '%Y-%m-%d').date()
        gap = abs((date_a - date_b).days)
        return gap

    @staticmethod
    def date_strs_to_date_ranges(date_strs):
        date_strs = sorted(list(set(date_strs)))
        date_ranges = []
        for date_str in date_strs:
            try:
                last_range = date_ranges[-1]
            except IndexError:
                date_ranges.append({'start': date_str, 'end': date_str})
            else:
                if Dates.gap(date_str, last_range['end']) == 1:
                    last_range['end'] = date_str
                else:
                    date_ranges.append({'start': date_str, 'end': date_str})
        date_ranges = [(date_range['start'], date_range['end'])
            for date_range in date_ranges]
        return date_ranges

    @staticmethod
    def get_local_date(y, m, d):
        return date(y, m, d)  ## you can't localise dates and don't need to

    @staticmethod
    def get_local_datetime(y, m, d, h=0, M=0, s=0, ms=0):
        return Dates.nz.localize(datetime(y, m, d, h, M, s, ms))

    @staticmethod
    def get_local_datetime_from_datetime_str(datetime_str):
        datetime_obj = datetime.strptime(datetime_str[:19], '%Y-%m-%d %H:%M:%S')
        return Dates.nz.localize(datetime_obj)

    @staticmethod
    def get_utc_datetime(y, m, d, h=0, M=0, s=0, ms=0):
        debug = False
        datetime2use = Dates.get_local_datetime(y, m, d, h, M, s, ms)
        gmt = datetime2use.astimezone(pytz.utc)
        if debug:
            print(y, m, d, h, M, s, ms, gmt, gmt.timetuple())
        return gmt

    @staticmethod
    def get_epoch_int(y, m, d, h=0, M=0, s=0, ms=0):
        """
        print(Dates.get_epoch_int(2015,5,1,0,13,47))
        print("Was it {}?".format(1430396027))
        """
        datetime2use = Dates.get_local_datetime(y, m, d, h, M, s, ms)
        epoch_int = calendar.timegm(datetime2use.astimezone(pytz.utc)\
            .timetuple())
        return epoch_int

    @staticmethod
    def get_epoch_int_from_date(date_obj):
        try:
            timetuple = date_obj.timetuple()[:6]
        except AttributeError:
            epoch_int = None
        else:
            epoch_int = Dates.get_epoch_int(*timetuple)
        return epoch_int

    @staticmethod
    def get_datetime_from_epoch_int(epoch_int):
        """
        print(Dates.get_datetime_from_epoch_int(1430396027))
        print("Was it 2015, 5, 1, 0, 13, 47 ?")
        """
        time_struct = time.localtime(epoch_int)
        datetime2use = Dates.get_local_datetime(*time_struct[:6])
        return datetime2use

    @staticmethod
    def _dates_from_min_max_strs(min_date_str, max_date_str):
        min_date = datetime.strptime(min_date_str, '%Y-%m-%d')
        max_date = datetime.strptime(max_date_str, '%Y-%m-%d')
        return min_date, max_date

    @staticmethod
    def _date_strs_from_min_max_dates(min_date, max_date):
        date_strs = []
        date2use = min_date
        while date2use <= max_date:
            date_str = date2use.strftime('%Y-%m-%d')
            date_strs.append(date_str)
            date2use += timedelta(days=1)
        return date_strs

    @staticmethod
    def date_strs_from_min_max_date_strs(min_date_str, max_date_str):
        min_date, max_date = Dates._dates_from_min_max_strs(min_date_str,
            max_date_str)
        return Dates._date_strs_from_min_max_dates(min_date, max_date)

    @staticmethod
    def _get_y_m_d_hs(min_date_str, max_date_str):
        min_date, max_date = Dates._dates_from_min_max_strs(min_date_str,
            max_date_str)
        min_datetime = datetime(*min_date.timetuple()[:6])
        max_datetime = datetime(*max_date.timetuple()[:6])
        y_m_d_hs = []
        datetime2use = min_datetime
        while datetime2use <= max_datetime:
            y_m_d_hs.append(datetime2use.timetuple()[:4])
            datetime2use += timedelta(hours=1)
        return y_m_d_hs

    @staticmethod
    def get_epoch_int_range(epoch_int, hours=3):
        if hours > 0:
            epoch_int_start = epoch_int
            epoch_int_end = epoch_int + (hours*3600)
        elif hours < 0:
            epoch_int_start = epoch_int + (hours*3600)
            epoch_int_end = epoch_int
        else:
            raise Exception("Unexpected value of hours ({})".format(hours))
        epoch_int_range = (epoch_int_start, epoch_int_end)
        return epoch_int_range

    @staticmethod
    def str_to_date_or_default(raw_date_str, date_format='%d/%m/%y',
            default=None):
        """
        Note - returns a date not a datetime.
        """
        date_or_none = (datetime.strptime(raw_date_str, date_format).date()
            if raw_date_str else default)
        return date_or_none

    @staticmethod
    def date_to_date_str_or_empty_str(date_obj):
        try:
            date_str = date_obj.strftime('%Y-%m-%d')
        except AttributeError:
            date_str = ''
        return date_str

    @staticmethod
    def date_str_to_datetime_str_range(date_str, halfday=False):
        if halfday:
            start_datetime_str = date_str + ' 00:00:00'
            end_datetime_str = date_str + ' 23:59:59'
        else:
            start_datetime_str = date_str + ' 08:00:00'
            end_datetime_str = date_str + ' 20:00:00'
        return start_datetime_str, end_datetime_str
