from collections import defaultdict, OrderedDict
from datetime import date, datetime, timedelta

import matplotlib.dates as mdates
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.pyplot as plt

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, utils
int2dt, keys2dts, str2dt, tups2dts = (utils.int2dt, utils.keys2dts,
    utils.str2dt, utils.tups2dts)


class Mpl():

    @staticmethod
    def set_titles(ax, title):
        for loc in ['left', 'center', 'right']:
            ax.set_title(title, fontsize=48, fontweight='bold', loc=loc,
                verticalalignment='bottom')

    @staticmethod
    def set_legends(ax, handles, lbls, fontsize=12, lower_propn=None):
        """
        lower_propn -- not useful unless you know the size of / number of items
        in the legend.
        """
        if lower_propn:
            loc = (0.008, lower_propn)
        else:
            loc = 'upper left'
        left_legend = ax.legend(handles, lbls, loc=loc, fontsize=fontsize)
        plt.gca().add_artist(left_legend)
        centre_legend = ax.legend(handles, lbls, loc='upper center',
            fontsize=fontsize)
        plt.gca().add_artist(centre_legend)
        ax.legend(handles, lbls, loc='upper right', fontsize=fontsize)

    @staticmethod
    def add_year_vlines(ax, x_offset, x, rotate=True, lower_propn_line=0,
            lower_propn_text=0.9, fontsize=24):
        """
        Relies on y_lim so if y_lim is manually set run this afterwards, not
        beforehand.
        """
        unused, y_upper = ax.get_ylim()
        min_dt = min(x)
        max_dt = max(x)
        min_year = min_dt.year
        test_dt = (dates.Dates.get_local_datetime(min_year, 1, 1, 0, 0, 0, 0)
            if isinstance(min_dt, datetime)
            else dates.Dates.get_local_date(min_year, 1, 1)
        )
        if test_dt <= min_dt:
            min_year = min_year + 1 ## so don't plot year line and lots of missing data prior to actual min date
        max_year = max_dt.year
        years = range(min_year, max_year+1)
        rotation = 90 if rotate else 0
        for year in years:
            year_x = str2dt('{}0101'.format(year))
            plt.axvline(x=year_x, linewidth=4, color='black',
                ymin=lower_propn_line)
            ax.text(year_x + x_offset, lower_propn_text*y_upper,
                '{} '.format(year), ha='left', va='top', fontsize=fontsize,
                rotation=rotation)

    @staticmethod
    def set_ticks(ax, interval=2, inc_minor=True):
        ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mdates.MO,
            interval=interval))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%b-%d\n%Y'))
        ax.tick_params(axis='both', which='major', length=20, width=3,
            labelsize=30, direction='inout')
        if inc_minor:
            ax.xaxis.set_minor_locator(mdates.DayLocator())
            ax.tick_params(axis='both', which='minor', length=12, width=2,
                labelsize=10, direction='inout')

    @staticmethod
    def set_axis_labels(ax, y_label='Freq'):
        ax.set_xlabel('Date', fontsize=30)
        ax.set_ylabel(y_label, fontsize=30)

    @staticmethod
    def get_y_offsets(max_y):
        std_offset = max_y/12
        small_std_offset = (std_offset/3)
        tiny_std_offset = (std_offset/20)
        return std_offset, small_std_offset, tiny_std_offset

    @staticmethod
    def add_mpl_annotations(ax, dts_with_annotations, x_offset, std_offset,
            small_std_offset, tiny_std_offset):
        """
        Try to make sure annotations don't obscure each other or the bars.

        The only reason we cycle through by day is so we can track previous day
        for (re)positioning annotation reasons (to minimise overlapping text).
        """
        last_text_ys = []
        small_x_offset = timedelta(hours=12)
        imgs = {}
        for unused_dt, annotations in dts_with_annotations.items():
            ## move up until not close to any immediately before (previous day or today)
            current_text_ys = []
            for annotation in annotations:
                x = annotation['x']
                arrow_y = annotation['arrow_y']
                text_y = arrow_y + std_offset
                icon_name = annotation.get('icon_name')
                while True:  ## keep moving up until OK
                    too_close2neighbour = False
                    neighbour_ys = last_text_ys + current_text_ys
                    for last_text_y in neighbour_ys:
                        ll_existing = last_text_y - small_std_offset
                        ul_existing = last_text_y + small_std_offset 
                        close = (ll_existing < text_y < ul_existing)
                        if close:
                            too_close2neighbour = True
                            break
                    if not too_close2neighbour:
                        break
                    text_y += small_std_offset  ## only needs a small nudge for subsequent annotations
                current_text_ys.append(text_y)
                ax.annotate(annotation['lbl'], xy=(x+small_x_offset, arrow_y),
                    xycoords='data', xytext=(x, text_y),
                    arrowprops={'arrowstyle': '->'},
                    bbox={'fc': '1', 'pad': 2, 'alpha': 0.25},
                    va='bottom', ha='left')
                if icon_name:
                    if icon_name in imgs:
                        img = imgs[icon_name]
                    else:
                        img = plt.imread("reports/images/icons/{}.png"
                            .format(icon_name))  ## for anchor image could have considered using "\N{ANCHOR}" as text
                        imgs[icon_name] = img
                    x_img = x - x_offset
                    y_img = text_y + tiny_std_offset
                    Mpl.annotate_img(ax, img, x_img, y_img)
            last_text_ys = current_text_ys

    @staticmethod
    def add_annotation_legend(ax, legend_x_offset, col_x_offset, min_x,
            small_std_offset, tiny_std_offset):
        """
        Add annotation legend (one for each icon in use).
        legend_x_offset -- from left
        col_x_offset -- also from left, not from icon
        """
        unused, top = ax.get_ylim()
        top_start = 0.95*top
        leg_items = [
            ('music', 'Concerts'),
            ('anchor', 'Cruise Ship Visits'),
            ('lightning', 'Disaster'),
            ('star', 'National Holidays'),
        ]
        for i, leg_item in enumerate(leg_items, 1):
            icon_name, leg_lbl = leg_item
            img = plt.imread("reports/images/icons/{}.png".format(icon_name))
            x_img = min_x + legend_x_offset
            x_text = min_x + col_x_offset
            y_text = top_start - (i*small_std_offset)
            y_img = y_text + (2*tiny_std_offset)
            Mpl.annotate_img(ax, img, x_img, y_img)
            ax.text(x_text, y_text, leg_lbl, fontsize=24)

    @staticmethod
    def annotate_img(ax, img, x, y):
        """Annotate image onto chart"""
        imagebox = OffsetImage(img)
        imagebox.image.axes = ax
        ab = AnnotationBbox(imagebox, (x, y), frameon=False)
        ax.add_artist(ab)


class Humanise():
    """
    Adding colour, annotations, human meaning etc to data so we can interpret it
    and apply all that extra knowledge we have. 
    """

    COLOURS_ALL = 'colours_all'
    COLOURS_SPECIAL_ONLY = 'colours_special_only'
    COLOURS_OTHER_ONLY = 'colours_other_only'

    @staticmethod
    def prep_date_event_mappings():
        """
        Process all mappings ensuring all dates are datetime dates ready for
        matching.
        """
        nat_hol_dts = tups2dts(conf.NAT_HOLS)
        reg_hol_dts = tups2dts(conf.REG_HOLS)
        hol_dt2dets = defaultdict(list)
        for datestr, lbl in conf.NAT_HOLS:
            hol_dt2dets[str2dt(datestr)].append(
                {'lbl': lbl, 'icon_name': 'star'})
        for datestr, lbl in conf.REG_HOLS:
            hol_dt2dets[str2dt(datestr)].append({'lbl': lbl})
        nat_event_dt2dets = keys2dts(conf.NAT_EVENTS, reg=False)
        cruise_reg_event_dt2dets = keys2dts(conf.CRUISE_REG_EVENTS, reg=True)
        concert_reg_event_dt2dets = keys2dts(conf.CONCERT_REG_EVENTS, reg=True)
        gen_reg_event_dt2dets = keys2dts(conf.GEN_REG_EVENTS, reg=True)
        reg_event_mappings = [
            cruise_reg_event_dt2dets,
            concert_reg_event_dt2dets,
            gen_reg_event_dt2dets,
        ]
        return (nat_hol_dts, reg_hol_dts, hol_dt2dets, nat_event_dt2dets,
            reg_event_mappings)

    @staticmethod
    def enrich_dates(raw_daily_data, nat_event_dt2dets, nat_hol_dts,
            reg_hol_dts):
        """
        [{'date': , 'freq': ), ...] --> [{'dt': , 'freq': , 'weekend': , etc}, ...]

        Enrich the data in one place for all uses rather than having to make the
        same calculations all over the place.

        raw_daily_data -- e.g. [(date, freq), ...] - just fresh from an sql
        call ;-)
        """
        daily_data = []
        for row in raw_daily_data:
            dt = row['date']
            if not isinstance(dt, date):
                #print(type(dt))
                dt = int2dt(dt)  ## an integer from SQLite?
            weekend = datetime.isoweekday(dt) > 5  ## Monday is 1
            nat_hol = (dt in nat_hol_dts)
            reg_hol = (dt in reg_hol_dts)
            during_nat_event = (dt in nat_event_dt2dets)
            daily_data.append(
                {'dt': dt, 'freq': row['freq'], 'weekend': weekend,
                 'nat_hol': nat_hol, 'reg_hol': reg_hol,
                 'during_nat_event': during_nat_event})
        return daily_data

    @staticmethod
    def _colour_dt(weekend, nat_hol, reg_hol, during_nat_event):
        if nat_hol:
            colour = conf.MELON
        elif reg_hol:
            colour = conf.LAVENDER
        elif weekend:
            colour = conf.DUSK_GREEN
        else:
            colour = conf.PALE_GREY
        return colour

    @staticmethod
    def annotate_and_colour(daily_data, rto, nat_event_dt2dets,
            reg_event_mappings, hol_dt2dets, max_y=None):
        """
        Add colours and annotations to contextualise values and make it easier
        to interpret them.

        Expects daily_data to be [{'weekend': ...}, ...]

        Example return value:
        {
          'colours4dts': ['grey', 'grey', 'red', ...],

          'dts_with_annotations': {  ## ordered dict - for each date list annotations
              '2017-01-01 etc': [
                  {
                      'x': '2017-01-01 etc',  ## note - included again as a convenience
                      'arrow_y': 850,  ## vertical position to point arrow at
                      'lbl': 'Simple Minds Concert',  ## text to display
                      'icon_name': 'music',  ## image to display to left of text
                  },
              ]
          }
        }
        """
        colours4dts = [] ## colours for dates in order e.g. grey for default, green for weekend etc
        annotations = defaultdict(list)  ## for each date key list annotation dicts
        for data_dic in daily_data:
            colours4dts.append(Humanise._colour_dt(data_dic['weekend'],
                data_dic['nat_hol'], data_dic['reg_hol'],
                data_dic['during_nat_event']))
            dt = data_dic['dt']
            y = data_dic['freq']
            init_annotation = {'x': dt, 'arrow_y': y}
            ## national event annotations
            if nat_event_dt2dets.get(dt):
                for nat_event_dets in nat_event_dt2dets[dt]:    
                    nat_event_annotation = init_annotation.copy()
                    nat_event_annotation.update(nat_event_dets)
                    annotations[dt].append(nat_event_annotation)
            ## regional event annotations
            for reg_event_dt2dets in reg_event_mappings:            
                if reg_event_dt2dets.get((dt, rto)):
                    for reg_event_dets in reg_event_dt2dets[(dt, rto)]:
                        reg_event_annotation = init_annotation.copy()
                        reg_event_annotation.update(reg_event_dets)
                        annotations[dt].append(reg_event_annotation)
            ## holiday annotations
            if hol_dt2dets.get(dt):
                for hol_dets in hol_dt2dets[dt]:
                    hol_annotation = init_annotation.copy()
                    hol_annotation.update(hol_dets)
                    annotations[dt].append(hol_annotation)
        dts_with_annotations = OrderedDict(sorted(annotations.items()))
        return {
            'colours4dts': colours4dts,
            'dts_with_annotations': dts_with_annotations,
        }

    @staticmethod
    def colours(colour_filter=COLOURS_ALL):
        """
        Full list of web-safe colours minus colours kept aside to specially mark
        key items so they stand out and are distinct from each other. 
        """
        spec_colours = [
            'red',
            'deepskyblue',
            'darkgreen',
            'mediumpurple',
            'deeppink',
            'teal',
            'gold',
            'lime',
            'black',
            'chocolate',
            'darkorange',
            'silver',
            'purple',
            'slategrey',
            'rosybrown',
            'blue',
            'olive',
        ]
        other_colours = [  ## https://en.wikipedia.org/wiki/Web_colors
            'dimgray',
            #'deepskyblue',
            'palegoldenrod',
            'darkslateblue',
            'blanchedalmond',
            #'mediumpurple',
            'lightskyblue',
            'darkolivegreen',
            'khaki',
            'lightblue',
            'yellowgreen',
            #'lime',
            'peachpuff',
            'lawngreen',
            'darkgray',
            'lightgoldenrodyellow',
            'darkmagenta',
            #'black',
            'sienna',
            'lemonchiffon',
            'palevioletred',
            'blueviolet',
            'mediumslateblue',
            'olivedrab',
            'darkcyan',
            'fuchsia',
            'white',
            'slateblue',
            'darkkhaki',
            'snow',
            'burlywood',
            #'silver',
            'coral',
            'darkseagreen',
            'crimson',
            'linen',
            'mintcream',
            'darkviolet',
            'beige',
            'mediumaquamarine',
            'forestgreen',
            'seashell',
            'peru',
            'saddlebrown',
            'darkslategrey',
            #'gold',
            'aquamarine',
            'yellow',
            'lightgray',
            'lightsteelblue',
            'goldenrod',
            'cornflowerblue',
            'greenyellow',
            'antiquewhite',
            'darkgoldenrod',
            'honeydew',
            'papayawhip',
            'darkturquoise',
            'turquoise',
            'lightslategrey',
            'darkgrey',
            'orchid',
            'green',
            #'slategrey',
            #'rosybrown',
            'ghostwhite',
            'navy',
            #'darkgreen',
            'cadetblue',
            'powderblue',
            'dodgerblue',
            'cyan',
            'plum',
            'indianred',
            'lightcyan',
            'gray',
            'palegreen',
            'maroon',
            'aliceblue',
            'oldlace',
            'mediumseagreen',
            'lightslategray',
            'thistle',
            'mediumorchid',
            'salmon',
            'skyblue',
            'paleturquoise',
            'bisque',
            'mediumvioletred',
            'seagreen',
            'lightgrey',
            'pink',
            'orangered',
            'floralwhite',
            'lavenderblush',
            'indigo',
            'lightgreen',
            #'chocolate',
            'lightpink',
            'steelblue',
            'mediumblue',
            'darkblue',
            'moccasin',
            'springgreen',
            'slategray',
            'aqua',
            'magenta',
            'lavender',
            'mediumturquoise',
            'cornsilk',
            'darkslategray',
            #'darkorange',
            'sandybrown',
            'dimgrey',
            'tomato',
            'azure',
            'navajowhite',
            #'blue',
            'darksalmon',
            'midnightblue',
            'limegreen',
            #'purple',
            'mistyrose',
            #'teal',
            'violet',
            'tan',
            'chartreuse',
            'mediumspringgreen',
            'orange',
            'lightcoral',
            #'olive',
            'wheat',
            'ivory',
            'royalblue',
            #'red',
            #'deeppink',
            'darkorchid',
            'hotpink',
            'grey',
            'firebrick',
            'lightsalmon',
            'lightseagreen',
            'whitesmoke',
            'lightyellow',
            'darkred',
            'gainsboro',
            'brown']
        if colour_filter == Humanise.COLOURS_SPECIAL_ONLY:
            colours = spec_colours
        elif colour_filter == Humanise.COLOURS_OTHER_ONLY:
            colours = other_colours
        elif colour_filter == Humanise.COLOURS_ALL:
            colours = spec_colours + other_colours
        else:
            raise Exception("Unexpected colour_filter '{}'".format(
                colour_filter))
        for colour in colours:
            yield colour
