from time import sleep
import sqlite3 as sqlite
from sqlite3 import Row #@UnresolvedImport
from subprocess import call

"""
Installing pyhive required:

## https://stackoverflow.com/questions/22838752/...
  ...hadoop-python-client-driver-for-hiveserver2-fails-to-install
sudo apt install libsasl2-dev

then

sudo python3 -m pip install pure-sasl
sudo python3 -m pip install thrift --no-deps
sudo python3 -m pip install thrift_sasl --no-deps

and finally

sudo -H python3 -m pip install pyhive[hive]
"""
from pyhive import hive
from TCLIService.ttypes import TOperationState

import psycopg2 as pg
import psycopg2.extras as pg_extras

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, not4git


class Db():

    @staticmethod
    def nums2clause(nums):
        return '(' + ', '.join([str(num) for num in nums]) + ')'

    @staticmethod
    def strs2clause(strs):
        return "('" + "', '".join([mystr for mystr in strs]) + "')"


class Sqlite(Db):

    """
    Example of import from csv
    sqlite3 segments.db
    sqlite3 > .mode csv
    sqlite3 > .import street_coords.csv streets_coord
    ctrl-D
    """

    @staticmethod
    def get_con_cur(db):
        con = sqlite.connect(db) #@UndefinedVariable
        con.row_factory = Row
        cur = con.cursor()
        return con, cur


class Hive(Db):
    """
    https://pypi.python.org/pypi/PyHive

    Note - no dict result - see https://github.com/dropbox/PyHive/pull/79
    """

    @staticmethod
    def hive(user='grant.patonsimpson'):
        con_hive = hive.connect(host='10.100.99.5', port=10000,
            username=user)
        cur_hive = con_hive.cursor()
        return con_hive, cur_hive

    @staticmethod
    def fetch_prog(cur):
        status = cur.poll().operationState
        in_prog_states = (TOperationState.INITIALIZED_STATE,
            TOperationState.RUNNING_STATE)
        while status in in_prog_states:
            logs = cur.fetch_logs()
            for message in logs:
                print(message)
            status = cur.poll().operationState
        print("Completed")


class Pg(Db):

    @staticmethod
    def table_exists(cur, tblname, schema='public'):
        """
        https://stackoverflow.com/questions/20582500/...
            ...how-to-check-if-a-table-exists-in-a-given-schema
        """
        sql = """\
        SELECT EXISTS (
        SELECT 1 
        FROM   pg_tables
        WHERE  schemaname = '{schema}'
        AND    tablename = '{tblname}'
        ) AS exists
        """.format(tblname=tblname, schema=schema)
        cur.execute(sql)
        exists = cur.fetchone()['exists']
        return exists

    @staticmethod
    def gp(user='tnz_loc'):
        """
        tnz_loc has fewer privileges than gpadmin. Default to that and only
        escalate as needed (for only as long as needed).
        """
        if user == 'gpadmin':
            pwd = not4git.access_rem_gpadmin  #getpass.getpass('Enter remote GP password for user {}: '.format(user))
        elif user == 'tnz_loc':
            pwd = not4git.access_rem_tnz_loc
        else:
            raise Exception("Unexpected user '{}'".format(user))
        kwargs = {
            'dbname': 'tnz_loc',
            'user': user,
            'host': '10.100.99.121',
            'port': 5432,
            'password': pwd,
        }
        con_gp = pg.connect(**kwargs)
        cur_gp = con_gp.cursor(cursor_factory=pg_extras.DictCursor)
        return con_gp, cur_gp

    @staticmethod
    def local():
        pwd_local = not4git.access_local  #getpass.getpass('Enter postgres password: ')
        kwargs = {
            'dbname': not4git.dbname_local,
            'user': 'postgres',
            'host': 'localhost',
            'port': not4git.port_local,
            'password': pwd_local,
        }
        con_local = pg.connect(**kwargs)
        cur_local = con_local.cursor(cursor_factory=pg_extras.DictCursor)
        return con_local, cur_local

    @staticmethod
    def get_cur(con):
        return con.cursor(cursor_factory=pg_extras.DictCursor)

    @staticmethod
    def grant_public_read_permissions(tblname):
        con_gp, cur_gp = Pg.gp(user='gpadmin')
        sql_permissions = "GRANT SELECT ON TABLE {} TO public".format(tblname)
        cur_gp.execute(sql_permissions)
        con_gp.commit()
        print("Added public read permissions to '{}'".format(tblname))

    @staticmethod
    def drop_tbl(con, cur, tbl):
        sql_drop_tbl = """\
        DROP TABLE IF EXISTS {tbl} CASCADE
        """.format(tbl=tbl)
        cur.execute(sql_drop_tbl)
        con.commit()

    @staticmethod
    def csv2tbl(src_csv_fpath, dest_tbl, dest_fields,
            dest_user='tnz_loc', dest_host=conf.GP_HOST, dest_db=conf.GP_DB):
        """
        src_csv_fpath -- csv fpath (note - no header row)
        """
        fields_clause = ", ".join(dest_fields)
        cmd = ("\COPY {dest_tbl} ({fields_clause}) "
            "FROM '{src_csv_fpath}' "
            "WITH CSV".format(dest_tbl=dest_tbl,
            fields_clause=fields_clause, src_csv_fpath=src_csv_fpath))
        cmds = ['psql',
            '-h', dest_host,
            '-d', dest_db,
            '-U', dest_user,
            '-c', cmd]
        call(cmds)

    @staticmethod
    def keep_alive(cycle_mins=5, n=None):
        sql = "SELECT 1"
        unused, cur_gp = Pg.gp()
        i = 1
        while True:
            sleep(cycle_mins*60)
            cur_gp.execute(sql)
            print("I've kept the VPN alive for {:,} minutes :-)".format(cycle_mins*i))
            if n and i > n:
                break
            i += 1

if __name__ == '__main__':
    Pg.keep_alive(n=12*6)
