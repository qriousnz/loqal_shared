import datetime
from pprint import pprint as pp

import s2sphere as s2

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db, folium_map, spatial, utils

REGION_PNG_MAP_WIDTH = 2000
REGION_PNG_MAP_HEIGHT = 1000


class Grids():

    @staticmethod
    def s2_id_to_rect_coords(s2_id):
        """
        Get lat/lon coords for polygon defining rectangle for s2_id grid. With
        Pg.make_polygon_str can create a PostGIS shape. Ready for looking for
        overlaps etc.
        """
        cellid = s2.sphere.CellId(s2_id)
        cell = s2.sphere.Cell(cellid)
        vertex_points = []
        for i in range(4):
            vertex_points.append(cell.get_vertex(i))
        rect_coords = []
        for vertex_point in vertex_points:
            latlng = s2.sphere.LatLng.from_point(vertex_point)
            rect_coords.append((latlng.lat().degrees, latlng.lng().degrees))
        rect_coords.append(rect_coords[0])  ## wrap up polygon
        return rect_coords

    @staticmethod
    def s2_id(lat, lon, level=12):
        """
        Get s2_12_id from lat/lon.
        """
        lat_lng = s2.sphere.LatLng.from_degrees(lat, lon)
        cell_id = s2.sphere.CellId.from_lat_lng(lat_lng)
        s2_id = cell_id.parent(level).id()
        return s2_id

    @staticmethod
    def make_grid_tbl(srid=conf.WGS84_SRID):
        """
        Must create grid12 geoms in same srid as used to spatially join with
        regions i.e. 4326.
        """
        S2_ZOOM_12 = 12
        debug = True
        resp = input("Proceed, wiping the '{}' table first? Really? y/n".format(
            conf.GRIDS))
        if resp.lower() != 'y':
            print("Aborted process to avoid destroying '{}'".format(
                conf.GRIDS))
            return
        con_gp, cur_gp = db.Pg.gp()
        unused, cur_local = db.Pg.local()
        db.Pg.drop_tbl(con_gp, cur_gp, conf.GRIDS)
        sql_make_tbl = """\
        CREATE TABLE {grids} (
          grid_id integer,
          s2_zoom integer,
          lat double precision,
          lon double precision,
          rect geometry,
          primary key (grid_id)
        )
        """.format(grids=conf.GRIDS)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        sql_insert_tpl = """\
        INSERT INTO {grids}
        (grid_id, s2_zoom, lat, lon, rect)
        VALUES
        """.format(grids=conf.GRIDS)
        N_ROWS_PER_CYCLE = 500
        if debug: print("Getting cellids")
        cellids = GridsRegions._get_cellids(GridsRegions.nz_bounding_boxes)
        if debug: print("Getting grid12 details")
        grid12_dets = GridsRegions._get_grid12_dets(cellids)
        if debug:
            N_GRIDS = 10
            print("N grids: {:,}".format(len(grid12_dets)))
            print("Sample details of first {:,} grids".format(N_GRIDS))
            pp(grid12_dets[:N_GRIDS])
        i = 0
        while True:
            start_idx = i*N_ROWS_PER_CYCLE
            end_idx = (i+1)*N_ROWS_PER_CYCLE
            rows = grid12_dets[start_idx: end_idx]
            if not rows:
                break
            row_tups = [(
                    row['grid12'],
                    S2_ZOOM_12,
                    row['lat'], row['lon'],
                    spatial.Pg.geom_from_coords(cur_local, row['rect_coords'],
                        srid=srid)
                ) for row in rows]
            values_clause = ",\n".join("({}, {}, {}, {}, '{}')".format(*row_tup)
                for row_tup in row_tups)
            sql_insert = sql_insert_tpl + values_clause
            cur_gp.execute(sql_insert)
            print(utils.prog(N_ROWS_PER_CYCLE*(i+1)))
            i += 1
        con_gp.commit()
        ## Add spatial index to rect
        sql_spatial_index = """\
        CREATE INDEX grid_rect_gix ON {grids} USING GIST(rect)
        """.format(grids=conf.GRIDS)
        cur_gp.execute(sql_spatial_index)
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.GRIDS)
        con_gp.commit()
        if debug:
            sql_tot = """\
            SELECT COUNT(*) AS n FROM {grids}
            """.format(grids=conf.GRIDS)
            cur_gp.execute(sql_tot)
            print(cur_gp.fetchone()['n'])
        print("Finished making '{}'".format(conf.GRIDS))


class GridsRegions():

    ## Bounding boxes bottom-left to top-right. Overly inclusive but OK for now.
    ## Will revisit if we start thinking about needing grids for ships, yachts
    ## etc.
    north_island_bb = ((-41.641982, 172.508895), (-33.856788, 179.005234)) 
    south_island_bb = ((-47.368870, 165.677994), (-40.440918, 174.711721)) 
    nz_bounding_boxes = [
        north_island_bb,
        south_island_bb,
    ]
    DG = '#069736'
    Y = '#FFFF35'
    O = '#FFCB00'
    LG = '#39FF66'
    G = '#676667'
    R = 'red'

    ## following Voyager 1 boundaries (same except North and South Cant + ChCh = 1 with Kaikoura separated out
    rto2colour = {
        conf.NORTHLAND_RTO_ID: DG,
        conf.AUCK_RTO_ID: Y,
        conf.COROMANDEL_RTO_ID: O,
        conf.WAIKATO_RTO_ID: LG,
        conf.ROTORUA_RTO_ID: Y,
        conf.BOP_RTO_ID: G,
        conf.KAWERAU_RTO_ID: O,
        conf.GISBORNE_RTO_ID: DG,
        conf.TARANAKI_RTO_ID: O,
        conf.RUAPEHU_RTO_ID: Y,
        conf.TAUPO_RTO_ID: DG,
        conf.HAWKES_BAY_RTO_ID: Y,
        conf.WANGANUI_RTO_ID: DG,
        conf.MANAWATU_RTO_ID: G,
        conf.KAPITI_RTO_ID: O,
        conf.WELLINGTON_RTO_ID: Y,
        conf.WAIRARAPA_RTO_ID: DG,
        conf.NELSON_RTO_ID: O,
        conf.MARLBOROUGH_RTO_ID: DG,
        conf.WEST_COAST_RTO_ID: LG,
        conf.CANTERBURY_RTO_ID: G,
        conf.KAIKOURA_RTO_ID: Y,
        conf.LAKE_WANAKA_RTO_ID: O,
        conf.WAITAKI_RTO_ID: Y,
        conf.QUEENSTOWN_RTO_ID: Y,
        conf.CENTRAL_OTAGO_RTO_ID: DG,
        conf.DUNEDIN_RTO_ID: LG,
        conf.FIORDLAND_RTO_ID: DG,
        conf.SOUTHLAND_RTO_ID: O,
        conf.CLUTHA_RTO_ID: Y,

        conf.WHANGAREI_CITY_ID: R,
        conf.AUCKLAND_CITY_ID: R,
        conf.HAMILTON_CITY_ID: R,
        conf.ROTORUA_CITY_ID: R,
        conf.WELLINGTON_CITY_ID: R,
        conf.CHRISTCHURCH_CITY_ID: R,
        conf.DUNEDIN_CITY_ID: R,

        conf.NORTHLAND_LESS_WHANGAREI_ID: DG,
        conf.AUCKLAND_LESS_CENTRE_ID: Y,
        conf.COROMANDEL_ID: O,
        conf.WAIKATO_LESS_HAMILTON_ID: LG,
        conf.ROTORUA_LESS_CITY_ID: Y,
        conf.BOP_ID: G,
        conf.KAWERAU_WHAKATANE_ID: O,
        conf.GISBORNE_ID: DG,
        conf.TARANAKI_ID: O,
        conf.RUAPEHU_ID: Y,
        conf.LAKE_TAUPO_ID: DG,
        conf.HAWKES_BAY_ID: Y,
        conf.WANGANUI_ID: DG,
        conf.MANAWATU_ID: G,
        conf.KAPITI_RTO_ID: O,
        conf.WELLINGTON_LESS_CITY_ID: Y,
        conf.WAIRARAPA_ID: DG,
        conf.NELSON_ID: O,
        conf.MARLBOROUGH_ID: DG,
        conf.WEST_COAST_ID: LG,
        conf.CANT_LESS_CITY_ID: G,
        conf.KAIKOURA_ID: Y,
        conf.LAKE_WANAKA_ID: O,
        conf.WAITAKI_ID: Y,
        conf.QUEENSTOWN_ID: Y,
        conf.CENTRAL_OTAGO_ID: DG,
        conf.DUNEDIN_LESS_CITY_ID: LG,
        conf.FIORDLAND_ID: DG,
        conf.SOUTHLAND_ID: O,
        conf.CLUTHA_ID: Y,
        
    }

    rto2opacity = {  ## more transparent so can see completely enclosed city regions. Outer regions cover entire area with 0.1 opacity, inner parts with 0 opacity (so still 0.1 overall), then enclosed cities with 0.8 opacity to punch through the 0.1 covering.
        conf.NORTHLAND_LESS_WHANGAREI_ID: 0.1,
        conf.AUCKLAND_LESS_CENTRE_ID: 0.1,
        conf.WAIKATO_LESS_HAMILTON_ID: 0.1,
        conf.ROTORUA_LESS_CITY_ID: 0.1,
    }

    sql_insert_region_to_group = """\
        INSERT INTO {region_to_group}
        (region_id, region_group_id)
        VALUES (%s, %s)
        """.format(region_to_group=conf.REGION_TO_GROUP)

    @staticmethod
    def _get_cellids(bounding_boxes):
        debug = False
        cellids = set()
        for bb in bounding_boxes:
            bottom_left, top_right = bb 
            rect = s2.sphere.LatLngRect(
                s2.sphere.LatLng.from_degrees(*bottom_left),
                s2.sphere.LatLng.from_degrees(*top_right))
            coverer = s2.sphere.RegionCoverer()
            coverer.min_level = 12
            coverer.max_level = 12
            bb_cellids = coverer.get_covering(rect)
            if debug: print(len(bb_cellids))
            cellids = cellids.union(set(bb_cellids))
        if debug: print(len(cellids))
        return cellids

    @staticmethod
    def _get_grid12_dets(cellids):
        debug = False
        grid12_dets = []
        for cellid in cellids:
            if debug: print('cellid', cellid)
            cell = s2.sphere.Cell(cellid)
            centre = cell.get_center()
            centroid_coord = s2.sphere.LatLng.from_point(centre)
            lat, lon = (centroid_coord.lat().degrees,
                centroid_coord.lng().degrees)
            if debug: print(lat, lon)
            s2_12_id = cellid.parent(12).id()
            rect_coords = Grids.s2_id_to_rect_coords(s2_12_id)
            grid12 = spatial.Spatial.grid12(s2_12_id)
            grid12_dets.append({'grid12': grid12, 'rect_coords': rect_coords,
                'lat': lat, 'lon': lon})
        if debug: print(grid12_dets)
        return grid12_dets

    @staticmethod
    def make_region_group_tbl():
        resp = input("Proceed, wiping the '{}' table first? Really? y/n".format(
            conf.REGION_GROUP))
        if resp.lower() != 'y':
            print("Aborted process to avoid destroying '{}'".format(
                conf.REGION_GROUP))
            return
        con_gp, cur_gp = db.Pg.gp()
        db.Pg.drop_tbl(con_gp, cur_gp, conf.REGION_GROUP)
        sql_make_tbl = """\
        CREATE TABLE {region_group} (
          region_group_id SERIAL,
          label text,
          description text,
          notes text,
          overlaps_ok boolean,
          PRIMARY KEY (region_group_id)
        )
        """.format(region_group=conf.REGION_GROUP)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        sql_insert = """\
        INSERT INTO {region_group}
        (label, description, notes, overlaps_ok)
        VALUES ('NZ RTO',
          'NZ Regional Tourism Organisations',
          'Uses original boundaries for Manawatu',
          false
        ),
        ('LoQal Destinations NZ',
          'LoQal Destinations NZ',
          'All regions included in LoQal Destinations for NZ',
          true
        )
        """.format(region_group=conf.REGION_GROUP)
        cur_gp.execute(sql_insert)
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.REGION_GROUP)
        print("Finished making '{}'".format(conf.REGION_GROUP))

    @staticmethod
    def make_empty_region_to_group_tbl():
        resp = input("Proceed, wiping the '{}' table first? Really? y/n".format(
            conf.REGION_TO_GROUP))
        if resp.lower() != 'y':
            print("Aborted process to avoid destroying '{}'".format(
                conf.REGION_TO_GROUP))
            return
        con_gp, cur_gp = db.Pg.gp()
        db.Pg.drop_tbl(con_gp, cur_gp, conf.REGION_TO_GROUP)
        sql_make_tbl = """\
        CREATE TABLE {region_to_group} (
          region_to_group_id SERIAL,
          region_id integer,
          region_group_id integer,
          PRIMARY KEY (region_to_group_id)
        )
        """.format(region_to_group=conf.REGION_TO_GROUP)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.REGION_TO_GROUP)
        print("Finished making '{}'".format(conf.REGION_TO_GROUP))

    @staticmethod
    def make_complementary_region(complementary_region_id,
            complementary_region_label, large_region_id, region_id_to_subtract):
        con_gp, cur_gp = db.Pg.gp()
        sql_complementary_geom = """\
        SELECT ST_DIFFERENCE(large_geom, geom_to_subtract) AS
        complementary_geom
        FROM (
          SELECT geom AS large_geom FROM {region} WHERE region_id = %s
        ) AS large
        CROSS JOIN
        (
          SELECT geom AS geom_to_subtract FROM {region} WHERE region_id = %s
        ) AS small
        """.format(region=conf.REGION)
        cur_gp.execute(sql_complementary_geom, (large_region_id,
            region_id_to_subtract))
        complementary_geom = cur_gp.fetchone()['complementary_geom']
        sql_insert_tpl = """\
        INSERT INTO {region}
        (region_id, label, notes, geom, eff_start_dt, eff_end_dt)
        VALUES (%s, %s,'', %s, null, null)
        """.format(region=conf.REGION)
        cur_gp.execute(sql_insert_tpl, (complementary_region_id,
            complementary_region_label, complementary_geom))
        con_gp.commit()

    @staticmethod
    def make_region_tbl_inc_rtos(cur_local):
        """
        Must use same SRID (spatial reference ID) as used by grids i.e. 4326
        otherwise will have to transform to 4326 in query.
        """
        resp = input("Proceed, wiping the '{}' table first? Really? y/n".format(
            conf.REGION))
        if resp.lower() != 'y':
            print("Aborted process to avoid destroying '{}'".format(
                conf.REGION))
            return
        con_gp, cur_gp = db.Pg.gp()
        db.Pg.drop_tbl(con_gp, cur_gp, conf.REGION)
        sql_make_tbl = """\
        CREATE TABLE {region} (
          region_id integer,
          label text,
          notes text,
          geom geometry,
          area double precision,
          tl_lat double precision,
          tl_lon double precision,
          br_lat double precision,
          br_lon double precision,
          PRIMARY KEY (region_id)
        )
        """.format(region=conf.REGION)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        print("Created new {} table".format(conf.REGION))
        ## get details
        region_label_geoms = []  ## sort later by region id - not adding in that order but want to put into table in that order so correct order for ids in region table
        ## 1) All in Ivan's rto_shapes with unchanged boundaries
        unchanged_rtos = [
            ("Auckland RTO", conf.AUCK_RTO_ID),
            ("Bay of Plenty RTO", conf.BOP_RTO_ID),
            ("Central Otago RTO", conf.CENTRAL_OTAGO_RTO_ID),
            ("Clutha", conf.CLUTHA_RTO_ID),
            ("Coromandel RTO", conf.COROMANDEL_RTO_ID),
            ("Dunedin RTO", conf.DUNEDIN_RTO_ID),
            ("Fiordland RTO", conf.FIORDLAND_RTO_ID),
            ("Gisborne RTO", conf.GISBORNE_RTO_ID),
            ("Hawkes Bay RTO", conf.HAWKES_BAY_RTO_ID),
            ("Kapiti-Horowhenua RTO", conf.KAPITI_RTO_ID),
            ("Kawerau-Whakatane", conf.KAWERAU_RTO_ID),
            ("Lake Taupo RTO", conf.TAUPO_RTO_ID),
            ("Lake Wanaka RTO", conf.LAKE_WANAKA_RTO_ID),
            ("Manawatu RTO", conf.MANAWATU_RTO_ID),
            ("Marlborough RTO", conf.MARLBOROUGH_RTO_ID),
            ("Nelson Tasman RTO", conf.NELSON_RTO_ID),
            ("Northland RTO", conf.NORTHLAND_RTO_ID),
            ("Queenstown RTO", conf.QUEENSTOWN_RTO_ID),
            ("Rotorua RTO", conf.ROTORUA_RTO_ID),
            ("Ruapehu RTO", conf.RUAPEHU_RTO_ID),
            ("Southland RTO", conf.SOUTHLAND_RTO_ID),
            ("Taranaki RTO", conf.TARANAKI_RTO_ID),
            ("Waikato RTO", conf.WAIKATO_RTO_ID),
            ("Wairarapa RTO", conf.WAIRARAPA_RTO_ID),
            ("Waitaki RTO", conf.WAITAKI_RTO_ID),
            ("Wanganui RTO", conf.WANGANUI_RTO_ID),
            ("Wellington RTO", conf.WELLINGTON_RTO_ID),
            ("West Coast RTO", conf.WEST_COAST_RTO_ID),
        ]
        sql_get_geom = """\
        SELECT ST_TRANSFORM(geom, {srid}) AS
        geom_srid
        FROM {rto_shapes}
        WHERE rto = %s
        """.format(srid=conf.WGS84_SRID, rto_shapes=conf.RTO_SHAPES_GREENPLUM)
        for rto_name, region_id in unchanged_rtos:
            cur_gp.execute(sql_get_geom, (rto_name, ))
            geom = cur_gp.fetchone()['geom_srid']
            label2use = (rto_name if rto_name.endswith('RTO')
                else rto_name + ' RTO')
            region_label_geoms.append((region_id, label2use, geom))
            print("Added details for {}".format(rto_name))
        ## Add Kaikoura from local database
        sql_kaikoura_geom = """\
        SELECT ST_TRANSFORM(geom, {srid}) AS kaikoura_geom FROM kaikoura LIMIT 1
        """.format(srid=conf.WGS84_SRID)
        cur_local.execute(sql_kaikoura_geom)
        kaikoura_geom = cur_local.fetchone()['kaikoura_geom']
        region_label_geoms.append((conf.KAIKOURA_RTO_ID, "Kaikoura RTO",
            kaikoura_geom))
        ## DO NOT Create Canterbury from North + South Cant + ChCh - Kaikoura - leaves a stray line because not exact alignment to nth degree. Practically same for including grid12's but ...
#         sql_cant_inc_kaikoura = """\
#         SELECT ST_UNION(ST_TRANSFORM(geom, {srid})) AS cant_inc_kaikoura_geom
#         FROM {rto_shapes}
#         WHERE rto IN ('North Canterbury', 'South Canterbury', 'Christchurch')
#         """.format(srid=conf.WGS84_SRID, rto_shapes=conf.RTO_SHAPES_GREENPLUM)
#         cur_rem.execute(sql_cant_inc_kaikoura)
#         cant_inc_kaikoura_geom = cur_rem.fetchone()['cant_inc_kaikoura_geom']
#         sql_cant_ex_kaikoura = """\
#         SELECT ST_DIFFERENCE('{cant_inc_kaikoura_geom}', '{kaikoura_geom}') AS
#         cant_ex_kaikoura_geom
#         """.format(cant_inc_kaikoura_geom=cant_inc_kaikoura_geom,
#             kaikoura_geom=kaikoura_geom)
#         cur_rem.execute(sql_cant_ex_kaikoura)
#         cant_ex_kaikoura_geom = cur_rem.fetchone()['cant_ex_kaikoura_geom']
#         region_label_geoms.append((conf.CANTERBURY_RTO_ID, "Canterbury RTO",
#             cant_ex_kaikoura_geom))
        sql_cant = "SELECT geom FROM canterbury_rto_combined LIMIT 1"
        cur_local.execute(sql_cant)
        cant_ex_kaikoura_geom = cur_local.fetchone()['geom']
        region_label_geoms.append((conf.CANTERBURY_RTO_ID, "Canterbury RTO",
            cant_ex_kaikoura_geom))
        ## sort ready for insertion into region table
        region_label_geoms.sort(key=lambda s: s[0])
        ## actually insert into table
        sql_insert_tpl = """\
        INSERT INTO {region}
        (region_id,
         label, notes,
         geom, area, tl_lat, tl_lon, br_lat, br_lon)
        VALUES (%s, %s, null, %s, %s, %s, %s, %s, %s)
        """.format(region=conf.REGION)
        for region_id, label, geom in region_label_geoms:
            print("About to process region_id {}".format(region_id))
            (area, tl_lat, tl_lon,
             br_lat, br_lon) = GridsRegions.get_geo_dets(
                 cur_local, region_id, geom)
            cur_gp.execute(sql_insert_tpl,
                (region_id, label, geom, area, tl_lat, tl_lon, br_lat, br_lon))
        con_gp.commit()
        print("Added all RTO's to table")
        ## Add spatial index
        sql_spatial_index = """\
        CREATE INDEX region_geom_gix ON {region} USING GIST(geom)
        """.format(region=conf.REGION)
        cur_gp.execute(sql_spatial_index)
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.REGION)
        print("Finished making '{}'".format(conf.REGION))

    @staticmethod
    def make_active_region_grid_tbl():
        """
        Active means ever active in life of data.
        """
        con_gp, cur_gp = db.Pg.gp(user='gpadmin')
        db.Pg.drop_tbl(con_gp, cur_gp, conf.ACTIVE_REGION_GRID)
        sql_make_tbl = """\
        CREATE TABLE {active_region_grid} (
            region_grid_id SERIAL,
            region_id integer,
            grid_id integer,
            PRIMARY KEY (region_grid_id)
        )
        """.format(active_region_grid=conf.ACTIVE_REGION_GRID)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        ## insert into table where grids have ever been active
        sql_insert_into = """\
        INSERT INTO {active_region_grid}
        (region_id, grid_id)
        SELECT region_id, grid_id
        FROM {region_grid}
        WHERE grid_id IN (SELECT DISTINCT grid12 FROM {cells}) -- don't care about dates - ever active is enough
        """.format(active_region_grid=conf.ACTIVE_REGION_GRID,
            region_grid=conf.REGION_GRID, cells=conf.CELL_LOCATION_SEED)
        cur_gp.execute(sql_insert_into)
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.ACTIVE_REGION_GRID)
        print("Finished making '{}'".format(conf.ACTIVE_REGION_GRID))

    @staticmethod
    def update_region_grid_tbl(custom_region_id=None, region_group_id=None,
            destroy_first=False, n_rows_per_cycle=2000):
        """
        Join region and grid tables and identify all grids that are associated
        with each region (using geoms available for both).

        region_id -- e.g. adding a custom region. Automatically greedy. _Any_
            overlap results in association.
        region_group_id -- e.g. adding DHB's. If set, non-greedy (mutually
            exclusive - grids overlapping multiple regions are allocated to just
            one)
        """
        debug = False
        doing_one_custom = (custom_region_id and region_group_id is None)
        doing_region_group = (custom_region_id is None and region_group_id)
        ## sanity check input args
        if not (doing_one_custom or doing_region_group):
            raise Exception("Filter by region_id or region_group_id. Not both")
        con_gp, cur_gp = db.Pg.gp()
        if destroy_first:
            resp = input("Proceed, wiping the entire (very expensive to build) "
                "'{}' table first? Really? y/n".format(conf.REGION_GRID))
            if resp.lower() != 'y':
                print("Aborted process to avoid destroying '{}'".format(
                    conf.REGION_GRID))
                return
            else:
                db.Pg.drop_tbl(con_gp, cur_gp, conf.REGION_GRID)
                sql_make_tbl = """\
                CREATE TABLE {region_grid} (
                    region_grid_id SERIAL,
                    region_id integer,
                    grid_id integer,
                    PRIMARY KEY (region_grid_id)
                )
                """.format(region_grid=conf.REGION_GRID)
                cur_gp.execute(sql_make_tbl)
                con_gp.commit()
                print("Created empty '{}'".format(conf.REGION_GRID))
        if region_group_id:
            sql_regions = """\
            SELECT region_id
            FROM {region}
            INNER JOIN
            {region_to_group}
            USING(region_id)
            WHERE region_group_id = %s
            ORDER BY region_id
            """.format(region=conf.REGION, region_to_group=conf.REGION_TO_GROUP)
            cur_gp.execute(sql_regions, (region_group_id, ))
            region_ids = [row['region_id'] for row in cur_gp.fetchall()]
            if not region_ids:
                raise Exception("No regions in region group {} so cannot update"
                    " table".format(region_group_id))
            print("Got regions (n={:,})".format(len(region_ids)))
        else:
            region_ids = [custom_region_id, ]
        sql_grids = """\
        SELECT grid_id
        FROM {grids}
        ORDER BY grid_id  -- <----------------------------------------------
        """.format(grids=conf.GRIDS)  ##                                   |
        cur_gp.execute(sql_grids)  ##                                     |
        grid_ids = [row['grid_id'] for row in cur_gp.fetchall()]  ##      |
        print("Got grid_ids (n={:,})".format(len(grid_ids)))  ##           |
        ## associate grids and regions                                     |
        tot = 0  ##                                                        |
        i = 0  ##                                                          |
        while True:  ##                                                    |
            start_idx = i*n_rows_per_cycle  ## e.g. 0, 500, 1000 etc       |
            end_idx = ((i+1)*n_rows_per_cycle) - 1  ## subtract one so not | overlapping ranges e.g. 499, 999, 1499 etc
            try:  ##                                                       |
                min_grid12 = grid_ids[start_idx]  ## grid12s sorted of course
            except IndexError:
                break  ## exhausted all grid12's
            try:
                max_grid12 = grid_ids[end_idx]
            except IndexError:
                max_grid12 = grid_ids[-1]
            if doing_one_custom:
                sql_region_grid12s_tpl = """\
                SELECT
                reg.region_id,
                grids.grid_id
                FROM (
                  SELECT
                    grid_id,
                    rect
                  FROM {grids}
                  WHERE grid_id BETWEEN %s AND %s
                  ) AS grids
                JOIN
                (
                  SELECT region_id, geom
                  FROM {region}
                  WHERE region_id = %s
                ) AS reg
                ON reg.geom && grids.rect  -- pre-filter for performance using indexed bounding boxes
                AND ST_INTERSECTS(reg.geom, grids.rect)  -- actual intersections
                """.format(grids=conf.GRIDS, region=conf.REGION)
                cur_gp.execute(sql_region_grid12s_tpl, (min_grid12, max_grid12,
                    custom_region_id))
            elif doing_region_group:
                sql_region_grid12s_tpl = """\
                SELECT DISTINCT ON (grid_id)  -- find the one region per grid_id that has most overlap
                region_id,
                grid_id
                FROM (
                  SELECT
                  reg.region_id,
                  grids.grid_id,
                    100*(
                      ST_Area(ST_Intersection(grids.rect, reg.geom))
                      /
                      ST_Area(grids.rect)
                    )::double precision AS 
                  pct_overlap
                  FROM (
                    SELECT
                      grid_id,
                      rect
                    FROM {grids}
                    WHERE grid_id BETWEEN %s AND %s
                    AND s2_zoom = 12
                    ) AS grids
                  JOIN
                  (
                    SELECT region_id, geom
                    FROM {region}
                    INNER JOIN
                    {region_to_group}
                    USING(region_id)
                    WHERE region_group_id = %s
                  ) AS reg
                  ON reg.geom && grids.rect  -- pre-filter for performance using indexed bounding boxes
                  AND ST_INTERSECTS(reg.geom, grids.rect)  -- actual intersections
                ) AS details
                ORDER BY grid_id, pct_overlap DESC, region_id  -- we want to get the record with the highest overlap and we order by region_id to make it determinate in case of a stalemate
                """.format(grids=conf.GRIDS, region=conf.REGION,
                    region_to_group=conf.REGION_TO_GROUP)
                cur_gp.execute(sql_region_grid12s_tpl, (min_grid12, max_grid12,
                    region_group_id))
            else:
                raise Exception("Not doing custom region or a region set.")
            if debug: print(str(cur_gp.query, encoding='utf-8'))
            data = cur_gp.fetchall()
            if data:  ## possible there will be no overlaps in the batch you're looking at
                sql_insert_tpl = """\
                INSERT INTO {region_grid}
                (region_id, grid_id)
                VALUES
                """.format(region_grid=conf.REGION_GRID)
                row_tups = [(row['region_id'], row['grid_id']) for row in data]
                values_clause = ",\n".join("({}, {})".format(*row_tup)
                    for row_tup in row_tups)
                sql_insert = sql_insert_tpl + values_clause
                if debug: print(sql_insert)
                cur_gp.execute(sql_insert)
                tot += len(data)
                print(utils.prog(tot))
            else:
                print(".") #, end='')
            i += 1
        con_gp.commit()
        if destroy_first:
            db.Pg.grant_public_read_permissions(tblname=conf.REGION_GRID)
        act = 'making' if destroy_first else 'updating'
        print("Finished {} '{}'".format(act, conf.REGION_GRID))

    @staticmethod
    def check_region_grids(region_id):
        unused, cur_gp = db.Pg.gp()
        title = "Check on region_id {}".format(region_id)
        sql_ref_geom = """\
        SELECT geom
        FROM {region}
        WHERE region_id = %s
        """.format(region=conf.REGION)
        cur_gp.execute(sql_ref_geom, (region_id, ))
        ref_geom = cur_gp.fetchone()['geom']
        sql_grid_geoms = """\
        SELECT rect
        FROM {region_grid}
        INNER JOIN
        {grids}
        USING(grid_id)
        WHERE region_id = %s
        """.format(region_grid=conf.REGION_GRID, grids=conf.GRIDS)
        cur_gp.execute(sql_grid_geoms, (region_id, ))
        rect_colour = GridsRegions.rto2colour.get(region_id, 'orange')
        rects_dets = [{'rect': row['rect'], 'lbl': None, 'colour': rect_colour}
            for row in cur_gp.fetchall()]
        folium_map.Map.show_rects_on_map(cur_gp, title, rects_dets, ref_geom,
            zoom_start=7)

    @staticmethod
    def _geoms_dets(region_id):
        unused, cur_gp = db.Pg.gp()
        sql = """\
        SELECT
          geom,
            ST_ASGEOJSON(geom) AS
          geojson
        FROM {region}
        WHERE region_id = %s
        """.format(region=conf.REGION)
        cur_gp.execute(sql, (region_id, ))
        geom, geojson = cur_gp.fetchone()
        geoms_dets = [folium_map.GeomDets(geojson, '#e1ae81', 0.7,
            '', spatial.Pg.geom_centroid(cur_gp, geom), 'red')]
        return geoms_dets   

    @staticmethod
    def display_region(region_id, label):
        geoms_dets = GridsRegions._geoms_dets(region_id)
        title="Boundary map for {} region".format(label)
        unique = region_id
        folium_map.Map.show_coloured_geoms_on_map(title, unique, geoms_dets,
            zoom_start=7, report_progress=True, show_border=False,
            chart_width=REGION_PNG_MAP_WIDTH,
            chart_height=REGION_PNG_MAP_HEIGHT)

    @staticmethod
    def display_regions(region_ids=None):
        title = 'Region maps ready for pngs'
        unused, cur_gp = db.Pg.gp()
        if region_ids is None:
            region_ids = conf.RTO_IDS
        region_clause = db.Pg.nums2clause(region_ids)
        sql = """\
        SELECT region_id, label
        FROM {region}
        WHERE region_id IN {region_clause}
        ORDER BY label
        """.format(region=conf.REGION, region_clause=region_clause)
        cur_gp.execute(sql)
        html = [conf.HTML_START_TPL % {
            'title': title,
            'more_styles': """\
              .mini-map {
                float: left;
              }
            """}]
        for region_id, label in cur_gp.fetchall():
            geoms_dets = GridsRegions._geoms_dets(region_id)
            map_url = folium_map.Map.geoms_dets_to_chart(label, geoms_dets,
                zoom_start=9, show_border=False)
            chart_title = ("{} (Open frame to get custom control over size and "
                "shape)".format(label))
            html.append("""\
            <div class='mini-map'>
              <h2>{chart_title}</h2>
              <iframe class='mini-map' src='{src}'
              width={chart_width}px height={chart_height}px>
              </iframe>
            </div>
            """.format(chart_title=chart_title, src=map_url,
                chart_width=REGION_PNG_MAP_WIDTH,
                chart_height=REGION_PNG_MAP_HEIGHT))
        unique = datetime.date.today().strftime('%Y-%m-%d')
        fpath = ("{}/reports/{}{}.html".format(conf.CSM_ROOT,
            utils.Text.filenamify(title), unique))
        utils.html_opened(html, fpath)

    @staticmethod
    def check_rto_grids(region_ids=None, colour_additional_polygons=True,
            lightweight=False, region2colour=None):
        """
        colour_additional_polygons -- what is displayed in folium is limited by
        folium's geojson display capabilities. So no holes possible inside a
        coloured polygon shape. E.g. if Auckland central isthmus is separate
        from Auckland RTO we will see the surrounding Auckland RTO region
        completely cover the 'hole' that the central isthmus shape contains. The
        underlying shapes and grids are correct (no overlaps and the region with
        the biggest share of a grid square 'wins') but we can't display the
        holes. Solution - reduce opacity of surrounding region by setting
        colour_additional_polygons to False. Note -- small regions carved out of
        larger that have an external edge are handled exactly as you'd hope.

        lightweight -- instead of one rectangle for each grid square we have a
        single shape for each region e.g.
           _ _                _ _
         _|_|_|  =====>     _|   |
        |_|_|_|            |_ _ _|
        
        We want a label on each region (one only) and, when looking at lots at
        once, we want it in the middle. The lightweight map has multiple large
        polygons (inner ones to exclude lakes etc) and we want to attach label
        to the largest only. For regions made of lots of rectangles we don't
        care as it will be fine wherever it is. 
        """
        debug = False
        region2colour = region2colour if region2colour else GridsRegions.rto2colour
        unused, cur_gp = db.Pg.gp()
        title = "RTO boundaries check"
        if region_ids:
            rto_filter = 'region_id IN {}'.format(db.Pg.nums2clause(region_ids))
            WHERE_rto_filter = ' WHERE {}'.format(rto_filter)
            AND_rto_filter = ' AND {}'.format(rto_filter)
        else:
            WHERE_rto_filter = AND_rto_filter = ''
        if lightweight:
            """
            rects --> multipolygons
            We start by combining all rect's per region into single
            multipolygons using ST_UNION.

            multipolygon --> polygons
            See https://stackoverflow.com/questions/21719941/...
                ...postgis-convert-multipolygon-to-single-polygon
            Splays out to one row per polygon in multipolygon because we tell it
            we want all polygons making up the multipolygon by passing into
            ST_GEOMETRYN 1 to the number of polygons (using ST_NUMGEOMETRIES).

            polygon --> separate rings for exterior and interior
            This means all rings are separated so they can be displayed as
            individual Folium polygons without artefacts (lines criss-crossing
            from interior to exterior) using ST_DUMPRINGS. We only want the geom
            not the index thus .geom.
            """
            sql_region_rects = """\
            SELECT
            region_id,
            label,
            geom,
              ST_ASGEOJSON(geom) AS
            geojson
            FROM (
              SELECT
              region_id,
              label,
                (ST_DUMPRINGS(poly)).geom AS
              geom FROM (
                  SELECT
                  region_id,
                  label,
                    ST_GEOMETRYN(multipoly,
                      generate_series(1, ST_NUMGEOMETRIES(multipoly))) AS
                  poly
                  FROM (
                      SELECT
                      {region}.region_id,
                      {region}.label,
                        ST_UNION(rect) AS
                      multipoly
                      FROM {region_grid}
                      INNER JOIN
                      {grids}
                      USING(grid_id)
                      INNER JOIN
                      {region}
                      USING(region_id)
                      WHERE s2_zoom = 12
                      {AND_rto_filter}
                      GROUP BY {region}.region_id, {region}.label
                  ) AS qry
              ) AS qry2
            ) AS qry3
            ORDER BY region_id, ST_AREA(geom) DESC  -- so can apply label to first knowing it is the largest geom (probably the outer ring)
            """.format(region_grid=conf.REGION_GRID, region=conf.REGION,
                grids=conf.GRIDS, AND_rto_filter=AND_rto_filter)
        else:
            sql_region_rects = """\
            SELECT
            {region}.region_id,
            label,
              rect AS
            geom,
              ST_ASGEOJSON(rect) AS
            geojson
            FROM {region_grid}
            INNER JOIN
            {grids}
            USING(grid_id)
            INNER JOIN
            {region}
            USING(region_id)
            {WHERE_rto_filter}
            ORDER BY {region}.region_id
            """.format(region_grid=conf.REGION_GRID, region=conf.REGION,
                grids=conf.GRIDS, WHERE_rto_filter=WHERE_rto_filter)
        if debug: print(sql_region_rects)
        cur_gp.execute(sql_region_rects)
        geoms_dets = []
        labels = []
        for region_id, label, geom, geojson in cur_gp.fetchall():
            first4region = (label not in labels)
            if first4region or colour_additional_polygons:
                label_line2use = ('<p style="line-height: 1;">{}</p>'.format(
                    label.replace(' RTO', '<br>RTO').replace('-', '-<br>'))
                    if first4region else '')
                geoms_dets.append(folium_map.GeomDets(geojson,
                    region2colour.get(region_id, 'green'),
                    GridsRegions.rto2opacity.get(region_id, 0.8),  ## can set opacity lower in rto2opacity if we want to be able to see enclosed regions e.g. cities
                    label_line2use, spatial.Pg.geom_centroid(cur_gp, geom),
                    'red'))
                labels.append(label)
            else:
                geoms_dets.append(folium_map.GeomDets(geojson,
                    region2colour.get(region_id, 'green'), 0,  ## 0 opacity for inner portions so they aren't adding to the opacity of their area given the larger polygon already covers them and supplies some opacity
                    None, None, 'red'))
        unique = "_".join(str(x) for x in region_ids)
        ## Note - when using lightweight=True each region may have more than one
        ## geom when internal polygons e.g. harbours etc.
        ## We don't want a region label for each ;-)
        folium_map.Map.show_coloured_geoms_on_map(title, unique, geoms_dets,
            zoom_start=7, report_progress=True)

    @staticmethod
    def get_geo_dets(cur_local, region_id, geom):
        debug = False
        sql_area = "SELECT ST_AREA('{geom}') * {scalar} AS area".format(
            geom=geom, scalar=conf.AREA2KM_SCALAR)
        """
        -42.5654305996055 , 173.180651466067 BL 0
        -41.9073832665076 , 173.180651466067 TL 1 *
        -41.9073832665076 , 174.063379516939 TR 2
        -42.5654305996055 , 174.063379516939 BR 3 *
        -42.5654305996055 , 173.180651466067 BL 4
        """
        sql_coords = """\
        SELECT
          ST_Y((dp).geom) AS
        lat,
          ST_X((dp).geom) AS
        lon
        FROM (
          SELECT
            ST_DUMPPOINTS(
              ST_ENVELOPE(
                ST_TRANSFORM('{geom}'::geometry, {srid}))) AS
          dp) AS qry
        """.format(geom=geom, srid=conf.WGS84_SRID)
        cur_local.execute(sql_area)
        area = cur_local.fetchone()['area']
        cur_local.execute(sql_coords)
        if debug: print(str(cur_local.query, encoding='utf-8'))
        coords = cur_local.fetchall()
        for i, coord in enumerate(coords):
            if i == 1:  ## TL
                tl_lat, tl_lon = coord
            elif i == 3: ## BR
                br_lat, br_lon = coord
        if not (tl_lat and tl_lon and br_lat and br_lon):
            raise Exception("Unable to get bounding box main coordinates "
                "for region id {}".format(region_id))
        return area, tl_lat, tl_lon, br_lat, br_lon

    @staticmethod
    def polygon2grid_ids(raw_coords_from_app):
        """
        raw_coords_from_app -- pasted from output of polygon web app.
        """
        unused, cur_gp = db.Pg.gp()
        coords = spatial.Spatial.get_coords(raw_coords_from_app)
        geom = spatial.Pg.geom_from_coords(cur_gp, coords,
            srid=conf.WGS84_SRID)
        sql = """\
        SELECT
            grids.grid_id AS
          grid12
        FROM (
          SELECT
            grid_id,
            rect
          FROM {grids}
          ) AS grids
        JOIN
        (
          SELECT '{geom}' AS geom
        ) AS reg
        ON reg.geom && grids.rect  -- pre-filter for performance using indexed bounding boxes
        AND ST_INTERSECTS(reg.geom, grids.rect)  -- actual intersections
        ORDER BY grids.grid_id
        """.format(grids=conf.GRIDS, geom=geom)
        cur_gp.execute(sql)
        grid12s = [row['grid12'] for row in cur_gp.fetchall()]
        return grid12s

    @staticmethod
    def custom_region_grid12s(reg_dets):
        """
        reg_dets -- e.g.
        [('Auckland Airport',
        '''
        new google.maps.LatLng(-36.96574,174.77931), 
        new google.maps.LatLng(-36.96305,174.76436),
        ...
        new google.maps.LatLng(-36.96294,174.79021),
        '''),
        ..., ]
        """
        unused, cur_gp = db.Pg.gp()
        for lbl, raw_coords_from_app in reg_dets:
            grid12s = GridsRegions.polygon2grid_ids(raw_coords_from_app)
            grid_clause = db.Pg.nums2clause(grid12s)
            print("\n{}\n{}".format(lbl, grid_clause))
            unique = datetime.datetime.today().strftime("%Y%m%d")
            sql = """\
            SELECT rect
            FROM {grids}
            WHERE grid_id IN {grid_clause}
            """.format(grids=conf.GRIDS, grid_clause=grid_clause)
            cur_gp.execute(sql)
            geoms = [row['rect'] for row in cur_gp.fetchall()]
            geoms_dets = []
            for geom in geoms:
                geojson = spatial.Pg.geojson_from_geom(cur_gp, geom)
                geom_dets = folium_map.GeomDets(geojson, '#e1ae81', 0.7,
                '', spatial.Pg.geom_centroid(cur_gp, geom), 'red')
                geoms_dets.append(geom_dets)
            title = "{} custom region (Edgar)".format(lbl)
            folium_map.Map.show_coloured_geoms_on_map(title, unique,
                geoms_dets, zoom_start=7)

    @staticmethod
    def _add_custom_region(cur_local, con_rem, cur_rem, region_id, label, geom,
            link2grids=True):
        (area, tl_lat, tl_lon,
         br_lat, br_lon) = GridsRegions.get_geo_dets(cur_local, region_id, geom)
        sql_add_region = """\
        INSERT INTO {region}
        (region_id,
         label, notes,
         geom, area, tl_lat, tl_lon, br_lat, br_lon)
        VALUES (%s, %s, null, %s, %s, %s, %s, %s, %s)
        """.format(region=conf.REGION)
        cur_rem.execute(sql_add_region, (region_id, label, geom, area, tl_lat,
            tl_lon, br_lat, br_lon))
        con_rem.commit()
        print("Added custom region '{}' to '{}'".format(label, conf.REGION))
        if link2grids:
            ## add region_id - grid12 mappings
            GridsRegions.update_region_grid_tbl(custom_region_id=region_id,
                n_rows_per_cycle=1000)  ## had to dial back to avoid "psycopg2.OperationalError: insufficient memory reserved for statement"
            con_rem.commit()
            print("Added grids for custom region '{}' to '{}'".format(label,
                conf.REGION_GRID))
            GridsRegions.check_rto_grids(region_ids=[region_id, ])
        return region_id

    @staticmethod
    def add_custom_region_from_shp(cur_local, region_id, label, tblname,
            link2grids=True):
        utils.warn("""Will only work if a command like
        shp2pgsql -s 4326 ~/Documents/tourism/area2grid/storage/<shapefile name without .shp> public.<tblname> tourism | psql -d tourism -U postgres -p 5433 -h localhost
        has previously been run to populate the local pg tourism database.
        """)
        sql = "SELECT geom FROM {tblname} LIMIT 1".format(tblname=tblname)
        cur_local.execute(sql)
        geom = cur_local.fetchone()['geom']
        con_gp, cur_gp = db.Pg.gp()
        return GridsRegions._add_custom_region(cur_local, con_gp, cur_gp,
            region_id, label, geom, link2grids)

    @staticmethod
    def add_custom_region_from_polygon(cur_local, region_id, label,
            raw_coords_from_app, link2grids=True):
        con_gp, cur_gp = db.Pg.gp()
        ## add to regions table
        coords = spatial.Spatial.get_coords(raw_coords_from_app)
        geom = spatial.Pg.geom_from_coords(cur_gp, coords,
            srid=conf.WGS84_SRID)
        return GridsRegions._add_custom_region(cur_local, con_gp, cur_gp,
            region_id, label, geom, link2grids)

    @staticmethod
    def make_new_group(label, desc, notes, overlaps_ok):
        con_gp, cur_gp = db.Pg.gp()
        sql_insert = """\
        INSERT INTO {region_group}
        (label, description, notes, overlaps_ok)
        VALUES (%s, %s, %s, %s)
        """.format(region_group=conf.REGION_GROUP)
        cur_gp.execute(sql_insert, (label, desc, notes, overlaps_ok))
        con_gp.commit()
        sql_max_id = "SELECT MAX(region_group_id) AS max_id FROM {}".format(
            conf.REGION_GROUP)
        cur_gp.execute(sql_max_id)
        max_id = cur_gp.fetchone()['max_id']
        return max_id

    @staticmethod
    def regions2group(con_gp, cur_gp, region_ids, region_group_id):
        for region_id in region_ids:
            cur_gp.execute(GridsRegions.sql_insert_region_to_group,
                (region_id, region_group_id))
            con_gp.commit()

    @staticmethod
    def wipe_region_group(region_group_id):
        """
        TODO -- add check later rather than relying on user ;-)
        """
        input("Assumed regions in group are not in any other groups. Cancel if "
            "not true!")
        con_gp, cur_gp = db.Pg.gp()
        sql_region_ids_to_delete = """\
        SELECT region_id
        FROM {region_to_group}
        WHERE region_group_id = %s
        """.format(region_to_group=conf.REGION_TO_GROUP)
        cur_gp.execute(sql_region_ids_to_delete, (region_group_id, ))
        region_ids = [row['region_id'] for row in cur_gp.fetchall()]
        ## clean up region_to_group
        region_ids_clause = db.Pg.nums2clause(region_ids)
        sql_clean_region_to_group = """\
        DELETE FROM {region_to_group}
        WHERE region_id IN {region_ids_clause}
        """.format(region_to_group=conf.REGION_TO_GROUP,
            region_ids_clause=region_ids_clause)
        cur_gp.execute(sql_clean_region_to_group)
        con_gp.commit()
        ## clean up region_grids
        sql_clean_region_grids = """\
        DELETE FROM {region_grid}
        WHERE region_id IN {region_ids_clause}
        """.format(region_grid=conf.REGION_GRID,
            region_ids_clause=region_ids_clause)
        cur_gp.execute(sql_clean_region_grids)
        con_gp.commit()
        ## clean up region_group
        sql_clean_region_group = """\
        DELETE FROM {region_group}
        WHERE region_group_id = %s
        """.format(region_group=conf.REGION_GROUP)
        cur_gp.execute(sql_clean_region_group, (region_group_id, ))
        con_gp.commit()
        ## wipe regions
        sql_clean_regions = """\
        DELETE FROM {regions}
        WHERE region_id IN {region_ids_clause}
        """.format(regions=conf.REGION, region_ids_clause=region_ids_clause)
        cur_gp.execute(sql_clean_regions)
        con_gp.commit()

    @staticmethod
    def nuke_replace_all_grid_region_tables(cur_local):
        con_gp, cur_gp = db.Pg.gp()
        ## A) Grids
        Grids.make_grid_tbl(srid=conf.WGS84_SRID)
        ## A) Region groups
        GridsRegions.make_region_group_tbl()
        ## B) Regions
        ## i) RTO regions
        ## a) region table
        GridsRegions.make_region_tbl_inc_rtos(cur_local)
        sql_regions = """\
        SELECT region_id, label
        FROM {region}
        ORDER BY label
        """.format(region=conf.REGION)
        cur_gp.execute(sql_regions)
        rto_data = cur_gp.fetchall()
        pp(rto_data)
        resp = input("All RTO regions correct? (y)")
        if resp.lower() != 'y':
            print("Halting while sorting out regions")
            return
        ## b) region_to_group
        GridsRegions.make_empty_region_to_group_tbl()
        ## all RTOs join region_group_id RTO_REGION_GROUP_ID
        rto_region_ids = [row['region_id'] for row in rto_data]
        GridsRegions.regions2group(con_gp, cur_gp, rto_region_ids,
            conf.RTO_REGION_GROUP_ID)    
        ## c) grids
        GridsRegions.update_region_grid_tbl(custom_region_id=None,
            region_group_id=1, destroy_first=True, n_rows_per_cycle=2000)
        ## ii) Custom regions
        print(sorted(conf.CUSTOM_RTO_IDS))
        resp = input("All custom regions correct? (y)")
        if resp.lower() != 'y':
            print("Halting while sorting out custom regions")
            return
        ## Wairoa
        GridsRegions.add_custom_region_from_shp(cur_local,
            region_id=conf.WAIROA_ID, label='Wairoa', tblname='wairoa')
        ## Hanmer Springs
        raw_hanmer = """new google.maps.LatLng(-42.4569,172.82859), new google.maps.LatLng(-42.46703,172.81812), new google.maps.LatLng(-42.47513,172.79255), new google.maps.LatLng(-42.5224,172.7698), new google.maps.LatLng(-42.544,172.77869), new google.maps.LatLng(-42.57975,172.77658), new google.maps.LatLng(-42.57125,172.8507), new google.maps.LatLng(-42.53493,172.87262), new google.maps.LatLng(-42.50981,172.90549), new google.maps.LatLng(-42.47121,172.90378), new google.maps.LatLng(-42.45139,172.86481),"""
        GridsRegions.add_custom_region_from_polygon(cur_local,
            region_id=conf.HANMER_SPRINGS_ID, label="Hanmer Springs",
            raw_coords_from_app=raw_hanmer)
        ## Waitakeres
        GridsRegions.add_custom_region_from_shp(cur_local,
            region_id=conf.WAITAKERES_ID, label='Waitakeres',
            tblname='waitakeres_smaller_combined')
        ## Auckland CBD
        raw_cbd = """new google.maps.LatLng(-36.852,174.76421), new google.maps.LatLng(-36.85182,174.76261), new google.maps.LatLng(-36.85186,174.76191), new google.maps.LatLng(-36.85293,174.76181), new google.maps.LatLng(-36.85356,174.76238), new google.maps.LatLng(-36.85376,174.76358), new google.maps.LatLng(-36.85295,174.76462),"""
        GridsRegions.add_custom_region_from_polygon(cur_local,
            region_id=conf.AUCK_CBD_ID, label='Auckland CBD',
            raw_coords_from_app=raw_cbd)
        ## Matakana
        GridsRegions.add_custom_region_from_shp(cur_local,
            region_id=conf.MATAKANA_ID, label='Matakana',
            tblname='matakana_combined')
        ## Milford Sound
        raw_milford = """new google.maps.LatLng(-45.34056,168.0437), new google.maps.LatLng(-45.34104,167.73471), new google.maps.LatLng(-45.43315,167.6894), new google.maps.LatLng(-45.43339,168.10756), """
        GridsRegions.add_custom_region_from_polygon(cur_local,
            region_id=conf.MILFORD_SOUND_ID, label='Milford Sound',
            raw_coords_from_app=raw_milford)
        ## Waiheke Island
        GridsRegions.add_custom_region_from_shp(cur_local,
            region_id=conf.WAIHEKE_IS_ID, label='Waiheke Island',
            tblname='waiheke')
        ## Check RTO's
        GridsRegions.check_rto_grids(conf.RTO_IDS, lightweight=True)
        ## Check regions inc custom regions
        sql_custom_regions = """\
        SELECT region_id, label
        FROM {region}
        WHERE region_id >= %s
        AND region_id != %s
        ORDER BY label
        """.format(region=conf.REGION)
        cur_gp.execute(sql_custom_regions, (min(conf.CUSTOM_RTO_IDS),
            conf.KAIKOURA_RTO_ID))
        pp(cur_gp.fetchall())
        resp = input("All custom regions correct? (y)")
        if resp.lower() != 'y':
            print("Halting while sorting out custom regions")
            return
        ## C regions to LoQal group (RTOs already done)
        ## all regions join region_group_id LOQAL_REGION_GROUP_ID
        cur_gp.execute(sql_regions)
        all_data = cur_gp.fetchall()
        region_ids = [row['region_id'] for row in all_data]
        GridsRegions.regions2group(con_gp, cur_gp, region_ids,
            conf.LOQAL_REGION_GROUP_ID)  
        ## Check regions in rto region_group
        sql = """\
        SELECT label
        FROM {region}
        INNER JOIN {region_to_group}
        USING(region_id)
        WHERE region_group_id = {rto_region_group_id}
        ORDER BY label
        """.format(region=conf.REGION, region_to_group=conf.REGION_TO_GROUP,
            rto_region_group_id=conf.RTO_REGION_GROUP_ID)
        cur_gp.execute(sql)
        print("Check RTO region group")
        pp(cur_gp.fetchall())
        print("Finished making all region tables - check them!")


if __name__ == '__main__':
    con_local, cur_local = db.Pg.local()
    #GridsRegions.display_region(region_id=102, label='102')
    #GridsRegions.nuke_replace_all_grid_region_tables(cur_local)
    reg_dets = [
    ('entrance5',
     """
     new google.maps.LatLng(-40.34018,175.92553), new google.maps.LatLng(-40.35355,175.79583), new google.maps.LatLng(-40.28735,175.83367), new google.maps.LatLng(-39.84999,176.40201), new google.maps.LatLng(-39.2527,176.62576), new google.maps.LatLng(-39.27813,176.95388), new google.maps.LatLng(-39.83471,176.95073), 
     """),

    ]
    GridsRegions.custom_region_grid12s(reg_dets)
