import unittest

from shapely.geometry import MultiPoint

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db #@UnresolvedImport @UnusedImport


class Spatial():

    VERY_HIGH_KPH = 1000000
    MIN_DIST = 150

    @staticmethod
    def get_coords_centroid(coords):
        """
        Possible gotcha - lon then lat for POINT (i.e. x, y)
        """
        points = MultiPoint([(lon, lat) for lat, lon in coords])
        lon, lat = points.centroid.coords[0]
        centroid_coord = (lat, lon)
        return centroid_coord

    @staticmethod
    def crazy_leap(gap_kms, gap_mins):
        """
        A crazy leap has to be distinguished from a valid shift. E.g. it would
        be valid for a car travelling between two rural towers to pass out of
        range of one up to 50km away in the backwards direction and switch
        connection to one 50km ahead. That would look like 100km/s!

        Another case could be an air hostess travelling back and forth between
        Auckland and Hamilton. Every 20 minutes they would have travelled 130km
        i.e. 390km/h. Or Auckland and Wellington (1000km in one hour
        i.e. 1000kph).

        So some shifts will be easy to identify as unambiguous crazy leaps.
        """
        try:
            kph = gap_kms/(gap_mins/60)
        except ZeroDivisionError:
            kph = Spatial.VERY_HIGH_KPH
        possible_fake_speed = (gap_kms <= Spatial.MIN_DIST)
        faster_than_plane = (kph > 900)
        faster_than_car_but_is_not_plane = ((kph > 120)
            and (gap_mins < 20))
        seems_too_fast = (faster_than_plane
            or faster_than_car_but_is_not_plane)
        crazy_leap = (seems_too_fast and not possible_fake_speed)
        return crazy_leap

    @staticmethod
    def get_centroid_ex_crazy_leaps(coords_dets):
        """
        coords_dets - [{'coord': coord, 'dt': dt}, ...]
        """
        unused, cur_local = db.Pg.local()
        coords2use = []
        prev_dt = None
        prev_coord = None
        for coord_dets in sorted(coords_dets, key=lambda d: d['dt']):
            current_coord = coord_dets['coord']
            current_dt = coord_dets['dt']
            if prev_dt:
                gap_kms = Pg.gap_km(cur_local, current_coord, prev_coord)
                gap_mins = ((current_dt - prev_dt).seconds)/60
                if Spatial.crazy_leap(gap_kms, gap_mins):
                    continue  ## leaving the prev dt and prev coords as they were (as if this crazy record never existed) - otherwise wipe out the sane data after the crazy record because that would also have a big gap
                else:
                    coords2use.append(current_coord)
            else:
                coords2use.append(current_coord)
            prev_dt = current_dt
            prev_coord = current_coord
        return Spatial.get_coords_centroid(coords2use)

    @staticmethod
    def grid12(s2_12_id):
        """
        Credit (Big Ups?) to Daniel Glidden for thinking of this approach.

        Why bit shifting 37? Because level 12. So 30-level i.e. 30-12 i.e. 18
        Double it to get 36. And take one more because the trailing 1 is static.
        See "followed by the position of the cell in the Hilbert curve always
        followed by a “1” bit that is a marker that will identify the level of
        the cell."
        (http://blog.christianperone.com/2015/08/...
            ...googles-s2-geometry-on-the-sphere-cells-and-hilbert-curve/)

        num             7860199238725009408
        bin             110110100010101000010011001000000000000000000000000000000000000
        bin(num >> 37)  11011010001010100001001100
        num >> 37       57190476 (8 digits in this case)

        hex only removes one more digit so not worth it.
        """
        grid12 = s2_12_id >> 37
        return grid12

    @staticmethod
    def id12(grid12):
        """
        Not just a bit shift as we "truncated" 1000000000000000000000000000000000000
        so we need to bit shift the other way AND add back 2**36 (10...0). XOR
        much faster than addition. Safe to use XOR when trying to put a 1 at the
        start of a sequence of 0's.

        e.g.
        57190476 (dec)
        11011010001010100001001100 (bin)
        << 37
        110110100010101000010011000000000000000000000000000000000000000
        ^ 2**36 i.e. 1000000000000000000000000000000000000
        
        110110100010101000010011000000000000000000000000000000000000000
        000000000000000000000000001000000000000000000000000000000000000
        ---------------------------------------------------------------
        110110100010101000010011001000000000000000000000000000000000000
        i.e. 7860199238725009408 (dec) QED
        """
        id12 = (grid12 << 37) ^ 2**36
        return id12

    @staticmethod
    def get_coords(raw):
        """
        new google.maps.LatLng(-36.83182,174.91119), new google.maps.LatLng(-36.82282,174.82085), new google.maps.LatLng(-36.82371,174.74287), new google.maps.LatLng(-36.94303,174.72288), new google.maps.LatLng(-36.98171,175.00286), new google.maps.LatLng(-36.93438,175.02603), new google.maps.LatLng(-36.85737,175.06431), 
        """
        debug = False
        raw = raw.strip()
        if debug: print(raw)
        if raw.endswith(')'):
            raw += ','
        raw = raw.replace("new google.maps.LatLng", '')
        if debug: print(raw)
        raw = raw.replace("),", "|").replace("(", '')
        if debug: print(raw)
        raw = [x.split(',') for x in raw.split('|') if x != '']
        if debug: print(raw)
        coords = [tuple([float(y.strip()) for y in x]) for x in raw]
        if debug: print(coords)
        coords.append(coords[0])
        if debug: print(coords)
        return coords

    @staticmethod
    def make_nz_geom():
        con_gp, cur_gp = db.Pg.gp(user='gpadmin')
        db.Pg.drop_tbl(con_gp, cur_gp, tbl=conf.NZ)
        sql_combine_nz = """\
         SELECT nz_geom INTO {nz}
         FROM (
             SELECT ST_UNION(ST_TRANSFORM(geom, {srid})) AS
           nz_geom
           FROM {region}
           INNER JOIN
           {region_to_group} AS region_to_group
           USING(region_id)
           WHERE region_to_group.region_group_id = {rto_region_group_id}
         ) AS src
         """.format(nz=conf.NZ, srid=conf.WGS84_SRID, region=conf.REGION,
            region_to_group=conf.REGION_TO_GROUP,
            rto_region_group_id=conf.RTO_REGION_GROUP_ID)
        cur_gp.execute(sql_combine_nz)
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.NZ)
        print("Finished making '{}'".format(conf.NZ))

    @staticmethod
    def in_NZ_bb(lat, lon):
        if lat is None or lon is None:
            return False
        in_bb = (
            (conf.NZ_BB_MIN_LAT < lat < conf.NZ_BB_MAX_LAT)
            and
            (conf.NZ_BB_MIN_LON < lon < conf.NZ_BB_MAX_LON)
        )
        return in_bb


class Pg():

    @staticmethod
    def coords_from_geom(cur, geom):
        debug = False
        sql = """\
        SELECT
          ST_Y((dp).geom) AS
        lat,
          ST_X((dp).geom) AS
        lon
        FROM (
            SELECT ST_DUMPPOINTS('{geom}') AS dp
        ) AS qry
        """.format(geom=geom)
        if debug: print(sql); raise Exception
        cur.execute(sql)
        data = cur.fetchall()
        return data

    @staticmethod
    def gap_km(cur, coord_a, coord_b, srid=conf.WGS84_SRID, dp=1):
        lat_a, lon_a = coord_a
        lat_b, lon_b = coord_b
        sql_gap = """\
        SELECT ST_DISTANCE(
          ST_GeomFromText('POINT(%s %s)', {srid}),
          ST_GeomFromText('POINT(%s %s)', {srid})
        ) AS dist""".format(srid=srid)
        cur.execute(sql_gap, (lon_a, lat_a, lon_b, lat_b))  ## lon then lat for POINT
        gap_km = round(cur.fetchone()[0]*conf.ST_DISTANCE2KM_SCALAR, dp)
        return gap_km

    @staticmethod
    def geom_centroid(cur, geom):
        sql_centroid = """\
        SELECT
          ST_Y(centroid) AS
        centroid_lat,
          ST_X(centroid) AS
        centroid_lon
        FROM (
          SELECT ST_Centroid(
            '{geom}'
          ) AS
        centroid
        ) AS centroid""".format(geom=geom)
        cur.execute(sql_centroid)
        centroid_coord = cur.fetchone()
        return centroid_coord

    @staticmethod
    def get_coords_centroid(cur, coords, srid=conf.WGS84_SRID):
        """
        Possible gotcha - lon then lat for POINT (i.e. x, y)
        """
        debug = True
        point_parts = ["ST_SetSRID(ST_POINT({}, {}), {})".format(lon, lat, srid)
            for lat, lon in coords]
        points = ",\n".join(point_parts)
        sql_centroid = """\
        SELECT
          ST_Y(centroid) AS
        centroid_lat,
          ST_X(centroid) AS
        centroid_lon
        FROM (
          SELECT ST_CENTROID(
            ST_UNION({points})
          ) AS centroid_val
        ) AS centroid""".format(points=points)
        if debug: print(sql_centroid)
        cur.execute(sql_centroid)
        centroid_coord = cur.fetchone()
        return centroid_coord        

    @staticmethod
    def make_polygon_str(polygon_coords, srid=conf.WGS84_SRID):
        polygon_parts = [
            """\
            ST_makepolygon(
            ST_linefromMultipoint(
            ST_Collect(ARRAY[""",
        ]
        polygon_inner_parts = []
        for lat, lon in polygon_coords:
            polygon_inner_parts.append("ST_GeomFromText('POINT({lon} {lat})',"
                " {srid})".format(lat=lat, lon=lon, srid=srid))
        polygon_parts.append(",\n".join(polygon_inner_parts))
        polygon_parts.append("])))")
        polygon_str = "\n".join(polygon_parts)
        return polygon_str

    @staticmethod
    def geojson_from_geom(cur, geom):
        sql = """\
        SELECT
            ST_ASGEOJSON(%s) AS
          geojson
        """
        cur.execute(sql, (geom, ))
        geojson = cur.fetchone()['geojson']
        return geojson

    @staticmethod
    def geom_from_coords(cur, polygon_coords, srid=conf.WGS84_SRID):
        polygon_str = Pg.make_polygon_str(polygon_coords, srid)
        sql = """\
        SELECT {} AS geom_str
        """.format(polygon_str)
        cur.execute(sql)
        geom_str = cur.fetchone()['geom_str']
        return geom_str

    @staticmethod
    def coord_in_rto(coord, rto, srid=conf.WGS84_SRID):
        unused, cur_gp = Pg.gp()
        lat, lon = coord
        sql = """\
        SELECT
          ST_CONTAINS(
            ST_TRANSFORM(geom, {srid}),
            ST_GeomFromText('POINT({lon} {lat})', {srid})
          )
          FROM {rto_shapes}
          WHERE rto = %s
        """.format(lat=lat, lon=lon, rto_shapes=conf.RTO_SHAPES_GREENPLUM,
            srid=srid)
        cur_gp.execute(sql, (rto, ))
        in_rto = bool(cur_gp.fetchone()[0])
        return in_rto

    @staticmethod
    def lat_lon_from_nzmg(cur_local, easting, northing):
        """
        27200 is the SRID for NZMG - see SRIDs - http://www.adventurer.org.nz/..
        ... ?page=gis/QGIS_tutorials/000_Introduction/049_SRIDs
        We read them in as 27200 and then convert to 4326 (WGS84).

        SELECT 
        ST_Y(ST_TRANSFORM(ST_SetSRID(ST_POINT(2658677, 5989282), 27200), 4326)),
        ST_X(ST_TRANSFORM(ST_SetSRID(ST_POINT(2658677, 5989282), 27200), 4326));

        -41.2896352571262;174.775324698125
        Confirmed in http://apps.linz.govt.nz/coordinate-conversion
        """
        sql = """\
        SELECT 
        ST_Y(ST_TRANSFORM(ST_SetSRID(ST_POINT(%s, %s), 27200), {srid})),
        ST_X(ST_TRANSFORM(ST_SetSRID(ST_POINT(%s, %s), 27200), {srid}))
        """.format(srid=conf.WGS84_SRID)
        cur_local.execute(sql, (easting, northing, easting, northing))
        lat, lon = cur_local.fetchone()
        return lat, lon

    @staticmethod
    def nzmg_from_lat_lon(cur_local, lat, lon):
        """
        27200 is the SRID for NZMG - see SRIDs - http://www.adventurer.org.nz/..
        ... ?page=gis/QGIS_tutorials/000_Introduction/049_SRIDs
        We read them in as 27200 and then convert to 4326 (WGS84).

        SELECT 
        ST_Y(ST_TRANSFORM(ST_SetSRID(ST_POINT(-41.2896352571262, 174.775324698125), 4326), 27200)),
        ST_X(ST_TRANSFORM(ST_SetSRID(ST_POINT(-41.2896352571262, 174.775324698125), 4326), 27200));

        2658677, 5989282
        """
        sql = """\
        SELECT 
        ST_Y(ST_TRANSFORM(ST_SetSRID(ST_POINT(%s, %s), {srid}), 27200)),
        ST_X(ST_TRANSFORM(ST_SetSRID(ST_POINT(%s, %s), {srid}), 27200))
        """.format(srid=conf.WGS84_SRID)
        cur_local.execute(sql, (lon, lat, lon, lat))
        northing, easting = cur_local.fetchone()
        return easting, northing


class TestGridIdsReversible(unittest.TestCase):

    good_s2_12_ids = [
        7857954723175858176,
        7868222649950797824,
        12117467029787639808,
    ]

    def test_good(self):
        for good_s2_12_id in TestGridIdsReversible.good_s2_12_ids:
            self.assertEqual(
                Spatial.id12(Spatial.grid12(good_s2_12_id)),
                good_s2_12_id,
                ("Should be able to convert {} and reverse it back to itself"
                 .format(good_s2_12_id)))

    def test_bad(self):
        bad_s2_12_ids = [x + 1 for x in TestGridIdsReversible.good_s2_12_ids]  ## add trailing 1 so something to lose in bit shift rightwards which won't be replaced when reversing
        bad_s2_12_ids.extend([0, 1, 100, 666, 999, 1024, 1025])
        for bad_s2_12_id in bad_s2_12_ids:
            self.assertNotEqual(
                Spatial.id12(Spatial.grid12(bad_s2_12_id)),
                bad_s2_12_id,
                ("Should NOT be able to convert {} and reverse it back to "
                 "itself".format(bad_s2_12_id)))


class TestCoordIngest(unittest.TestCase):

    def test_good(self):
        tests = [
            ('''new google.maps.LatLng(-42.4569,172.82859), new google.maps.LatLng(-42.46703,172.81812), new google.maps.LatLng(-42.47513,172.79255), new google.maps.LatLng(-42.5224,172.7698), new google.maps.LatLng(-42.544,172.77869), new google.maps.LatLng(-42.57975,172.77658), new google.maps.LatLng(-42.57125,172.8507), new google.maps.LatLng(-42.53493,172.87262), new google.maps.LatLng(-42.50981,172.90549), new google.maps.LatLng(-42.47121,172.90378), new google.maps.LatLng(-42.45139,172.86481), ''',
             [
                 (-42.4569,172.82859),
                 (-42.46703,172.81812),
                 (-42.47513,172.79255),
                 (-42.5224,172.7698),
                 (-42.544,172.77869),
                 (-42.57975,172.77658),
                 (-42.57125,172.8507),
                 (-42.53493,172.87262),
                 (-42.50981,172.90549),
                 (-42.47121,172.90378),
                 (-42.45139,172.86481),
                 (-42.4569,172.82859),
             ]
            ),
        ]
        for test_input, test_output in tests:
            self.assertEqual(Spatial.get_coords(test_input), test_output)


if __name__ == '__main__':
    #pass
    unittest.main()
