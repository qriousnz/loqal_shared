# README #

### What is this repository for? ###

Supporting location analyses

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies

python3
s2sphere
psycopg2
folium
imgkit
matplotlib
selenium

postgresql with a local database and a user 'postgres'. Must be running and
receiving connections on port as defined below,

Relevant passwords and local db config in not4git.py (not supplied in repo
of course ;-))

access_local = 'password to your local postgreSQL server'
access_rem_gpadmin = 'password to Greenplum for gpadmin user'
access_rem_tnz_loc = 'password to Greenplum for tnz_loc user'

dbname_local = 'database name (needs to exist ;-))'
port_local = 5432  ## or whatever you have set it up for locally


### Who do I talk to? ###

Grant Paton-Simpson