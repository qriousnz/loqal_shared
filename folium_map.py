from collections import namedtuple
import datetime
import json
from pprint import pprint as pp
from webbrowser import open_new_tab

import folium
from selenium import webdriver

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

GeomDets = namedtuple('GeomDets',
    'geojson, area_colour, opacity, '
    'lbl_html, lbl_coord, popup_colour')  ## before import of location_sources to avoid circular import

from voyager_shared import conf, spatial, utils

DEFAULT_BLACK_THRESHOLD = 1000
DEFAULT_RED_THRESHOLD = 300
DEFAULT_ORANGE_THRESHOLD = 150


class Map():
    """
    http://folium.readthedocs.io/en/latest/quickstart.html
    https://github.com/python-visualization/folium
    """

    @staticmethod
    def standard_marker_dets_fn(n_val,
            black_threshold=DEFAULT_BLACK_THRESHOLD,
            red_threshold=DEFAULT_RED_THRESHOLD,
            orange_threshold=DEFAULT_ORANGE_THRESHOLD):
        if n_val >= black_threshold:
            colour = 'black'
            fill_opacity = 0.5
        elif n_val >= red_threshold:
            colour = 'red'
            fill_opacity = 0.4
        elif n_val >= orange_threshold:
            colour = 'orange'
            fill_opacity = 0.3
        else:
            colour = 'yellow'
            fill_opacity = 0.2
        popup = "N IMSI's = {:,}".format(n_val)
        radius = n_val
        marker_dets = {
            'colour': colour,
            'fill_opacity': fill_opacity,
            'popup': popup,
            'radius': radius}
        return marker_dets
    
    @staticmethod
    def add_map_icon_popup(map_osm, text, colour, coord):
        folium.Marker(
            coord,
            popup=text,
            icon=folium.Icon(color=colour, icon='info-sign')
        ).add_to(map_osm)

    @staticmethod
    def plot_circle(map_osm, location, radius, colour, fill_opacity=0.6,
            popup=None):
        kwargs = {'location': location, 'radius': radius, 'color': colour,
            'fill_opacity': fill_opacity, 'fill_color': colour}
        if popup:
            kwargs['popup'] = popup
        folium.CircleMarker(**kwargs).add_to(map_osm)

    @staticmethod
    def add_map_text_annotation(map_osm, text, colour, coord, fontsize=10,
            fontweight='normal', xshift=-16):
        """
        xshift: Positive moves you leftwards.
        """
        html_text = ('<div style="font-size: {}pt; font-weight: {}; '
        'color: {}">{}</div>'.format(fontsize, fontweight, colour, text))
        marker_icon = folium.features.DivIcon(
            icon_size=(450, 50),
            icon_anchor=(xshift, 0),  ## x, y from top left corner.
            html=html_text, )
        folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
            coord,
            icon=marker_icon
        ).add_to(map_osm)

    @staticmethod
    def map_rtis_by_coord(cur_local):
        debug = False
        sql = """\
        SELECT tower_lat, tower_lon, ARRAY_AGG(cell_id_rti) AS rtis
        FROM {mod_p_locations}
        GROUP BY tower_lat, tower_lon
        ORDER BY tower_lat, tower_lon
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        cur_local.execute(sql)
        data = cur_local.fetchall()
        if debug: pp(data)
        map_osm = folium.Map(location=conf.NZ_CENTRE, zoom_start=5)
        for lat, lon, rtis in data:
            colour = 'red'
            text = ', '.join(str(rti) for rti in sorted(rtis))
            Map.add_map_icon_popup(map_osm, text, colour, (lat, lon))
        fpath = "{}/reports/rtis_by_coord.html".format(conf.CSM_ROOT)
        map_url = "file://{}".format(fpath)
        map_osm.save(fpath)
        open_new_tab(map_url)

    @staticmethod
    def geoms_dets_to_chart(title, geoms_dets, use_popup_not_lbl=False,
            zoom_start=7, show_border=True, show_points=True):
        first_geom_dets = geoms_dets[0]
        first_lbl_coord = first_geom_dets.lbl_coord
        map_osm = folium.Map(location=first_lbl_coord, zoom_start=zoom_start)
        for n, geom_dets in enumerate(geoms_dets, 1):
            geo = json.loads(geom_dets.geojson)
            geo['colour'] = geom_dets.area_colour
            geo['opacity'] = geom_dets.opacity
            folium.GeoJson(data=json.dumps(geo),
                style_function=lambda x: {
                    'color' : x['geometry']['colour'],
                    'weight' : 3 if show_border else 0,
                    'opacity': 0.8 if show_border else 0,
                    'fillColor' : x['geometry']['colour'],
                    'fillOpacity': x['geometry']['opacity']
                }).add_to(map_osm)
            if show_points and geom_dets.lbl_html:
                if use_popup_not_lbl:
                    Map.add_map_icon_popup(map_osm, geom_dets.lbl_html,
                        geom_dets.popup_colour, geom_dets.lbl_coord)
                else:
                    Map.add_map_text_annotation(map_osm,
                        text=geom_dets.lbl_html, colour='b',
                        coord=geom_dets.lbl_coord, fontsize=10,
                        fontweight='bold', xshift=32)
            if n % 100 == 0:
                print(utils.prog(n, 'geom', tot=len(geoms_dets)))
        chart_name = "geom_dets mapped_{}".format(
            utils.Text.filenamify(title))
        chart_fpath = ("{}/reports/{}.html".format(conf.CSM_ROOT,
            chart_name))
        map_url = "file://{}".format(chart_fpath)
        map_osm.save(chart_fpath)
        return map_url

    @staticmethod
    def show_coloured_geoms_on_map(title, unique, geoms_dets, zoom_start=7,
            report_progress=False, show_border=True, chart_width=1200,
            chart_height=1200):
        html = [conf.HTML_START_TPL % {
            'title': title,
            'more_styles': """\
              .mini-map {
                float: left;
              }
              .shrink {
                font-size: 4px;
              }
              .rto {
                line-height: 12px;
              }
            """}]
        map_url = Map.geoms_dets_to_chart(title, geoms_dets,
            zoom_start=zoom_start, show_border=show_border)
        html.append("""\
        <div class='mini-map'>
          <h2>{title}</h2>
          <iframe class='mini-map' src='{src}'
          width={chart_width}px height={chart_height}px>
          </iframe>
        </div>
        """.format(title=title, src=map_url, chart_width=chart_width,
            chart_height=chart_height))
        fpath = ("{}/reports/{}{}.html".format(conf.CSM_ROOT,
            utils.Text.filenamify(title), unique))
        if report_progress:
            print("About to open html ...")
        utils.html_opened(html, fpath)
        return fpath

    @staticmethod
    def show_rects_on_map(cur, title, rects_dets, ref_geom=None, zoom_start=7):
        """
        rects_dets -- [{'rect': ..., 'lbl': N=10, 'colour': 'black'}, ...]
        ref_geom -- reference shape e.g. a region geom to check rects against
        """
        html = [conf.HTML_START_TPL % {
            'title': title,
            'more_styles': """\
              .mini-map {
                float: left;
              }
              .shrink {
                font-size: 4px;
              }
            """}]
        chart_width = 1200
        map_osm = folium.Map(location=conf.NZ_CENTRE, zoom_start=zoom_start)
        if ref_geom:
            folium.PolyLine(spatial.Pg.coords_from_geom(cur, ref_geom),
                color='red', weight=5, opacity=1, latlon=True, popup=None
            ).add_to(map_osm)
        for rect_dets in rects_dets:
            folium.PolyLine(spatial.Pg.coords_from_geom(cur, rect_dets['rect']),
                color=rect_dets.get('colour', 'black'),
                weight=rect_dets.get('weight', 2),
                opacity=rect_dets.get('opacity', 1), latlon=True,
                popup=rect_dets.get('lbl')
            ).add_to(map_osm)
        chart_name = "check_region_grid12_associations_{}".format(
            utils.Text.filenamify(title))
        chart_fpath = ("{}/reports/{}.html".format(conf.CSM_ROOT,
            chart_name))
        map_url = "file://{}".format(chart_fpath)
        map_osm.save(chart_fpath)
        html.append("""\
        <div class='mini-map'>
          <iframe class='mini-map' src='{src}'
          width={chart_width}px height=1200px>
          </iframe>
        </div>
        """.format(src=map_url, chart_width=chart_width))
        fpath = ("{}/reports/{}.html".format(conf.CSM_ROOT,
            utils.Text.filenamify(title)))
        utils.html_opened(html, fpath)
        return fpath

    @staticmethod
    def mapobj2url(map_osm):
        unique = datetime.datetime.now().isoformat()
        tmp_fpath = "/tmp/map_{}.html".format(unique)
        tmp_url = "file://{}".format(tmp_fpath)
        map_osm.save(tmp_fpath)
        return tmp_url

    @staticmethod
    def display(map_osm):
        tmp_fpath = "/tmp/map_peek.html"
        tmp_url = "file://{}".format(tmp_fpath)
        map_osm.save(tmp_fpath)
        browser = webdriver.Firefox()
        browser.get(tmp_url)

    @staticmethod
    def map_coord_data(title,
            lbl_a, coord_data_a,
            is_n_val, subtitle=None,
            lbl_b=None, coord_data_b=None,
            marker_dets_fn=None, icon_for_all_n_vals=False, zoom_start=5):
        """
        Display maps (or two maps side by side) showing a circle for each coord
        with a popup.

        lbl (_a and _b) -- also used in naming of chart files so mandatory if
        chart created.

        coord_data -- list of (coord, n) tuples

        is_n_val -- if coord_data has N as second item in tuples or whether a
        string.

        marker_dets_fn -- only used with n_vals. input n_vals, output dict with
        keys:
        colour e.g. 'red', fill_opacity e.g. 0.7, popup e.g. "N=154", and radius
        e.g. 4 or 6000. To use Map.standard_marker_dets_fn with different
        thresholds use partial:
        from functools import partial
        fn = partial(Map.standard_marker_dets_fn, black_threshold=10,
            red_threshold=5, orange_threshold=2)
        map_coord_data(..., marker_dets_fn=fn)

        icon_for_all_n_vals -- if n_vals, forces icon for all values, not just
        highest vals etc.
        """
        if not marker_dets_fn:
            marker_dets_fn = Map.standard_marker_dets_fn
        html = [conf.HTML_START_TPL % {
            'title': title,
            'more_styles': """\
              .mini-map {
                float: left;
              }
              .shrink {
                font-size: 4px;
              }
            """}]
        now_str = datetime.datetime.now().strftime('%Y-%m-%d')
        chart_dets = [
            (lbl_a, "{}_a".format(now_str), coord_data_a),
        ]
        if lbl_b and coord_data_b:
            chart_dets.append((lbl_b, "{}_b".format(now_str), coord_data_b))
        chart_width = 800 if len(chart_dets) == 2 else 1200
        if subtitle:
            html.append("<h2>{}</h2>".format(subtitle))
        for lbl, unique, coord_data in chart_dets:
            map_osm = folium.Map(location=conf.NZ_CENTRE, zoom_start=zoom_start)
            ## sort so that higher n_val markers appear on top of lower n_val
            if is_n_val: coord_data.sort(key=lambda t: t[1])
            for coord, coord_val in coord_data:
                if coord[0] is None or coord[1] is None:
                    raise Exception("Remove missing coordinates before "
                        "supplying coordinates for mapping - a single missing "
                        "one is enough to break everything (possibly depending "
                        "on whether it leads of not)")
                if is_n_val:
                    n_val = coord_val
                    marker_dets = marker_dets_fn(n_val)
                    Map.plot_circle(map_osm, coord,
                        radius=marker_dets['radius'],
                        colour=marker_dets['colour'],
                        fill_opacity=marker_dets['fill_opacity'],
                        popup=marker_dets['popup'])
                    if n_val > 1000 or icon_for_all_n_vals:
                        Map.add_map_icon_popup(map_osm,
                            marker_dets['popup'], marker_dets['colour'], coord)
                    elif n_val > 300:
                        Map.add_map_icon_popup(map_osm,
                            marker_dets['popup'], marker_dets['colour'], coord)
                else:
                    coord_lbl = coord_val
                    Map.add_map_icon_popup(map_osm, text=coord_lbl,
                        colour='red', coord=coord)
            chart_name = "comparison_{}_{}".format(
                utils.Text.filenamify(lbl)[:25], unique)
            chart_fpath = ("{}/reports/{}.html".format(conf.CSM_ROOT,
                chart_name))
            map_url = "file://{}".format(chart_fpath)
            map_osm.save(chart_fpath)
            html.append("""\
            <div class='mini-map'>
              <h3>{lbl}</h3>
              <iframe class='mini-map' src='{src}'
              width={chart_width}px height=1200px>
              </iframe>
            </div>
            """.format(lbl=lbl, src=map_url, chart_width=chart_width))
        fpath = ("{}/reports/{}.html".format(conf.CSM_ROOT,
            utils.Text.filenamify(title)))
        utils.html_opened(html, fpath)
        return fpath

    @staticmethod
    def show_centroids_on_map(title, coord_data, zoom_start=7):
        lbl_a = 'Centroids'

        def marker_dets_fn(s2_12_id):
            colour = 'black'
            fill_opacity = 0.5
            popup = "S2 12 id: {}".format(s2_12_id)
            radius = 1  ## N/A
            marker_dets = {
                'colour': colour,
                'fill_opacity': fill_opacity,
                'popup': popup,
                'radius': radius}
            return marker_dets

        Map.map_coord_data(title, lbl_a, coord_data, is_n_val=False,
            marker_dets_fn=marker_dets_fn, icon_for_all_n_vals=True,
            zoom_start=zoom_start)
