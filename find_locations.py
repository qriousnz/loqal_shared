import datetime
from pprint import pprint as pp
import requests
from webbrowser import open_new_tab

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, db, folium_map, spatial, utils

MAX_IMSIS_4_BEING_GAP = 50  ## should treat tiny amounts in middle of 0s as being testing associated with one end or the other. Either way, we want the "real" edge when a cell_id_rti kicks off again in force
MIN_IMSIS_TO_PLOT_COORD = 5
MAX_COORDS_TO_PLOT = 10000
MIN_IMSIS_TO_USE_AS_HERE = 100
N_HOURS_TO_SEARCH = 10


class Locator():
    """
    Based heavily on Daniel's code.
    """
    api_key = "AIzaSyBnLnNEuxl5vihsznzOR3s1kiTbmuTyzSg"
    url = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + api_key
    carrier_name = "Spark NZ"
    mcc = 530
    mnc = 5

    @staticmethod
    def _handle_resp(obj, show_map=True):
        debug = False
        response = requests.post(Locator.url, json=obj)
        data = response.json()
        if "error" not in data:
            lat = data["location"]["lat"]
            lon = data["location"]["lng"]
            cell_id = obj["cellTowers"][0]["cellId"]
            if debug: print("{}, {}, {}".format(cell_id, lat, lon))
            if show_map:
                url = "https://www.google.co.nz/maps/place/{},{}".format(lat, lon)
                print(url)
                open_new_tab(url)
        else:
            if debug:
                print("Error: {}".format(
                    response.json()["error"]["errors"][0]["reason"]))
            lat, lon = None, None
        return lat, lon

    @staticmethod
    def get_location(rti, lac_code, show_map=True):
        tower_data = [
            {
                "cellId": rti,
                "locationAreaCode": lac_code,
                "mobileCountryCode": Locator.mcc,
                "mobileNetworkCode": Locator.mnc,
                "age": 0,
            }]
        post_data = {
            "homeMobileCountryCode": Locator.mcc,
            "homeMobileNetworkCode": Locator.mnc,
            "radioType": "lte",  ## should only use LTE because we won't find a match on 3G 2**16xRNC + cell_id
            "carrier": Locator.carrier_name,
            "cellTowers": tower_data,
            "considerIp": 'false',
        }
        lat, lon = Locator._handle_resp(post_data, show_map)
        return lat, lon


class LikelyLocations():

    """
    When cell_id_rti's relocate (actually are reused in a different location)
    there will be an interruption of the otherwise near-continuous connection
    data. Unfortunately interruption of service is not sufficient to determine
    the exact times a cell_id_rti ceased to be used at one location and started
    being used at another location. There can be testing where a cell_id_rti is
    temporarily operational. What we have to do is look at the IMSI's that
    connect to any given cell_id_rti and see where the other cell_id_rti's they
    connect to are clustered. Around the origin (the known location prior to the
    interruption) or the destination (the known location in the future)? If the
    origin has a much tighter concentration than the destination then we assume
    they relocation has not yet occurred.

    Using epoch_ints a lot because queries using them instead of date strings
    are orders of magnitude faster - other than that, no reason ;-).
    """
    @staticmethod
    def _get_latest_dict():
        cells_tbl = conf.CELL_LOCATION_SEED
        utils.warn("Using {} for now - will need to switch later".format(
            cells_tbl))
        unused, cur_gp = db.Pg.gp()
        sql_latest_locations = """\
        SELECT DISTINCT ON (cell_id_rti)
        cell_id_rti,
        tower_lat,
        tower_lon
        FROM {cells}
        ORDER BY cell_id_rti, start_date DESC
        """.format(cells=cells_tbl)
        cur_gp.execute(sql_latest_locations)
        data = cur_gp.fetchall()
        latest_dict = {cell_id_rti: (tower_lat, tower_lon)
            for cell_id_rti, tower_lat, tower_lon in data}
        return latest_dict

    @staticmethod
    def _get_gap_epoch_int_bounds(cell_id_rti, min_date_str, max_date_str):
        """
        Get any significant gaps i.e. more than an hour and return them as
        [(gap_start_dt, gap_end_dt), ...]

        min_date_str -- we know the relocation happens after this.
        max_date_str -- we know the relocation happens before this. Could be as
          much as a year after min_date_str.

        Find all hours within days and identify all with 0 records. An aggregate
        query is much, much more performant than individually processing hours.
        For each block of 0s (overlooking anything <= MAX_IMSIS_4_BEING_GAP in
        the middle) look for max end date before and min start date after to get
        empty range.

        Can look at every hour and see if any IMSI's with overlapping date
        ranges for that cell_id_rti. If nothing then find max before and min
        after and we have our gap range.
        
        Note - dates are recorded as seconds since epoch start (utc obviously)
        so must convert nz-localised datetime input into utc so can convert to
        integer.
        """
        debug = True
        unused, cur_gp = db.Pg.gp()
        gaps = []
        date_strs = dates.Dates.date_strs_from_min_max_date_strs(min_date_str,
            max_date_str)
        dates_clause = "'" +  "', '".join(date_strs) + "'"
        if debug: print(date_strs)
        sql = """\
        SELECT
        year,
        month,
        day,
        hour,
          COUNT(*) AS
        freq
        FROM (
        SELECT
        date_part('year', start_date_str::timestamp) AS year,
        date_part('month', start_date_str::timestamp) AS month,
        date_part('day', start_date_str::timestamp) AS day,
        date_part('hour', start_date_str::timestamp) AS hour
        FROM lbs_agg.fact_rti_summary
        WHERE date IN ({dates_clause})
        AND cell_id_rti = {cell_id_rti}
        ) AS with_extracted
        GROUP BY year, month, day, hour
        ORDER BY year, month, day, hour
        """.format(dates_clause=dates_clause, cell_id_rti=cell_id_rti)
        zero_blocks = []
        cur_gp.execute(sql)
        data = cur_gp.fetchall()
        observed_y_m_d_hs = {(y, m, d, h): freq for y, m, d, h, freq in data}
        ## identify 0s (missing from expected records)
        expected_y_m_d_hs = dates.Dates._get_y_m_d_hs(min_date_str, max_date_str)
        block = []
        prev_y_m_d_h = None
        for expected_y_m_d_h in expected_y_m_d_hs:
            freq = observed_y_m_d_hs.get(expected_y_m_d_h, 0)
            if debug: print(expected_y_m_d_h, freq)
            if freq == 0:
                if not block:  ## if first 0, open a new block
                    block = [expected_y_m_d_h, ]
            elif freq > MAX_IMSIS_4_BEING_GAP:
                if block: ## if 0s have stopped, close open block off
                    block.append(prev_y_m_d_h)
                    zero_blocks.append(tuple(block))
                    block = []
            prev_y_m_d_h = expected_y_m_d_h
        if debug: print(zero_blocks)  ## e.g. [((2015, 3, 2, 7), (2015, 3, 2, 16)), ((2015, 3, 4, 4), (2015, 3, 4, 4)), ((2015, 3, 7, 16), (2015, 3, 7, 16)), ((2015, 3, 7, 20), (2015, 3, 8, 20)), ((2015, 3, 8, 22), (2015, 3, 9, 11))]
        sql_zero_min_bound_tpl = """\
        SELECT MAX(end_date) AS
        min_bound_epoch_int
        FROM lbs_agg.fact_rti_summary
        WHERE date IN ({{date_strs_clause}})
        AND cell_id_rti = {cell_id_rti}
        AND end_date <= %s
        """.format(cell_id_rti=cell_id_rti)
        sql_zero_max_bound_tpl = """\
        SELECT MIN(start_date) AS
        max_bound_epoch_int
        FROM lbs_agg.fact_rti_summary
        WHERE date IN ({{date_strs_clause}})
        AND cell_id_rti = {cell_id_rti}
        AND start_date >= %s
        """.format(cell_id_rti=cell_id_rti) 
        for zero_block_start, zero_block_end in zero_blocks:
            zero_block_start_date = (
                dates.Dates.get_local_datetime(*zero_block_start)
                - datetime.timedelta(days=1)).date()  ## from day before zero_block_start to day after zero_block_end
            zero_block_end_date = (dates.Dates.get_local_datetime(*zero_block_end)
                + datetime.timedelta(days=1)).date()
            date_strs = dates.Dates._date_strs_from_min_max_dates(
                min_date=zero_block_start_date, max_date=zero_block_end_date)
            date_strs_clause = "'" + "', '".join(date_strs) + "'"
            sql_zero_min_bound = sql_zero_min_bound_tpl.format(
                date_strs_clause=date_strs_clause)
            sql_zero_max_bound = sql_zero_max_bound_tpl.format(
                date_strs_clause=date_strs_clause)
            ## actual start of zero block
            start_epoch_int = dates.Dates.get_epoch_int(*zero_block_start)
            cur_gp.execute(sql_zero_min_bound, (start_epoch_int, ))
            min_bound_epoch_int = cur_gp.fetchone()['min_bound_epoch_int']
            ## actual end of zero block
            end_epoch_int = dates.Dates.get_epoch_int(*zero_block_end)
            cur_gp.execute(sql_zero_max_bound, (end_epoch_int, ))
            max_bound_epoch_int = cur_gp.fetchone()['max_bound_epoch_int']
            ## add to gaps
            gaps.append((min_bound_epoch_int, max_bound_epoch_int))
        gaps.sort(key=lambda s: s[1]-s[0], reverse=True)  ## biggest gaps first as the rest are more likely to be missing days of data, minor testing tweaks etc
        return gaps

    @staticmethod
    def _get_probable_coord(cur_local, cell_id_rti, as_at_date_for_coord,
            latest_dict=None):
        """
        Get the coords associated with the cell_id_rti as at a particular
        datetime.

        Note - using CSM history which is better than nothing (but only just).
        May be out-of-date. Will also be missing some cell_id_rti's still
        languishing in errors because not entered into Atoll yet etc.

        Try finding a value prior to the as_at_date_for_coord or, failing that,
        the earliest available. If latest_dict supplied, use that
        """
        if latest_dict:  ## if supplied use the dict - much, much faster than lots of db calls
            coord = latest_dict.get(cell_id_rti)
            return coord
        sql_as_at = """\
        SELECT tower_latitude, tower_longitude
        FROM {csm_history}
        WHERE cell_id_rti = {cell_id_rti}
        AND cob_date = (
          SELECT MAX(cob_date)
          FROM {csm_history}
          WHERE cob_date <= %s
        )
        """.format(csm_history=conf.CSM_HISTORY, cell_id_rti=cell_id_rti)
        cur_local.execute(sql_as_at, (as_at_date_for_coord, ))
        coord = cur_local.fetchone()
        if coord:
            return coord
        ## nothing - OK use earliest
        sql_earliest = """\
        SELECT tower_latitude, tower_longitude
        FROM {csm_history}
        WHERE cell_id_rti = {cell_id_rti}
        AND as_at_datetime = (
          SELECT MIN(cob_date)
          FROM {csm_history}
          WHERE cell_id_rti = {cell_id_rti}
        )
        """.format(csm_history=conf.CSM_HISTORY, cell_id_rti=cell_id_rti)
        cur_local.execute(sql_earliest)
        coord = cur_local.fetchone()
        return coord

    @staticmethod
    def other_cell_id_rtis_touched(cell_id_rti, start_epoch_int, end_epoch_int,
            cell_id_rtis_to_exclude=None):
        """
        Excluding cell_id_rtis_to_exclude, which cell_id_rti's were connected to
        by IMSI's which also connected to the cell_id_rti we are trying to time
        the relocation of?
        """
        debug = False
        unused, cur_gp = db.Pg.gp()
        ## Must return cell_id_rti, n_unique_imsi's
        min_date_str = (dates.Dates.get_datetime_from_epoch_int(start_epoch_int)
            .strftime('%Y-%m-%d'))
        max_date_str = (dates.Dates.get_datetime_from_epoch_int(end_epoch_int)
            .strftime('%Y-%m-%d'))
        date_strs = dates.Dates.date_strs_from_min_max_date_strs(min_date_str,
            max_date_str)
        dates_clause = "'" +  "', '".join(date_strs) + "'"
        sql_connected_imsis = """\
        SELECT DISTINCT imsi_rti
        FROM lbs_agg.fact_rti_summary
        WHERE date IN ({dates_clause})
        AND start_date >= {start_epoch_int}
        AND end_date <= {end_epoch_int}
        AND cell_id_rti = {cell_id_rti}
        """.format(dates_clause=dates_clause, start_epoch_int=start_epoch_int,
            end_epoch_int=end_epoch_int, cell_id_rti=cell_id_rti)
        if cell_id_rtis_to_exclude is None:
            cell_id_rtis_to_exclude = []
        cell_id_rtis_to_exclude.append(cell_id_rti)
        all_cell_id_rtis_to_exclude = db.Pg.nums2clause(cell_id_rtis_to_exclude)
        sql_rti_imsis_for_connected_imsis = """\
        SELECT DISTINCT cell_id_rti, imsi_rti
        FROM lbs_agg.fact_rti_summary
        INNER JOIN (
          {sql_connected_imsis}
        ) AS connected_imsis /* filter to IMSI's we care about */
        USING(imsi_rti)
        WHERE date IN ({dates_clause})
        AND start_date >= {start_epoch_int}
        AND end_date <= {end_epoch_int}
        AND cell_id_rti NOT IN
          {all_cell_id_rtis_to_exclude} /* ignore the cell_id_rtis we are trying to establish locations of */
        """.format(dates_clause=dates_clause,
            sql_connected_imsis=sql_connected_imsis,
            start_epoch_int=start_epoch_int, end_epoch_int=end_epoch_int,
            all_cell_id_rtis_to_exclude=all_cell_id_rtis_to_exclude)
        sql_freqs = """\
        SELECT
        cell_id_rti,
          COUNT(*) AS
        n_imsis
        FROM (
          {sql_rti_imsis_for_connected_imsis}
        ) AS associated_cell_id_rtis
        GROUP BY cell_id_rti
        ORDER BY COUNT(*) DESC, cell_id_rti
        """.format(
            sql_rti_imsis_for_connected_imsis=sql_rti_imsis_for_connected_imsis)
        if debug: print(sql_freqs)
        cur_gp.execute(sql_freqs)
        touched_data = cur_gp.fetchall()
        n_other_cell_id_rtis_touched = len(touched_data)
        capped_coord_imsis = touched_data[:MAX_COORDS_TO_PLOT]
        plotworthy_coords = [data for data in touched_data
            if data[1] >= MIN_IMSIS_TO_PLOT_COORD]
        ## use shortest
        if len(plotworthy_coords) < MAX_COORDS_TO_PLOT:
            touched_cell_id_rtis_to_use = plotworthy_coords
        else:
            touched_cell_id_rtis_to_use = capped_coord_imsis
        if debug:
            print("{:,} other cell_id_rti's touched".format(
                n_other_cell_id_rtis_touched))
            pp(touched_cell_id_rtis_to_use[:5])
            print("...")
            pp(touched_cell_id_rtis_to_use[-5:])
        return touched_cell_id_rtis_to_use

    @staticmethod
    def _n_local_imsis(cur_local, coord_imsis, ref_coord, max_dist_to_be_local):
        """
        For each coord_weight get distance from coord. If within the range
        considered local add n_imsis to n_local_imsis.
        """
        n_local_imsis = 0
        for oth_cell_coord, n_imsis in coord_imsis:
            dist = spatial.Pg.gap_km(cur_local, ref_coord, oth_cell_coord)
            if dist <= max_dist_to_be_local:
                n_local_imsis += n_imsis
        return n_local_imsis

    @staticmethod
    def _probably_here_not_there(cur_local, coord_imsis, here, there):
        """
        We conclude that the original cell_id_rti is here rather than there if
        its associated cell_id_rti's are more tightly clustered to here than
        they are to, err, there.
        """
        debug = True
        max_dist_to_be_local = spatial.Pg.gap_km(cur_local, here, there)/2  ## Excluding tourist flights e.g. from Auckland to Christchurch etc and counting according to which watershed they fall into
        here_n_local_imsis = LikelyLocations._n_local_imsis(cur_local,
            coord_imsis, ref_coord=here,
            max_dist_to_be_local=max_dist_to_be_local)
        if debug:
            print("here_n_local_imsis: {:,.0f}".format(here_n_local_imsis))
        there_n_local_imsis = LikelyLocations._n_local_imsis(cur_local,
            coord_imsis, ref_coord=there,
            max_dist_to_be_local=max_dist_to_be_local)
        if debug:
            print("there_n_local_imsis: {:,.0f}".format(there_n_local_imsis))
        if there_n_local_imsis == 0:
            probably_here_not_there = (
                here_n_local_imsis > MIN_IMSIS_TO_USE_AS_HERE)
        else:
            here2there = here_n_local_imsis/there_n_local_imsis
            probably_here_not_there = here2there > 2
        return probably_here_not_there

    @staticmethod
    def _get_associated_coord_imsis(cur_local, cell_id_rti, start_epoch_int,
            end_epoch_int, cell_id_rtis_to_exclude, latest_dict, rto=None,
            smallfry_limit=None):
        """
        rto -- if None, include all, otherwise require to be in rto boundaries.

        smallfry_limit -- ignore coordinates that don't have enough IMSI's to
        care about
        """
        debug = False
        if not smallfry_limit:
            smallfry_limit = 1
        other_cell_id_rtis_touched = (LikelyLocations
            .other_cell_id_rtis_touched(cell_id_rti, start_epoch_int,
            end_epoch_int, cell_id_rtis_to_exclude))
        coord_imsis = []
        for other_cell_id_rti, n_imsis in other_cell_id_rtis_touched:
            if n_imsis < smallfry_limit:
                continue
            start_date = (
                dates.Dates.get_datetime_from_epoch_int(start_epoch_int).date())
            other_cell_id_rti_coord = LikelyLocations._get_probable_coord(
                cur_local, other_cell_id_rti, as_at_date_for_coord=start_date,
                latest_dict=latest_dict)

            if other_cell_id_rti_coord is not None:
                if rto and not spatial.Pg.coord_in_rto(other_cell_id_rti_coord,
                        rto):
                    continue
                coord_imsis.append((other_cell_id_rti_coord, n_imsis))
            else:
                if debug:
                    print("Unable to get coord for other cell_id_rti {}".format(
                        other_cell_id_rti))
        return coord_imsis

    @staticmethod
    def _was_in_location(cur_local, cell_id_rti, start_epoch_int, end_epoch_int,
            here, rather_than_there, cell_id_rtis_to_exclude=None,
            latest_dict=False):
        """
        We look at all IMSI's connected to the supplied cell_id_rti in the
        datetime range. What other cell_id_rti's did these people also connect
        to in the datetime range?

        Once we know all the other cell_id_rti's, we can use our (admittedly
        imperfect) location information for those other cell_id_rti's to work
        out if these people were most likely clustered around the 'here'
        location rather than the 'rather_than_there' location. Even if the
        people disperse we expect the centre to be closer to the true location
        of the cell_id_rti.

        Note - for any given cell_id_rti we are questioning, we would expect all
        other cell_id_rti's apart from those in cell_id_rtis_to_exclude
        (explained below) to be more reliable on average because relocations are
        less common than stability.

        If the cell_id_rti is in a block of multiple cell_id_rti's all sharing
        the same origin datetime and location details and the same dest datetime
        and location details we should add the 'block siblings' to
        cell_id_rtis_to_exclude. It is quite common for cell_id_rti's to be
        relocated in blocks e.g. 21501, 21502, 21503, 21504, and 21505. Note -
        it is not assumed that sharing common origin and dest details mean
        cell_id_rti's actually relocated at the same datetime. There are big
        gaps in the datetimes and movement could have been staggered or
        following some other pattern. Which is why we look at each cell_id_rti
        in a block individually.

        Note - we are not looking at where the cell_id_rti's cluster per se,
        just whether they are more clustered around the origin than the dest (or
        vv depending on whether we are looking at the per-gap period or the
        post-gap period).

        The data for calculating centre is based on each person at each other
        cell_id_rti. We don't want one individual traveler to have the same
        weight as 2000 people staying put.
        """
        coord_imsis = LikelyLocations._get_associated_coord_imsis(cur_local,
            cell_id_rti, start_epoch_int, end_epoch_int,
            cell_id_rtis_to_exclude, latest_dict)
        was_in_location = LikelyLocations._probably_here_not_there(cur_local,
            coord_imsis, here, rather_than_there)
        return was_in_location, coord_imsis

    @staticmethod
    def _was_origin_then_dest(cur_local, cell_id_rti,
            end_of_pre_range_epoch_int, start_of_post_range_epoch_int,
            origin_coords, dest_coords,
            cell_id_rtis_to_exclude=None, latest_dict=None,
            n_hours2search=N_HOURS_TO_SEARCH):
        """
        Look at two time periods - one just before gap when cell_id_rti off-line
        and one just after the gap. Was the cell_id_rti in the first range
        associated with cell_id_rti's near the origin and in the second
        associated with cell_id_rti's near the destination?

        We want to look at a long enough block so that some IMSI's will connect
        to other cell_id_rti's but not so long that we have too much data to
        process. Not worried about dispersal of people even if mainly in an area
        that has people passing through rather than staying at.
        """
        debug = True
        (pre_start_epoch_int,
         pre_end_epoch_int) = dates.Dates.get_epoch_int_range(
            epoch_int=end_of_pre_range_epoch_int, hours=-n_hours2search)
        (post_start_epoch_int,
         post_end_epoch_int) = dates.Dates.get_epoch_int_range(
            epoch_int=start_of_post_range_epoch_int, hours=n_hours2search)
        was_in_origin, pre_coord_imsis = LikelyLocations._was_in_location(
            cur_local, cell_id_rti, pre_start_epoch_int, pre_end_epoch_int,
            here=origin_coords, rather_than_there=dest_coords,
            cell_id_rtis_to_exclude=cell_id_rtis_to_exclude,
            latest_dict=latest_dict)
        if debug: print("was_in_origin: {}".format(was_in_origin))
        then_in_dest, post_coord_imsis = LikelyLocations._was_in_location(
            cur_local, cell_id_rti, post_start_epoch_int, post_end_epoch_int,
            here=dest_coords, rather_than_there=origin_coords,
            cell_id_rtis_to_exclude=cell_id_rtis_to_exclude,
            latest_dict=latest_dict)
        if debug: print("then_in_dest: {}".format(then_in_dest))
        origin_then_dest = was_in_origin and then_in_dest
        return origin_then_dest, pre_coord_imsis, post_coord_imsis

    @staticmethod
    def likely_location_map(cur_local, start_datetime_str, end_datetime_str,
            rti, rtis_to_exclude=None,
            smallfry_limit=None, restrict_to_rto=None):
        """
        Show map of connections for IMSI between datetimes. Only one map and
        flexibility of datetime range.

        filter_to_rto -- if supplied, we filter so we only get coordinates which
        are inside the RTO. And we only show a map if there are some events
        within the RTO (possibly with a smallfry limit). Otherwise we report
        that there is nothing (significant) within RTO.
        """
        title = "Data relevant to locating {} between {} and {}".format(rti,
            start_datetime_str, end_datetime_str)
        if rtis_to_exclude is None:
            rtis_to_exclude = []
        latest_dict = LikelyLocations._get_latest_dict()
        start_datetime = datetime.datetime.strptime(start_datetime_str,
            '%Y-%m-%d %H:%M:%S')
        end_datetime = datetime.datetime.strptime(end_datetime_str,
            '%Y-%m-%d %H:%M:%S')
        start_epoch_int = dates.Dates.get_epoch_int(
            *start_datetime.timetuple()[:6])
        end_epoch_int = dates.Dates.get_epoch_int(
            *end_datetime.timetuple()[:6])
        lbl_a = title
        coord_data_a = LikelyLocations._get_associated_coord_imsis(cur_local,
            rti, start_epoch_int, end_epoch_int, rtis_to_exclude, latest_dict,
            rto=restrict_to_rto, smallfry_limit=smallfry_limit)
        date_str = start_datetime.date().strftime('%Y-%m-%d')
        if coord_data_a:
            if restrict_to_rto:
                print("FOUND connection between cell_id_rti {} and rto '{}' "
                    "at {} ************".format(rti, restrict_to_rto, date_str))
            fpath = folium_map.Map.map_coord_data(title, lbl_a, coord_data_a,
                is_n_val=True)
        else:
            fpath = None
            if restrict_to_rto:
                print("Cell id rti {} almost certainly not in rto '{}' at '{}'"
                    .format(rti, restrict_to_rto, date_str))
            else:
                print("Nothing to chart for {}".format(rti))
        return fpath

    @staticmethod
    def likely_location_comparison_maps_two_dates(cur_local,
            date_str_a, date_str_b,
            rti, rtis_to_exclude=None,
            smallfry_limit=None, restrict_to_rto=None):
        """
        Create 2 comparison charts - one for each day.

        Useful for comparing two dates to see if something has shifted between
        the dates.
        """
        title = "Data relevant to locating {} on both {} and {}".format(
            rti, date_str_a, date_str_b)
        if rtis_to_exclude is None:
            rtis_to_exclude = []
        latest_dict = LikelyLocations._get_latest_dict()
        datetime_a = datetime.datetime.strptime(date_str_a, '%Y-%m-%d')
        datetime_b = datetime.datetime.strptime(date_str_b, '%Y-%m-%d')
        epoch_int_a = dates.Dates.get_epoch_int(*datetime_a.timetuple()[:6])
        epoch_int_b = dates.Dates.get_epoch_int(*datetime_b.timetuple()[:6])
        day_as_secs = 24*60*60
        lbl_a = "Likely location of {} at {}".format(rti, date_str_a)
        lbl_b = "Likely location of {} at {}".format(rti, date_str_b)
        coord_data_a = LikelyLocations._get_associated_coord_imsis(cur_local,
            rti, epoch_int_a, epoch_int_a+day_as_secs,
            rtis_to_exclude, latest_dict, rto=restrict_to_rto,
            smallfry_limit=smallfry_limit)
        coord_data_b = LikelyLocations._get_associated_coord_imsis(cur_local,
            rti, epoch_int_b, epoch_int_b+day_as_secs,
            rtis_to_exclude, latest_dict, rto=restrict_to_rto,
            smallfry_limit=smallfry_limit)
        folium_map.Map.map_coord_data(title, lbl_a, unique_a=epoch_int_a,
            coord_data_a=coord_data_a, lbl_b=lbl_b, unique_b=epoch_int_b,
            coord_data_b=coord_data_b)

    @staticmethod
    def get_relocation_dets(cur_local, cell_id_rti,
            origin_coords, origin_date_str,
            dest_coords, dest_date_str,
            cell_id_rtis_to_exclude=None,
            display_maps=True, display_misses=True, use_latest=False,
            examine_inner_gaps=True):
        """
        Trying to identify relocations for rti by looking at gaps within a date
        range. Different locations on either side? Displaying maps is optional.

        The only gap we are interested in (if there is such a thing) is that
        which has the cell_id_rti being located (probably)in the origin coords
        before the gap (the relocation gap?) and located (probably) in the dest
        coords after the gap.

        origin_date_str, dest_date_str -- function as range within which to look
        for gaps so OK to set this a bit wider to be safe.

        display_maps -- almost always want this unless checking validity of code
        and wanting to look at debugging results of _was_origin_then_dest() etc.

        examine_inner_gaps -- look at all gap ranges within the overall date
        range supplied as opposed to just looking at the two dates (defining the
        date range) supplied.
        """
        if examine_inner_gaps:
            epoch_int_bounds = LikelyLocations._get_gap_epoch_int_bounds(
                cell_id_rti, min_date_str=origin_date_str,
                max_date_str=dest_date_str)
            n_hours2search = N_HOURS_TO_SEARCH
        else:
            day_end = datetime.datetime.strptime(
                origin_date_str, '%Y-%m-%d').timetuple()
            day_start = datetime.datetime.strptime(
                dest_date_str, '%Y-%m-%d').timetuple()
            day_end_origin_epoch_int = dates.Dates.get_epoch_int(
                *day_end[:3], 23, 59, 59)
            day_start_dest_epoch_int = dates.Dates.get_epoch_int(
                *day_start[:3], 0, 0, 0)
            epoch_int_bounds = [(day_end_origin_epoch_int,
                day_start_dest_epoch_int), ]
            n_hours2search = 24  ## need to examine each complete day because the changeover could happen anywhere in either day. We would expect one day overall to be more in one location than the other but the effect is diluted.
        end_of_origin_epoch_int = None
        start_of_dest_epoch_int = None
        pre_coord_imsis = None
        post_coord_imsis = None
        if use_latest:
            latest_dict = LikelyLocations._get_latest_dict()
        else:
            latest_dict = None
        for (end_of_pre_range_epoch_int,
                start_of_post_range_epoch_int) in epoch_int_bounds:
            try:
                (was_origin_then_dest, pre_coord_imsis,
                 post_coord_imsis) = LikelyLocations._was_origin_then_dest(
                    cur_local, cell_id_rti,
                    end_of_pre_range_epoch_int, start_of_post_range_epoch_int,
                    origin_coords, dest_coords,
                    cell_id_rtis_to_exclude, latest_dict, n_hours2search)
            except Exception as e:
                print(e)
                continue  ## not worth mapping anything
            end_of_pre_range_datetime_str = dates.Dates.get_datetime_from_epoch_int(
                end_of_pre_range_epoch_int).strftime('%Y-%m-%d %H:%M')
            start_of_post_range_datetime_str = dates.Dates.get_datetime_from_epoch_int(
                start_of_post_range_epoch_int).strftime('%Y-%m-%d %H:%M')
            if was_origin_then_dest:
                end_of_origin_epoch_int = end_of_pre_range_epoch_int
                start_of_dest_epoch_int = start_of_post_range_epoch_int
                if display_maps:
                    comp_title = ("Evidence of {} relocating from {} to {}"
                        .format(cell_id_rti, origin_date_str, dest_date_str))
                    lbl_a = ("Probable location of cell_id_rti {} as at {}"
                        .format(cell_id_rti, end_of_pre_range_datetime_str))
                    lbl_b = ("Probable location of cell_id_rti {} as at {}"
                        .format(cell_id_rti, start_of_post_range_datetime_str))
                    folium_map.Map.map_coord_data(comp_title, lbl_a,
                        unique_a=end_of_pre_range_epoch_int,
                        coord_data_a=pre_coord_imsis, lbl_b=lbl_b,
                        unique_b=start_of_post_range_epoch_int,
                        coord_data_b=post_coord_imsis)
                break  ## found first and presumably only instance of change
            else:
                if display_maps and display_misses:
                    comp_title = ("Data relevant to locating {} in {} or {}"
                        .format(cell_id_rti, origin_date_str, dest_date_str))
                    lbl_a = ("Locations associated with cell_id_rti {} as at {}"
                        .format(cell_id_rti, end_of_pre_range_datetime_str))
                    lbl_b = ("Locations associated with cell_id_rti {} as at {}"
                        .format(cell_id_rti, start_of_post_range_datetime_str))
                    folium_map.Map.map_coord_data(comp_title, lbl_a,
                        unique_a=end_of_pre_range_epoch_int,
                        coord_data_a=pre_coord_imsis, lbl_b=lbl_b,
                        unique_b=start_of_post_range_epoch_int,
                        coord_data_b=post_coord_imsis)
        return (end_of_origin_epoch_int, start_of_dest_epoch_int,
            pre_coord_imsis, post_coord_imsis)


if __name__ == '__main__':
    con_local, cur_local = db.Pg.local()

