#! /usr/bin/env python3

from datetime import date, datetime, timedelta
import json
#import getpass
from pprint import pprint as pp
from random import randint
from statistics import mean
from subprocess import call
import textwrap
import time
import unittest
from webbrowser import open_new_tab

import imgkit
from selenium import webdriver

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db


class Text():

    @staticmethod
    def filenamify(text):
        text = Text.space_diet(text)
        return text.replace(' ', '_'). replace("'", "")
    
    @staticmethod
    def space_diet(raw_str):
        """
        Shrink multiple spaces to single spaces.
        """
        while True:
            len_str = len(raw_str)
            raw_str = raw_str.replace('  ', ' ')
            if len(raw_str) == len_str:
                break
        return raw_str


class Json():
    """
    https://code.tutsplus.com/tutorials/...
        ...serialization-and-deserialization-of-python-objects-part-1--cms-26183
    Usage - serialized = json.dumps(complex, indent=4, cls=CustomEncoder)
    Usage - deserialized = json.loads(serialized, object_hook=decode_object)
    """

    class CustomEncoder(json.JSONEncoder):
    
        def default(self, o):
    
            if isinstance(o, datetime):
    
                return {'__datetime__': o.replace(microsecond=0).isoformat()}
    
            return {'__{}__'.format(o.__class__.__name__): o.__dict__}

    @staticmethod
    def decode_object(o):
        if '__datetime__' in o:
            return datetime.strptime(o['__datetime__'], '%Y-%m-%dT%H:%M:%S')
        return o


class TestNeedleSequenceFinder(unittest.TestCase):

    needle_seq = (1, 5, 3, 8)

    def test_ok_exact(self):
        ok_haystack_seqs = [
            (1, 5, 3, 8),
            (0, 1, 5, 3, 8, 0),
            (1, 5, 3, 8, 1, 5, 3, 8),
        ]
        for ok_haystack_seq in ok_haystack_seqs:
            self.assertTrue(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=ok_haystack_seq, exact_match=True))
 
    def test_not_ok_exact(self):
        bad_haystack_seqs = [
            (1, 5, 3),
            (0, 1, 0, 5, 0, 3, 0, 8, 0),
            (8,3,5, 1, 8,3, 5, 8, 3, 8),
            (0,6, 1, 4, 5, 1, 3, 7,9,0, 8, 1),
            (1, 5, 8, 3),
            (1, 5, 8, 0),
            (),
        ]
        for bad_haystack_seq in bad_haystack_seqs:
            self.assertFalse(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=bad_haystack_seq, exact_match=True))
 
    def test_ok_no_skip(self):
        ok_haystack_seqs = [
            (1, 5, 3, 8),
            (0, 1, 5, 3, 8, 0),
            (0, 1, 0, 5, 0, 3, 0, 8, 0),
            (1, 5, 3, 8, 1, 5, 3, 8),
            (8,3,5, 1, 8,3, 5, 8, 3, 8),
            (0,6, 1, 4, 5, 1, 3, 7,9,0, 8, 1),
        ]
        for ok_haystack_seq in ok_haystack_seqs:
            self.assertTrue(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=ok_haystack_seq, exact_match=False))
 
    def test_not_ok_no_skip(self):
        bad_haystack_seqs = [
            (1, 5, 3),
            (1, 5, 8, 3),
            (1, 5, 8, 0),
            (),
        ]
        for bad_haystack_seq in bad_haystack_seqs:
            self.assertFalse(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=bad_haystack_seq, exact_match=False))

    def test_ok_skip(self):
        ok_haystack_seqs = [
            (1, 5, 8),
            (0, 1, 3, 8, 0),
            (0, 1, 0, 0, 3, 0, 8, 0),
            (1, 5, 5, 1, 8),
            (8,3,5, 1, 8,3, 5, 8, 8),
            (0,6, 1, 4,1, 3, 7,9,0, 8, 1),
            (1, 5, 8, 3),  ## tricks some processes which jump from id to next id without taking into account end e.g. 1 then 5, then 3 then can't find 8 in what's left
        ]
        for ok_haystack_seq in ok_haystack_seqs:
            self.assertTrue(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=ok_haystack_seq, exact_match=False,
                n_mid_skips_allowed=1),
                ("Didn't find needle {} in {} when should have given 1 skip in "
                 "the middle allowed".format(
                TestNeedleSequenceFinder.needle_seq, ok_haystack_seq)))
        ## as above but more liberal about skipping so should work
        for ok_haystack_seq in ok_haystack_seqs:
            self.assertTrue(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=ok_haystack_seq, exact_match=False,
                n_mid_skips_allowed=2),
                ("Didn't find needle {} in {} when should have given 2 skip in "
                 "the middle allowed".format(
                TestNeedleSequenceFinder.needle_seq, ok_haystack_seq)))
        ## even more liberal but should still succeed even though excessively high limit allowed
        for ok_haystack_seq in ok_haystack_seqs:
            self.assertTrue(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=ok_haystack_seq, exact_match=False,
                n_mid_skips_allowed=100),
                ("Didn't find needle {} in {} when should have given 100 skip in "
                 "the middle allowed".format(
                TestNeedleSequenceFinder.needle_seq, ok_haystack_seq)))
        ok_haystack_seqs = [
            (1, 8),
            (0, 1, 7, 4, 8, 0),
        ]
        for ok_haystack_seq in ok_haystack_seqs:
            self.assertTrue(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=ok_haystack_seq, exact_match=False,
                n_mid_skips_allowed=2),
                ("Didn't find needle {} in {} when should have given 2 skip in "
                 "the middle allowed".format(
                TestNeedleSequenceFinder.needle_seq, ok_haystack_seq)))
 
    def test_not_ok_skip(self):
        bad_haystack_seqs = [
            (1, 5, 3),
            (0, 5, 3, 8),
            (),
        ]
        for bad_haystack_seq in bad_haystack_seqs:
            self.assertFalse(sequence_in_sequence(
                needle_seq=TestNeedleSequenceFinder.needle_seq,
                haystack_seq=bad_haystack_seq, exact_match=False,
                n_mid_skips_allowed=1),
                ("Should not have found needle {} in haystack {} but did!"
                .format(TestNeedleSequenceFinder.needle_seq, bad_haystack_seq)))


def sequence_in_sequence(needle_seq, haystack_seq, exact_match=True,
        n_mid_skips_allowed=0):
    """
    n_mid_skips_allowed -- if we are looking for 1,5,3,8 and it is OK if we skip
    one of them then 1,3,8 would be OK. 5,3,8 would not because only middle
    items can be skipped.
    """
    if exact_match:
        if n_mid_skips_allowed:
            raise Exception("No skips are allowed if requiring an exact match")
        needle_str = ','.join(str(x) for x in needle_seq)
        haystack_str = ','.join(str(x) for x in haystack_seq)
        found = (needle_str in haystack_str)
    else:
        ## has to have start and ends so trim off everything before first start id and after last end id. Prevents edge case where 1, 5, 8, 3 fails because goes 1, 5, 3 and then can't find an 8 after that
        n_mid_skips = 0
        try:
            idx_start = haystack_seq.index(needle_seq[0])
            idx_end = len(haystack_seq) - list(reversed(haystack_seq)).index(needle_seq[-1])  ## e.g. 0,1,5,3,4,0,0,8,0,0,0 N=11 index to include 8 is 8 which is 11 - 3 (index of 8 in reversed sequence)
        except ValueError:
            found = False
        else:
            remaining_seq = haystack_seq[idx_start:idx_end]
            found = True
            for n, needle in enumerate(needle_seq, 1):
                try:
                    idx = remaining_seq.index(needle)
                except ValueError:
                    if not n_mid_skips_allowed:
                        found = False
                        break
                    elif n in (1, len(needle_seq)):
                        ## not allowed to skip first or last
                        found = False
                        break
                    else:
                        n_mid_skips += 1
                        if n_mid_skips > n_mid_skips_allowed:
                            found = False
                            break
                        else:
                            idx = 0  ## still want to look at existing sequence
                remaining_seq = remaining_seq[idx:]
    return found

def none2empty(val):
    if val is None:
        val2return = ''
    else:
        val2return = val
    return val2return

def report_on_imsi(imsi, min_date_str, max_date_str, msg=None,
        inc_rti=True, inc_grid12=False):
    warn("Using cell_location_SEED temporarily. When changed will need to match"
        " using date range with Sentinel 9999-12-31")
    debug = False
    unused, cur_gp = db.Pg.gp()
    sql_rti_dets = """\
    SELECT
      cell_id_rti,
      cells.grid12,
      cells.start_date,
      cells.end_date,
      tower_lat,
      tower_lon,
      node_description,
      region.label AS rto
    FROM {cells} AS cells
    INNER JOIN
    {grid_region} AS grid_region
    ON cells.grid12 = grid_region.grid_id
    INNER JOIN
    {region} AS region
    USING(region_id)
    INNER JOIN
    {region_to_group}
    USING(region_id)
    WHERE region_group_id = {rto_group}
    """.format(cells=conf.CELL_LOCATION_SEED, grid_region=conf.REGION_GRID,
        region=conf.REGION, region_to_group=conf.REGION_TO_GROUP,
        rto_group=conf.RTO_REGION_GROUP_ID)
    sql = """\
    SELECT
      events.start_date_str,
      events.cell_id_rti,
      rti_dets.tower_lat,
      rti_dets.tower_lon,
      rti_dets.node_description,
      rti_dets.rto,
      rti_dets.grid12
    FROM {events} AS events
    LEFT JOIN
    ({sql_rti_dets}) AS rti_dets
    USING(cell_id_rti)
    WHERE events.imsi_rti = %s
    AND (events.date BETWEEN %s AND %s)
    AND (
      rti_dets.cell_id_rti IS NULL OR  -- or left join effectively becomes an inner join
      (
        events.date >= rti_dets.start_date
      AND
        (rti_dets.end_date IS NULL OR events.date <= rti_dets.end_date)
      )
    )
    ORDER BY events.start_date
    """.format(events=conf.CONNECTION_DATA, sql_rti_dets=sql_rti_dets)
    cur_gp.execute(sql, (imsi, min_date_str, max_date_str))
    if debug: print(str(cur_gp.query, encoding='utf-8'))
    data = cur_gp.fetchall()
    if min_date_str == max_date_str:
        date_msg = "on '{}'".format(min_date_str)
    else:
        date_msg = "between '{}' and '{}'".format(min_date_str, max_date_str)
    date_msg_for_filename = date_msg.replace("'","").replace(" ", "_")
    fname = "{}_{}.txt".format(imsi, date_msg_for_filename)
    fpath = os.path.join(conf.CSM_ROOT, 'reports', 'mass_reports',
        'indiv_reports', fname)
    tpl = "{:<26} {} {:<20} {:<20} {:<32} {:>10}"
    if inc_grid12:
        tpl += " {:>10}"
    tpl += "\n"
    with open(fpath, 'w') as f:
        f.write("{} IMSI {} {} {}\n".format('*'*4, imsi, date_msg, '*'*55))
        if msg:
            f.write("{}\n".format(msg))
        prev_date_only = None
        for row in data:
            rto = none2empty(row['rto'])
            start_date_str = row['start_date_str']
            start_date_only =  start_date_str[:10]
            if start_date_only != prev_date_only:
                f.write("\n" + "*"*40 + "\n")
            rti = none2empty(row['cell_id_rti'])
            grid12 = none2empty(row['grid12'])
            lat = none2empty(row['tower_lat'])
            lon = none2empty(row['tower_lon'])
            desc = none2empty(row['node_description'])
            args = [rto, start_date_str, lat, lon, desc, rti]
            if inc_grid12:
                args.append(grid12)
            f.write(tpl.format(*args))
            prev_date_only = start_date_only
    cmd = ['gedit', fpath]
    call(cmd)

def has_spike(vals, min_factor_times_mean=5, min_size=None):
    if len(vals) < 2:
        return False
    vals = list(vals)
    max_val = max(vals)
    vals.remove(max_val)
    mean_oth_vals = mean(vals)
    rel_spike = (max_val/mean_oth_vals >= min_factor_times_mean)
    if min_size:
        spike = rel_spike and max_val >= min_size
    else:
        spike = rel_spike
    return spike

def dates2pairs(dates):
    date_pairs = []
    for idx in range(len(dates)):
        start = dates[idx]
        try:
            end = dates[idx+1]
        except IndexError:
            break
        date_pairs.append((start, end))
    return date_pairs

def _get_month_range_dts(min_dt, max_dt):
    debug = False
    month_range_dts = []
    start_dt = date(min_dt.year, min_dt.month, 1)
    while True:
        if start_dt.month < 12:
            end_dt = (date(start_dt.year, start_dt.month+1, 1)
                - timedelta(days=1))
        else:  ## December
            end_dt = date(start_dt.year, 12, 31)  ## i.e. end of December
        if end_dt >= max_dt:
            end_dt = max_dt
            month_range_dts.append((start_dt, end_dt))
            break
        month_range_dts.append((start_dt, end_dt))
        if debug: print(start_dt, end_dt)
        start_dt = end_dt + timedelta(days=1)
    return month_range_dts

def get_month_range_dates(min_dt, max_dt):
    month_range_dts = _get_month_range_dts(min_dt, max_dt)
    month_range_dates = [
        (start_dt.strftime("%Y-%m-%d"), end_dt.strftime("%Y-%m-%d"))
        for start_dt, end_dt in month_range_dts]
    return month_range_dates

def warn(msg, width=100):
    star_len = len(msg) if len(msg) <= width else width
    msg = '\n'.join(textwrap.wrap(msg, width))
    print("*"*star_len + "\n\n{}\n\n".format(msg) + "*"*star_len)

def extract_4g_node_and_cell_id(cell_id_rti_4g):
    """
    151227189
    590731
    53
    :-)
    """
    node_id = cell_id_rti_4g//256
    cell_id = cell_id_rti_4g - (256*node_id)
    return node_id, cell_id

def prog(n, label=None, tot=None, dp_pct=1):
    label = ' ' + label if label else ''
    if tot:
        msg = "Processed {:,}{} records of {:,} ({}%)".format(n, label, tot,
            round((100*n)/tot, dp_pct))
    else:
        msg = "Processed {:,}{} records".format(n, label)
    return msg

def int2dt(dateint):
    return datetime.strptime(str(dateint), '%Y%m%d').date()

def str2dt(datestr):
    dt = datetime.strptime(datestr, '%Y%m%d').date()
    return dt

def keys2dts(event_dic, reg=False):
    if reg:
        new_event_dic = {(str2dt(k[0]), k[1]): v for k, v in event_dic.items()}
    else:
        new_event_dic = {str2dt(k): v for k, v in event_dic.items()}
    return new_event_dic

def tups2dts(lst):
    new_lst = [str2dt(x[0]) for x in lst]
    return new_lst

def html_opened(html, fpath):
    """
    html becomes file which is opened.
    """
    content = "\n".join(html)
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))

def open_map_url(map_url):
    open_new_tab(map_url)

def get_daily_tots(rti):
    unused, cur_gp = db.Pg.gp()
    sql = """\
    SELECT
    cell_id_rti,
    date,
      SUM(distinct_imsi) AS
    imsi_n
    FROM {daily_connections}
    WHERE cell_id_rti = %s
    GROUP BY cell_id_rti, date
    ORDER BY date, cell_id_rti
    """.format(daily_connections=conf.DAILY_CONNECTION_DATA)
    cur_gp.execute(sql, (rti, ))
    data = cur_gp.fetchall()
    pp(data)

def url2png(args):
    """
    https://blog.ouseful.info/2015/12/15/...
      ...grabbing-screenshots-of-folium-produced-choropleth-leaflet-maps-using-selenium/
    Using a single input to allow function to be readily consumed in async
    map.
    """
    debug = False
    ENOUGH_TILE_LOAD_TIME_DISPLAYED = 3
    ENOUGH_TILE_LOAD_TIME_HEADLESS = 2.5
    if args.get('display', False):
        browser = webdriver.Firefox()
        browser.get(args['url'])
        time.sleep(ENOUGH_TILE_LOAD_TIME_DISPLAYED)
        browser.save_screenshot(args['output'])
        browser.quit()
    else:
        inner_url = args['url']
        now = datetime.now().isoformat()
        outer_fpath = '/tmp/folium_outer_{}_{:>05}.html'.format(now,
            randint(1, 10000))  ## don't reuse - will cause problems if running async ;-)
        outer_url = 'file://' + outer_fpath
        outer_html = """
        <!DOCTYPE html>
        <head>
        <style>
        html, body {
            width: 100%%;
            height: 100%%; 
            margin: 0;
            padding: 0;
            }
        #inner {
            position: absolute;
            top: 0;
            left: 0;
            height: 800px;
            width: 1200px;
        }
        </style>
        </head>

        <body>

        <iframe id='inner' src='%s'></iframe>

        </body>
        </html>
        """ % inner_url
        with open(outer_fpath, 'w') as f:
            f.write(outer_html)
        delay_ms = int(ENOUGH_TILE_LOAD_TIME_HEADLESS*1000)
        options = {'javascript-delay': delay_ms}
        if not debug:
            options['quiet'] = ''
        imgkit.from_url(outer_url, args['output'], options=options)

if __name__ == '__main__':
    unittest.main()
    #url2png({'url': 'file:///home/gps/map_2017-10-10T11:39:24.425339.html', 'output': '/home/gps/test.png'})
    #url2png({'url': 'file:///home/gps/map.html', 'output': '/home/gps/map.png'})
    #imgkit.from_url('file:///home/gps/map.html', '/home/gps/map.png', options={'javascript-delay': 2000})
