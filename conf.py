#! /usr/bin/env python3

from collections import OrderedDict
import os
import re

VOYAGER_ROOT = '/home/gps/Documents/tourism/'
CAM_ROOT = os.path.join(VOYAGER_ROOT, 'storage', 'cam')
AREA_ROOT = os.path.join(VOYAGER_ROOT, 'area2grid')
CSM_ROOT = os.path.join(VOYAGER_ROOT, 'csm')
VALIDATION_ROOT = os.path.join(VOYAGER_ROOT, 'validation')
RR_ROOT = os.path.join(CSM_ROOT, 'redrock')
RR_DATE_SRC_FOLDERS = 'processed'
RR_DATA_BY_BAND = 'data_by_band'
RR_SRC_FOLDER = os.path.join(RR_ROOT, RR_DATE_SRC_FOLDERS)
RR_EXTRACTED_FOLDER = os.path.join(RR_ROOT, RR_DATA_BY_BAND)
REDROCK_HISTORY = 'redrock_history'
REDROCK_ERROR_HISTORY = 'redrock_error_history'
SECTOR_LOCATION_HISTORY = 'sector_location_history'
CONNECTION_DATA = 'lbs_agg.fact_rti_summary'
DAILY_CONNECTION_DATA = 'lbs_agg.fact_cell_summary'
CSM_HISTORY = 'csm'
RED_CARDED = 'red_carded'
RED_CARDED_PAIRS = 'red_carded_pairs'
YELLOW_CARDED = 'yellow_carded'
AREA2KM_SCALAR = 10000 

CSM_HISTORY_SRC = 'master.cell_sector_master_history'

## need to be under lbs_master in HIVE but no such schema in GP so using master
GRIDS = 'master.grid'
REGION_GROUP = 'master.region_group'
REGION_TO_GROUP = 'master.region_to_group'
REGION = 'master.region'
REGION_GRID = 'master.region_grid'
ACTIVE_REGION_GRID = 'master.active_region_grid'

RTO_REGION_GROUP_ID = 1
LOQAL_REGION_GROUP_ID = 2

GP_HOST = '10.100.99.121'
GP_DB = 'tnz_loc'
NZ = 'grantps_test.nz'
ORACLE_WNMS_LOCATIONS = 'oracle_wnms_locations'
MOD_P_LOCATIONS_ORIG = 'mod_p_locations_orig'
MOD_P_LOCATIONS = 'mod_p_locations'
S2_12_FREQS = 's2_12_freqs'
CSM2_GREENPLUM = 'john_test.csm2'
CSM2_UNFIXED_GREENPLUM = 'john_test.csm2_unfixed'
CELL_LOCATION = 'master.cell_location'  ## should be lbs_fact?
CELL_LOCATION_SEED = 'master.cell_location_SEED'  ## should be lbs_fact?
S2_COORDS = 's2_coords'
S2_COORDS_GREENPLUM = 'john_test.s2_coords'
EXPECTED_REGION_RTIS = 'grantps_test.expected_reg_rtis'
UNEXPECTED_REGION_RTIS = 'grantps_test.unexpected_reg_rtis'
S2_ID2INDEX_GREENPLUM = 'john_test.s2id2index'
S2_REGIONID2NAME_GREENPLUM = 'john_test.regionid2name'
RTO_SHAPES_GREENPLUM = 'ir.rto_shapes'
RTOS_TO_S2_12_IDS_GREENPLUM = 'john_test.rtos_to_s2_12_ids'
LION_POI_IMSIS = 'grantps_test.lions_poi_imsis'
SEEN_IN_DAY_LIONS = 'grantps_test.seen_in_day_for_lion_poi_imsis'
LIONS_HOME_INTL = 'grantps_test.lions_home_intl'
LIONS_HOME_DOM = 'grantps_test.lions_home_dom'
SUBSCRIBERS = 'lbs_raw.dim_subscriber'
LIONS_HOME_ORIGIN = 'grantps_test.lions_home_origin'
LIONS_VISITS = 'grantps_test.lions_visits'
CAM_ORIGIN = 'rto_raw.fact_cam_origin_data'
CAM_SURVEY = 'rto_raw.fact_cam_survey_data'
CAM_ORIGIN_TEST = 'grantps_test.cam_origin_data'
CAM_SURVEY_TEST = 'grantps_test.cam_survey_data'
FEED_RTIS = 'grantps_test.feed_rtis'
FEED_RTIS_LOCAL = 'feed_rtis'
TEMP_IMSIS = 'temp_imsis'
TOWER_PAIR_DETS = 'tower_pair_dets'

## COMMON_ appears to be a duplicate of HUA_UMTS (from the small sample examined)
LTE_PATTERN = r'HUA_LTE_(\d{12}).txt'
UMTS_PATTERN = r'HUA_UMTS_(\d{12}).txt'
LTE_ERROR_PATTERN = r'lte_excep_e_(\d{10}).txt'
UMTS_ERROR_PATTERN = r'umts_excep_e_(\d{10}).txt'
prop_lte = re.compile(LTE_PATTERN)
prop_umts = re.compile(UMTS_PATTERN)
prop_error_lte = re.compile(LTE_ERROR_PATTERN)
prop_error_umts = re.compile(UMTS_ERROR_PATTERN)
UMTS_FLD_MAPPING = {  ## 3G
    'cell_id_rti': 'cellid',
    'tower_lat': 'latitude',
    'tower_lon': 'longitude',
    'node_id': 'rncid',
    'cell_id': 'cellid',
}
LTE_FLD_MAPPING = {  ## 4G
    'cell_id_rti': 'Cell_ID',
    'tower_lat': 'Latitude',
    'tower_lon': 'Longitude',
    'node_id': 'eNB_ID',
    'cell_id': 'Sector',
}

WGS84_SRID = 4326

NZ_BB_MIN_LAT = -47
NZ_BB_MAX_LAT = -34
NZ_BB_MIN_LON = 167
NZ_BB_MAX_LON = 179
NZ_CENTRE = [-40.729765, 174.127701]
AUCKLAND_CENTRE = [-37.0036264321328, 174.791211168486]
NORTHLAND_CENTRE = [-35.568902, 173.951683]

NICE_DT_FORMAT = '%Y-%m-%d %-I:%M %p'

ST_DISTANCE2KM_SCALAR = 100
ST_DISTANCE2M_SCALAR = ST_DISTANCE2KM_SCALAR*1000

DARK_PURPLE = '#1b1b55'
LAVENDER = '#9c4992'
DUSK_GREEN = '#51a898'
MELON = '#f15950'
PALE_GREY = '#d1d1d1'
FOLIUM_COLOURS = [  ## Pop-up colours taken from docstring in folium.map.Icon. Excluded white because hard-to-see as a popup.
    ('red', 'red'), ('blue', 'blue'), ('green', 'green'), ('purple', 'purple'),
    ('orange', 'orange'), ('darkred', 'darkred'), ('#ff8c7d', 'lightred'),
    ('beige', 'beige'), ('darkblue', 'darkblue'), ('darkgreen', 'darkgreen'),
    ('cadetblue', 'cadetblue'), ('#583768', 'darkpurple'), ('pink', 'pink'),
    ('lightblue', 'lightblue'), ('lightgreen', 'lightgreen'), ('gray', 'gray'),
    ('black', 'black'), ('lightgray', 'lightgray'), ]

## cf ir.rto_shapes we consolidate 3 into Cant (Nth, Sth, ChCh) but separate out Kaikoura
## Following legacy ids which are alphabetical for RTO's other than Kaikoura and by order added for custom regions. 
AUCK_RTO_ID = 1
BOP_RTO_ID = 2
CANTERBURY_RTO_ID = 3  ## although now means something different - used to mean just ChCh
CENTRAL_OTAGO_RTO_ID = 4
CLUTHA_RTO_ID = 5
COROMANDEL_RTO_ID = 6
DUNEDIN_RTO_ID = 7
FIORDLAND_RTO_ID = 8
GISBORNE_RTO_ID = 9
HAWKES_BAY_RTO_ID = 10
KAPITI_RTO_ID = 11
MANAWATU_RTO_ID = 12
MARLBOROUGH_RTO_ID = 13
NELSON_RTO_ID = 14
## 15 was North Canterbury RTO
NORTHLAND_RTO_ID = 16
QUEENSTOWN_RTO_ID = 17
ROTORUA_RTO_ID = 18
RUAPEHU_RTO_ID = 19
## 20 was South Canterbury RTO
SOUTHLAND_RTO_ID = 21
TARANAKI_RTO_ID = 22
TAUPO_RTO_ID = 23
WAIKATO_RTO_ID = 24
WAIRARAPA_RTO_ID = 25
WAITAKI_RTO_ID = 26
LAKE_WANAKA_RTO_ID = 27
WANGANUI_RTO_ID = 28
WELLINGTON_RTO_ID = 29
WEST_COAST_RTO_ID = 30
KAWERAU_RTO_ID = 31

MILFORD_SOUND_ID = 100
MATAKANA_ID = 101
AUCK_CBD_ID = 102
WAITAKERES_ID = 103
HANMER_SPRINGS_ID = 104
WAIROA_ID = 105
KAIKOURA_RTO_ID = 106  ## added late as a custom region (which makes no sense ;-))
HELENSVILLE_ID = 107
PUKEKOHE_ID = 108
WAIHEKE_IS_ID = 109

WHANGAREI_CITY_ID = 50
AUCKLAND_CITY_ID = 51
HAMILTON_CITY_ID = 52
ROTORUA_CITY_ID = 53
WELLINGTON_CITY_ID = 54
CHRISTCHURCH_CITY_ID = 55
DUNEDIN_CITY_ID = 56

NORTHLAND_LESS_WHANGAREI_ID = 60
AUCKLAND_LESS_CENTRE_ID = 61
WAIKATO_LESS_HAMILTON_ID = 62
ROTORUA_LESS_CITY_ID = 63
WELLINGTON_LESS_CITY_ID = 64
CANT_LESS_CITY_ID = 65
DUNEDIN_LESS_CITY_ID = 66
COROMANDEL_ID = 67
BOP_ID = 68
KAWERAU_WHAKATANE_ID = 69
GISBORNE_ID = 70
TARANAKI_ID = 71
RUAPEHU_ID = 72
LAKE_TAUPO_ID = 73
HAWKES_BAY_ID = 74
WANGANUI_ID = 75
MANAWATU_ID = 76
KAPITI_ID = 77
WAIRARAPA_ID = 78
NELSON_ID = 79
MARLBOROUGH_ID = 80
KAIKOURA_ID = 81
WEST_COAST_ID = 82
FIORDLAND_ID = 83
QUEENSTOWN_ID = 84
LAKE_WANAKA_ID = 85
CENTRAL_OTAGO_ID = 86
WAITAKI_ID = 87
SOUTHLAND_ID = 88
CLUTHA_ID = 89

CUSTOM_RTO_IDS = [MILFORD_SOUND_ID, MATAKANA_ID, AUCK_CBD_ID, WAITAKERES_ID,
    HANMER_SPRINGS_ID, WAIROA_ID, HELENSVILLE_ID, PUKEKOHE_ID, WAIHEKE_IS_ID]

RTO_IDS = [AUCK_RTO_ID, BOP_RTO_ID, CANTERBURY_RTO_ID, CENTRAL_OTAGO_RTO_ID,
    CLUTHA_RTO_ID, COROMANDEL_RTO_ID, DUNEDIN_RTO_ID, FIORDLAND_RTO_ID,
    GISBORNE_RTO_ID, HAWKES_BAY_RTO_ID, KAPITI_RTO_ID, MANAWATU_RTO_ID,
    MARLBOROUGH_RTO_ID, NELSON_RTO_ID, NORTHLAND_RTO_ID, QUEENSTOWN_RTO_ID,
    ROTORUA_RTO_ID, RUAPEHU_RTO_ID, SOUTHLAND_RTO_ID, TARANAKI_RTO_ID,
    TAUPO_RTO_ID, WAIKATO_RTO_ID, WAIRARAPA_RTO_ID, WAITAKI_RTO_ID,
    LAKE_WANAKA_RTO_ID, WANGANUI_RTO_ID, WELLINGTON_RTO_ID, WEST_COAST_RTO_ID,
    KAWERAU_RTO_ID, KAIKOURA_RTO_ID, ]
'''
## when North-to-South
NORTHLAND_RTO_ID = 1
AUCK_RTO_ID = 2
COROMANDEL_RTO_ID = 3
WAIKATO_RTO_ID = 4
ROTORUA_RTO_ID = 5
BOP_RTO_ID = 6
TARANAKI_RTO_ID = 7
WANGANUI_RTO_ID = 8
RUAPEHU_RTO_ID = 9
TAUPO_RTO_ID = 10
KAWERAU_RTO_ID = 11
GISBORNE_RTO_ID = 12
HAWKES_BAY_RTO_ID = 13
MANAWATU_RTO_ID = 14
KAPITI_RTO_ID = 15
WELLINGTON_RTO_ID = 16
WAIRARAPA_RTO_ID = 17
NELSON_RTO_ID = 18
MARLBOROUGH_RTO_ID = 19
WEST_COAST_RTO_ID = 20
CANTERBURY_RTO_ID = 21
KAIKOURA_RTO_ID = 22
LAKE_WANAKA_RTO_ID = 23
WAITAKI_RTO_ID = 24
QUEENSTOWN_RTO_ID = 25
CENTRAL_OTAGO_RTO_ID = 26
DUNEDIN_RTO_ID = 27
FIORDLAND_RTO_ID = 28
SOUTHLAND_RTO_ID = 29
CLUTHA_RTO_ID = 30
'''

NORTHLAND_RTO = "Northland RTO"
AUCK_RTO = "Auckland RTO"
WAIKATO_RTO = "Waikato RTO"
COROMANDEL_RTO = "Coromandel RTO"
BOP_RTO = "Bay of Plenty RTO"
ROTORUA_RTO = "Rotorua RTO"
HAWKES_BAY_RTO = "Hawkes Bay RTO"
LAKE_TAUPO_RTO = "Lake Taupo RTO"
TARANAKI_RTO = "Taranaki RTO"
WGN_RTO = "Wellington RTO"
MARLB_RTO = "Marlborough RTO"
CANT_RTO = "Canterbury RTO"
WANAKA_RTO = "Lake Wanaka RTO"
CLUTHA_RTO = "Clutha RTO"
WEST_COAST_RTO = "West Coast RTO"
WAITAKI_RTO = "Waitaki RTO"
DUN_RTO = "Dunedin RTO"
QUEENSTOWN_RTO = "Queenstown RTO"
MILFORD_SOUND_CUSTOM = "Milford Sound"
MATAKANA_CUSTOM = "Matakana"
RTO_MAP = OrderedDict([  ## http://www.rtonz.org.nz/rto-location-map.html
    (1, AUCK_RTO),
    (2, BOP_RTO),
    (3, CANT_RTO),
    (4, "Central Otago RTO"),
    (5, CLUTHA_RTO),
    (6, COROMANDEL_RTO),
    (7, DUN_RTO),
    (8, "Fiordland RTO"),
    (9, "Gisborne RTO"),
    (10, HAWKES_BAY_RTO),
    (11, "Kapiti-Horowhenua RTO"),
    (12, "Manawatu RTO"),
    (13, MARLB_RTO),
    (14, "Nelson Tasman RTO"),
    (16, NORTHLAND_RTO),
    (17, QUEENSTOWN_RTO),
    (18, ROTORUA_RTO),
    (19, "Ruapehu RTO"),
    (21, "Southland RTO"),
    (22, TARANAKI_RTO),
    (23, LAKE_TAUPO_RTO),
    (24, WAIKATO_RTO),
    (25, "Wairarapa RTO"),
    (26, WAITAKI_RTO),
    (27, WANAKA_RTO),
    (28, "Wanganui RTO"),
    (29, WGN_RTO),
    (30, WEST_COAST_RTO),
    (31, "Kawerau-Whakatane RTO"),
    (52, "Hamilton City"),
    #(56, "Dunedin City"
    (100, MILFORD_SOUND_CUSTOM),
    (106, "Kaikoura RTO"),
    (109, "Waiheke Island"), ])

NAT_HOLS = [  ## include hols which occur during the weekend as they affect people movements anyway e.g. Christmas if different from Christmas (hols)
    ## all holidays are relevant for tourism e.g. Auckland holidays might increase numbers of tourists to Rotorua coming from Auckland
    ('20150403', 'Good Fri'),
    ('20150406', 'Easter Mon'),
    ('20150427', 'ANZAC Day'),
    ('20150601', 'Queens Bday'),
    ('20151026', 'Labour Day'),
    ('20151225', 'Christmas'),
    ('20151228', 'Boxing Day'),
    ('20151231', 'New Yrs Eve'),

    ('20160101', 'New Years'),
    ('20160104', 'After New Yrs'),
    ('20160208', 'Waitangi Day'),
    ('20160325', 'Good Fri'),
    ('20160328', 'Easter Mon'),
    ('20160425', 'ANZAC Day'),
    ('20160606', 'Queens Bday'),
    ('20161024', 'Labour Day'),
    ('20161225', 'Christmas'),
    ('20161226', 'Boxing Day'),
    ('20161227', 'Christmas (hol)'),

    ('20161231', 'New Yrs Eve'),
    ('20170101', 'New Yrs Day'),
    ('20170102', 'After New Yrs'),
    ('20170103', 'New Years (hol)'),
    ('20170206', 'Waitangi Day'),
    ('20170414', 'Good Fri'),
    ('20170417', 'Easter Mon'),
    ('20170425', 'ANZAC Day'),
    ('20170605', 'Queens Bday'),
    ('20171023', 'Labour Day'),
    ('20171225', 'Christmas'),
    ('20171226', 'Boxing Day'),
    ('20171227', 'Christmas (hol)'),  ## Keep adding from http://publicholiday.co.nz/nz-public-holidays-2017.html
]

REG_HOLS = [
    ## all holidays are relevant for tourism e.g. Auckland holidays might increase numbers of tourists to Rotorua coming from Auckland
    ('20150309', 'Taranaki'),
    ('20150323', 'Otago'),
    ('20150407', 'Southland'),
    ('20150928', 'South Cant.'),
    ('20151023', 'Hawkes Bay'),
    ('20151102', 'Marlborough'),
    ('20151113', 'Canterbury'),
    ('20151130', 'Westland'),

    ('20160125', 'Wellington'),
    ('20160201', 'Auckland'),
    ('20160201', 'Nelson'),
    ('20160314', 'Taranaki'),
    ('20160321', 'Otago'),
    ('20160329', 'Southland'),
    ('20160926', 'South Cant.'),
    ('20161021', 'Hawkes Bay'),
    ('20161031', 'Marlborough'),
    ('20161111', 'Canterbury'),
    ('20161128', 'Westland'),

    ('20170123', 'Wellington'),
    ('20170130', 'Auckland'),
    ('20170130', 'Nelson'),
    ('20170313', 'Taranaki'),
    ('20170320', 'Otago'),
    ('20170418', 'Southland'),
    ('20170925', 'South Cant.'),
    ('20171020', 'Hawkes Bay'),
    ('20171030', 'Marlborough'),
    ('20171117', 'Canterbury'),
    ('20171204', 'Westland'),
     ## Keep adding from http://publicholiday.co.nz/nz-public-holidays-2017.html
]

NON_DAYLIGHT_SAVINGS = [  ## http://publicholiday.co.nz/nz-public-holidays-2015.html etc
    ('20150405', '20150927'),
    ('20160403', '20160925'),
    ('20170402', '20170924'),
    ('20180401', '20180930'),
]

## National events - including holidays (note - holidays in one region impact on others so seen as national events), major national disaster such as Kaikoura earthquake
NAT_EVENTS = {
    '20160901': [{'lbl': 'RTI--> Trucall', 'icon_name': 'lightning'}, ],
    '20161114': [{'lbl': 'Kaikoura Quake', 'icon_name': 'lightning'}, ],
    '20170129': [{'lbl': 'Spark Outage', 'icon_name': 'lightning'}, ],
}

## Regional events
cruise_reg_events = {  ## adding image config all at once at end to simplify configuration
    ## Cruise ship visits - note - date is approx apart from for the Napier port
    ## https://web.archive.org/web/20150210105241/http://napierport.co.nz/shipping-info/cruise-schedule/
    ## ETA 	        Time 	ETD 	        Time 	Vessel 	        Agent 	Operator 	        Star Rating 	From 	   To 	        Pass 	Crew 	Berth
    ## 7-Feb-2015 	11.00 	17-Feb-2015 	19.00 	Dawn Princess 	ISS 	Princess Cruise Line 	4        	Tauranga   Wellington 	1950 	900 	2S
    ## Note - some "confirmed" cruises are in the Napier page on some dates and not others so presumably plans can actually change
    ## Cruises as confirmed Feb 2015 https://web.archive.org/web/20150210105241/http://napierport.co.nz/shipping-info/cruise-schedule/
    ('20150302', BOP_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20150302', HAWKES_BAY_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20150302', WGN_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    
    ('20150310', BOP_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20150310', HAWKES_BAY_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20150310', WGN_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    
    ('20150313', BOP_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20150313', HAWKES_BAY_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20150313', WGN_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    
    ('20150315', BOP_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20150315', HAWKES_BAY_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20150315', WGN_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    
    ('20150324', BOP_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20150324', HAWKES_BAY_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20150324', WGN_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    
    ('20150326', BOP_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20150326', HAWKES_BAY_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20150326', WGN_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    
    ('20150403', BOP_RTO): [{'lbl': '3K (Oosterdam)'}, ],
    ('20150403', HAWKES_BAY_RTO): [{'lbl': '3K (Oosterdam)'}, ],
    ('20150403', WGN_RTO): [{'lbl': '3K (Oosterdam)'}, ],
    
    ('20150407', BOP_RTO): [{'lbl': '6K (2 ships)'}, ],
    ('20150407', HAWKES_BAY_RTO): [{'lbl': '6K (2 ships)'}, ],
    ('20150407', WGN_RTO): [{'lbl': '6K (2 ships)'}, ],
    
    ('20150408', BOP_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20150408', HAWKES_BAY_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20150408', WGN_RTO): [{'lbl': '3K (Sun Princess)'}, ],

    ## Cruises as confirmed Sept 2015 https://web.archive.org/web/20150922172938/http://www.napierport.co.nz/shipping-info/cruise-schedule/
    ('20151017', BOP_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20151017', HAWKES_BAY_RTO): [{'lbl': '3K (Sun Princess)'}, ],
    ('20151017', WGN_RTO): [{'lbl': '3K (Sun Princess)'}, ],

    ('20151106', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151106', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151106', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20151110', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151110', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151110', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ## Cruises as confirmed Nov 2015 https://web.archive.org/web/20151119053206/http://www.napierport.co.nz/shipping-info/cruise-schedule 
    ('20151129', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151129', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151129', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],
    
    ('20151203', BOP_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    ('20151203', HAWKES_BAY_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    ('20151203', WGN_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],

    ('20151223', BOP_RTO): [{'lbl': '4K (Voyager of the Seas)'}, ],
    ('20151223', HAWKES_BAY_RTO): [{'lbl': '4K (Voyager of the Seas)'}, ],
    ('20151223', WGN_RTO): [{'lbl': '4K (Voyager of the Seas)'}, ],

    ('20151226', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151226', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20151226', MARLB_RTO): [{'lbl': '3K (Noordam)'}, ],  ## Yep - Picton for once

    ('20160105', BOP_RTO): [{'lbl': '3K (Carnival Legend)'}, ],
    ('20160105', HAWKES_BAY_RTO): [{'lbl': '3K (Carnival Legend)'}, ],
    ('20160105', WGN_RTO): [{'lbl': '3K (Carnival Legend)'}, ],

    ('20161111', BOP_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20161111', HAWKES_BAY_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20161111', WGN_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],

    ('20160114', BOP_RTO): [{'lbl': '4.5K (Voyager of the Seas)'}, ],
    ('20160114', HAWKES_BAY_RTO): [{'lbl': '4.5K (Voyager of the Seas)'}, ],
    ('20160114', WGN_RTO): [{'lbl': '4.5K (Voyager of the Seas)'}, ],

    ('20160122', BOP_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20160122', HAWKES_BAY_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20160122', WGN_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],

    ('20160123', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160123', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160123', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20160131', BOP_RTO): [{'lbl': '3K (Legend of the Seas)'}, ],
    ('20160131', HAWKES_BAY_RTO): [{'lbl': '3K (Legend of the Seas)'}, ],
    ('20160131', WGN_RTO): [{'lbl': '3K (Legend of the Seas)'}, ],

    ('20160211', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160211', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160211', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20160215', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160215', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160215', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20160220', BOP_RTO): [{'lbl': '2.5K (Pacific Pearl)'}, ],
    ('20160220', HAWKES_BAY_RTO): [{'lbl': '2.5K (Pacific Pearl)'}, ],
    ('20160220', WGN_RTO): [{'lbl': '2.5K (Pacific Pearl)'}, ],

    ('20160229', BOP_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20160229', HAWKES_BAY_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20160229', WGN_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],

    ('20160303', BOP_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    ('20160303', HAWKES_BAY_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    ('20160303', WGN_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],

    ('20160310', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160310', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160310', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20160314', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160314', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20160314', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20160417', BOP_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20160417', HAWKES_BAY_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],
    ('20160417', WGN_RTO): [{'lbl': '4.5K (Explorer of the Seas)'}, ],

    ('20161023', BOP_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],
    ('20161023', HAWKES_BAY_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],
    ('20161023', MARLB_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],  ## Picton

    ('20161101', BOP_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20161101', HAWKES_BAY_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20161101', WGN_RTO): [{'lbl': '3K (Sea Princess)'}, ],

    ('20161104', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161104', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161104', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20161104', BOP_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20161104', HAWKES_BAY_RTO): [{'lbl': '3K (Sea Princess)'}, ],
    ('20161104', WGN_RTO): [{'lbl': '3K (Sea Princess)'}, ],

    ('20161106', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161106', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161106', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20161110', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161110', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161110', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20161118', BOP_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    ('20161118', HAWKES_BAY_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    ('20161118', WGN_RTO): [{'lbl': '2K (Pacific Pearl)'}, ],
    
    ('20161120', BOP_RTO): [{'lbl': '2K (Pacific Aria)'}, ],
    ('20161120', HAWKES_BAY_RTO): [{'lbl': '2K (Pacific Aria)'}, ],
    ('20161120', WGN_RTO): [{'lbl': '2K (Pacific Aria)'}, ],

    ('20161122', BOP_RTO): [{'lbl': '4K (Emerald Princess)'}, ],
    ('20161122', HAWKES_BAY_RTO): [{'lbl': '4K (Emerald Princess)'}, ],
    ('20161122', WGN_RTO): [{'lbl': '4K (Emerald Princess)'}, ],

    ('20161129', BOP_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20161129', HAWKES_BAY_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20161129', WGN_RTO): [{'lbl': '2K (Maasdam)'}, ],

    ('20161203', BOP_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20161203', HAWKES_BAY_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20161203', CANT_RTO): [{'lbl': '3K (Dawn Princess)'}, ],  ## Akaroa

    ('20161205', BOP_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20161205', HAWKES_BAY_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20161205', WGN_RTO): [{'lbl': '2K (Maasdam)'}, ],

    ('20161215', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161215', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20161215', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20161216', BOP_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20161216', HAWKES_BAY_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20161216', CANT_RTO): [{'lbl': '3K (Dawn Princess)'}, ],  ## Akaroa

    ('20161218', BOP_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],
    ('20161218', HAWKES_BAY_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],
    ('20161218', WGN_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],

    ## https://web.archive.org/web/20161015035025/http://napierport.co.nz/shipping-info/cruise-schedule/

    ('20161230', BOP_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20161230', HAWKES_BAY_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20161230', WGN_RTO): [{'lbl': '2K (Maasdam)'}, ],

    ('20160101', BOP_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],
    ('20160101', HAWKES_BAY_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],
    ('20160101', WGN_RTO): [{'lbl': '3K (Radiance of the Seas)'}, ],

    ('20170104', BOP_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20170104', HAWKES_BAY_RTO): [{'lbl': '2K (Maasdam)'}, ],
    ('20170104', WGN_RTO): [{'lbl': '2K (Maasdam)'}, ],

    ('20170105', WGN_RTO): [{'lbl': '5.5K (Ovation of the Seas)'}, ],
    ('20170105', HAWKES_BAY_RTO): [{'lbl': '5.5K (Ovation of the Seas)'}, ],
    ('20170105', MARLB_RTO): [{'lbl': '5.5K (Ovation of the Seas)'}, ],  ## Picton

    ('20170110', BOP_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20170110', HAWKES_BAY_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20170110', WGN_RTO): [{'lbl': '3K (Dawn Princess)'}, ],

    ('20170112', BOP_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20170112', HAWKES_BAY_RTO): [{'lbl': '3K (Noordam)'}, ],
    ('20170112', WGN_RTO): [{'lbl': '3K (Noordam)'}, ],

    ('20170121', BOP_RTO): [{'lbl': '3K (Legend of the Seas)'}, ],
    ('20170121', HAWKES_BAY_RTO): [{'lbl': '3K (Legend of the Seas)'}, ],
    ('20170121', WGN_RTO): [{'lbl': '3K (Legend of the Seas)'}, ],

    ('20170123', BOP_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20170123', HAWKES_BAY_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
    ('20170123', WGN_RTO): [{'lbl': '3K (Dawn Princess)'}, ],
}

concert_reg_events = {
    ('20151019', AUCK_RTO): [{'lbl': 'Neil Diamond (Auck)', 'icon': 'music'}, ],
    ('20151024', DUN_RTO): [{'lbl': 'Neil Diamond (Dun)', 'icon': 'music'}, ],
    ('20161203', AUCK_RTO): [{'lbl': 'Coldplay', 'icon': 'music'}, ],
    ('20151212', AUCK_RTO): [{'lbl': 'Ed Sheeran', 'icon': 'music'}, ],
    ('20150512', AUCK_RTO): [{'lbl': 'Back St Boys', 'icon': 'music'}, ],
    ('20170204', AUCK_RTO): [{'lbl': "Guns'n'Roses", 'icon': 'music'}, ],
    ('20170318', AUCK_RTO): [{'lbl': "Justin Bieber", 'icon': 'music'}, ],
    ('20150131', QUEENSTOWN_RTO): [{'lbl': 'Winery Concert'}, ],
    ('20160123', QUEENSTOWN_RTO): [{'lbl': 'Winery Concert'}, ],
    ('20170121', QUEENSTOWN_RTO): [{'lbl': 'Winery Concert'}, ],
}

def add_icon_name(reg_events_dict, icon_name):
    new_events_dict = {}
    for date_reg, labels_dets in reg_events_dict.items():
        for label_dets in labels_dets:
            label_dets.update({'icon_name': icon_name})
        new_events_dict[(date_reg[0], date_reg[1])] = labels_dets
    return new_events_dict

CRUISE_REG_EVENTS = add_icon_name(cruise_reg_events, 'anchor')
CONCERT_REG_EVENTS = add_icon_name(concert_reg_events, 'music')

## Can add custom region even if within an RTO - we just look up by region and date to get data
GEN_REG_EVENTS = {  ## e.g. major concerts, regional disasters (e.g. localised fires on Coromandel Peninsula)
    ('20160216', HAWKES_BAY_RTO): [{'lbl': 'Art Deco Festival (16-22)'}, ],
    ('20160301', HAWKES_BAY_RTO): [{'lbl': 'Horse of the Year (1-6)'}, ],
    ('20160204', ROTORUA_RTO): [{'lbl': 'Tarawera Ultramarathon (4-7)'}, ],
    ('20160309', ROTORUA_RTO): [{'lbl': 'Crankworx (9-13)'}, ],
    ('20160214', CANT_RTO): [{'lbl': 'Christchurch Quake', 'icon': 'lightning'}, ],
    ('20151121', QUEENSTOWN_RTO): [{'lbl': 'Air NZ Marathon'}, ],
    ('20161119', QUEENSTOWN_RTO): [{'lbl': 'Air NZ Marathon'}, ],
    ('20171118', QUEENSTOWN_RTO): [{'lbl': 'Air NZ Marathon'}, ],
    ('20150610', WAIKATO_RTO): [{'lbl': 'Nat Ag Fieldays (10-13)'}],  ## http://www.fieldays.co.nz/fieldaysdates
    ('20160615', WAIKATO_RTO): [{'lbl': 'Nat Ag Fieldays (15-18)'}],
    ('20170614', WAIKATO_RTO): [{'lbl': 'Nat Ag Fieldays (14-17)'}],
    ('20180613', WAIKATO_RTO): [{'lbl': 'Nat Ag Fieldays (13-16)'}],
    ('20190612', WAIKATO_RTO): [{'lbl': 'Nat Ag Fieldays (12-15)'}],
    ('20200610', WAIKATO_RTO): [{'lbl': 'Nat Ag Fieldays (10-13)'}],
}

HTML_START_TPL = """\
<!DOCTYPE html>
<html>
<head>
<title>%(title)s</title>
<style>
body {
  padding: 0;
  margin: 0;
  font-size: 12px;
  font-family: "Averta Bold";
}
#content {
  padding: 12px;
}
#banner {
  background-color: #09002b;
  background-image: url("images/format/Qrious_White.png");
  background-repeat: no-repeat;
  background-size: 130px;
  background-position: 12px 50%%;
  height: 65px;
}
#banner-right {
  margin: 0;
  padding: 0;
  background-image: url("images/format/qrious_swirls.png");
  background-repeat: no-repeat;
  background-position: 140px 50%%;
  height: 65px;
}
h1, h2 {
  color: #241c54;
}
%(more_styles)s
</style>
</head>
<body>
<div id='banner'><div id='banner-right'></div></div>
<div id='content'>
<h1>%(title)s</h1>
"""
